package ar.com.odra.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import ar.com.odra.data.builder.ImportContextBuilder;
import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.exception.ImporterDataMaximumNoProcessedException;
import ar.com.odra.data.importer.ImporterData;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.mock.factory.MockStrategiesFactory;
import ar.com.odra.data.mock.strategy.MockStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * 
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class ImporterDataTest {

	private static final String EXCEL_FILE_PATH = "src/test/resources/Importacion_Datos_Test.xls";

	private static final String TEXTO_FILE_PATH = "src/test/resources/Importacion_Datos_Test.txt";

	@Test(expected = ImporterDataMaximumNoProcessedException.class)
	public void testImportadorDatos_EnExcelSuperaMaximoNoProcesado_Invalido() {
		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, 
				MockStrategy.NOMBRE_ESTRATEGIA, "Importacion Materiales Por ID.", new Integer[] { 4 });
		importacionContextoBuilder.withMaximumNoProcess(2);
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		importadorDatos.importar(byteDatos, importacionContexto);

		fail("La importacion debio haber fallado ya que supero el maximo de no procesados.");
	}

	/**
	 * Test de importacion de datos en archivo de texto con una sola columna.
	 * 
	 */
	@Test
	public void testImportadorDatos_EnTextoCantidadProcesada_Valido() {
		byte[] byteDatos = MockUtils.getContenido(TEXTO_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.createImportContextBuilderText(MockUtils.CONFIGURATION_FACTORY, 
				MockStrategy.NOMBRE_ESTRATEGIA, "Importacion Materiales Por ID.");

		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		ImportResult<String, String> resultadoImportacion = importadorDatos.importar(byteDatos, importacionContexto);

		assertTrue("Los resultados importados deberian ser mayor a uno: ", resultadoImportacion.tieneFilasProcesadas());
		assertEquals("La cantidad de item debe ser igual: ", 15, resultadoImportacion.getCantidadProcesada());
		assertEquals("El resultado en la 1er posicion debe ser igual: ", 900001l, resultadoImportacion.getFilaProcesada(0));
	}

	/**
	 * Test de importacion de datos en excel con una sola columna en la solapa 1
	 * (en el exceles la 0).
	 */
	@Test
	public void testImportadorDatos_EnExcelCantidadProcesada_Valido() {
		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, 
				MockStrategy.NOMBRE_ESTRATEGIA, "Importacion Materiales Por ID.");
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		ImportResult<String, String> resultadoImportacion = importadorDatos.importar(byteDatos, importacionContexto);

		assertTrue("Los resultados importados no deberian ser vacios: ", resultadoImportacion.tieneFilasProcesadas());
		assertEquals("La cantidad de item debe ser igual: ", 20, resultadoImportacion.getCantidadProcesada());

		assertEquals("El resultado en la 1er posicion debe ser igual: ", 9999999l, resultadoImportacion.getFilaProcesada(0));
	}

	@Test
	public void testImportadorDatos_EnExcelConAccionEnElResultado_Valido() {
		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, MockStrategy.NOMBRE_ESTRATEGIA,
				"Importacion Materiales Por ID.");
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		ImportResult<String, String> resultadoImportacion = importadorDatos.importar(byteDatos, importacionContexto);

		assertTrue("Los resultados importados no deberian ser vacios: ", resultadoImportacion.tieneFilasProcesadas());
		assertEquals("La cantidad de item debe ser igual: ", 20, resultadoImportacion.getCantidadProcesada());

	}

	@Test
	public void testImportadorDatos_EnExcelConAccionEnElResultado_Invalido() {
		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, MockStrategy.NOMBRE_ESTRATEGIA,
				"Importacion Materiales Por ID.", new Integer[] { 2 });
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		ImportResult<String, String> resultadoImportacion = importadorDatos.importar(byteDatos, importacionContexto);

		assertTrue("Los resultados importados no deberian ser vacios: ", resultadoImportacion.tieneFilasProcesadas());
		assertEquals("La cantidad de item debe ser igual: ", 11, resultadoImportacion.getCantidadProcesada());
	}

	@Test
	public void testImportadorDatos_EnExcelConAccionEnElResultado2Paginas_Vvalido() {

		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, 
				MockStrategy.NOMBRE_ESTRATEGIA, "Importacion Materiales Por ID.", new Integer[] { 1, 2 });
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		ImportResult<String, String> resultadoImportacion = importadorDatos.importar(byteDatos, importacionContexto);

		assertTrue("Los resultados importados no deberian ser vacios: ", resultadoImportacion.tieneFilasProcesadas());
		assertEquals("La cantidad de item debe ser igual: ", 31, resultadoImportacion.getCantidadProcesada());

	}

	@Test
	public void testImportadorDatos_EnExcelConEncabezado_Valido() {
		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, 
				MockStrategy.NOMBRE_ESTRATEGIA, "Importacion Materiales Por ID.", new Integer[] { 3 });
		importacionContextoBuilder.withHeader();
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		ImportResult<String, String> resultadoImportacion = importadorDatos.importar(byteDatos, importacionContexto);

		assertTrue("Los resultados importados no deberian ser vacios: ", resultadoImportacion.tieneFilasProcesadas());
		assertEquals("La cantidad de item debe ser igual: ", 10, resultadoImportacion.getCantidadProcesada());

	}

	@Test(expected = ImporterDataException.class)
	public void testImportadorDatos_EnExcelConPaginaInvalida_Invalido() {
		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, MockStrategy.NOMBRE_ESTRATEGIA,
				"Importacion Materiales Por ID.", new Integer[] { 6 });
		importacionContextoBuilder.withHeader();
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		importadorDatos.importar(byteDatos, importacionContexto);
		fail("La importacion debio haber fallado, ya que tiene una pagina NO existente.");
	}

	@Test(expected = ImporterDataException.class)
	public void testImportadorDatos_EnExcelConNombreEstrategiaInvalida_Invalido() {
		byte[] byteDatos = MockUtils.getContenido(EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, "Estrategia_Invalida", "Importacion Invalida.",
				new Integer[] { 1 });
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		importadorDatos.importar(byteDatos, importacionContexto);
		fail("La importacion debio haber fallado, ya que la estrategia NO existe.");
	}

	@Test(expected = ImporterDataException.class)
	public void testImportadorDatos_EnExcelConBytesInvalidos_Invalido() {
		byte[] byteDatos = new byte[] { 123, 124, 65, 76, 27, 112, 65, 12, 75, 33, 23 };
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY, MockStrategy.NOMBRE_ESTRATEGIA,
				"Descripcion Importacion.", new Integer[] { 1 });
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		ImporterData importadorDatos = new ImporterData(new MockStrategiesFactory());
		importadorDatos.importar(byteDatos, importacionContexto);
		fail("La importacion debio haber fallado, ya que los bytes son invalidos.");
	}
}
