/**
 * 
 */
package ar.com.odra.data.mock.factory;

import java.util.ArrayList;
import java.util.Collection;

import ar.com.odra.data.factory.StrategiesRowFactory;
import ar.com.odra.data.mock.strategy.MockStrategy;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MockStrategiesFactory implements StrategiesRowFactory{

	private int cantidadOk = 0;
	
	public MockStrategiesFactory(int cantidadOk) {
		super();
		this.cantidadOk = cantidadOk;
	}
	
	public MockStrategiesFactory() {
		this(999999);
	}

	public Collection<RowProcessorStrategy> crearEstrategias() {
		Collection<RowProcessorStrategy> estrategias = new ArrayList<RowProcessorStrategy>();
		estrategias.add(new MockStrategy(this.cantidadOk));
		return estrategias;
	}



}
