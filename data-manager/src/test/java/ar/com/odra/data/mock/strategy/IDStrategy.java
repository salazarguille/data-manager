/**
 * 
 */
package ar.com.odra.data.mock.strategy;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class IDStrategy extends RowProcessorStrategy{
	
	public static final String NOMBRE_ESTRATEGIA = "EstrategiaBoletaID";

	public String getNombre() {
		return NOMBRE_ESTRATEGIA;
	}

	@SuppressWarnings("unchecked")
	public String procesarFila(ImportContext importContext, RowFileImport filaArchivo) {
		return filaArchivo.getValorString(0);
	}

	@SuppressWarnings("unchecked")
	public String procesarFilaErronea(ImportContext importContext, RowFileImport filaArchivo, Exception error) {
		String mensajeError = error.getMessage();
		return mensajeError;
	}
}