package ar.com.odra.data.mock.strategy;

import java.util.Date;

import ar.com.odra.data.annotation.as.AsArray;
import ar.com.odra.data.annotation.as.AsBoolean;
import ar.com.odra.data.annotation.as.AsDate;
import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.MethodGenerator;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.utils.BindMode;
import ar.com.odra.data.utils.BooleanFilter;
import ar.com.odra.data.utils.FieldSeparator;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@DataGenerator
public class AsAppRowStrategy {

	@MethodGenerator(bindMode = BindMode.BY_NAME, parameterNames = {"name", "age", "borned"})
	public void testAsDateValid(User bean, String name, Integer age, @AsDate(format = "yyyy-MM-dd") Date nacimiento) {
		
	}
	
	@MethodGenerator(bindMode = BindMode.BY_NAME, parameterNames = {"name", "age","numbers"})
	public void testAsArrayValid(User bean, String name, Integer age, @AsArray(splitter = FieldSeparator.AMPERSON) String[] names) {
		
	}
	
	@MethodGenerator(bindMode = BindMode.BY_NAME, parameterNames = {"name", "age","habilitado"})
	public void testAsBooleanValid(User bean, String name, Integer age, @AsBoolean(value = "SI", filter = BooleanFilter.EQUALS_IGNORECASE) boolean names) {
		
	}
}
