package ar.com.odra.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.mock.strategy.AsAppRowStrategy;
import ar.com.odra.data.strategy.AnnotatedMethodRowProcessorStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MatchingResultAsAnnotationTest {

	@Test
	public void testMatchingResult_BindBeanBasicWithAsDate_Valid() {
		String row = "Diego,21,2013-10-25";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new AsAppRowStrategy(), "testAsDateValid", row);
		MatchResult matchingResult = context.match();
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class, String.class, Integer.class,Date.class);
		assertTrue("Parameter types must be equals.", hasParameterValues);
		
		Object bean = matchingResult.bindBean(new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod()));
		
		assertNotNull("Result bean must not be null.", bean);
		assertEquals("Result bean class must be equals.", User.class, bean.getClass());
		
		Date date = MockUtils.createDate(2013,10,25,00,00,00, Calendar.AM);
		User user = (User) bean;
		assertEquals("Name must be equals.",	"Diego",	user.getName());
		assertEquals("Age must be equals.",			21,			user.getAge());
		assertEquals("Borned must be equals.",		date.toString(),			user.getBorned().toString());
	}
	
	@Test
	public void testMatchingResult_BindBeanBasicWithAsArray_Valid() {
		String row = "Diego,21,Parte1&Parte2&Parte3";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new AsAppRowStrategy(), "testAsArrayValid", row);
		MatchResult matchingResult = context.match();
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class, String.class, Integer.class,String[].class);
		assertTrue("Parameter types must be equals.", hasParameterValues);
		
		Object bean = matchingResult.bindBean(new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod()));
		
		assertNotNull("Result bean must not be null.", bean);
		assertEquals("Result bean class must be equals.", User.class, bean.getClass());
		
		User user = (User) bean;
		assertEquals("Name must be equals.",	"Diego",	user.getName());
		assertEquals("Age must be equals.",			21,			user.getAge());
		assertEquals("Numbers must be equals.",		Arrays.toString(new String[]{"Parte1","Parte2","Parte3"}),	Arrays.toString(user.getNumbers()));
	}
	
	@Test
	public void testMatchingResult_BindBeanBasicWithAsBoolean_Valid() {
		String row = "Diego,21,si";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new AsAppRowStrategy(), "testAsBooleanValid", row);
		MatchResult matchingResult = context.match();
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class, String.class, Integer.class, boolean.class);
		assertTrue("Parameter types must be equals.", hasParameterValues);
		
		Object bean = matchingResult.bindBean(new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod()));
		
		assertNotNull("Result bean must not be null.", bean);
		assertEquals("Result bean class must be equals.", User.class, bean.getClass());
		
		User user = (User) bean;
		assertEquals("Name must be equals.",	"Diego",	user.getName());
		assertEquals("Age must be equals.",			21,			user.getAge());
		assertEquals("Numbers must be equals.",		Boolean.TRUE,	user.isHabilitado());
	}
}
