package ar.com.odra.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.mock.strategy.MockAppRowStrategy;
import ar.com.odra.data.strategy.AnnotatedRowProcessorStrategy;
import ar.com.odra.data.strategy.AnnotatedStaticRowProcessorStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MatchingResultStaticTest {

	@Test
	public void testMatchingResult_BindBeanStatic_Valid() {
		String row = "Diego,21";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createClass", row);
		StaticGenerator staticGenerator = MockUtils.getAnnotation(MockAppRowStrategy.class, StaticGenerator.class);
		MatchResult matchingResult = context.match();
		AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy = new AnnotatedStaticRowProcessorStrategy(context.getDataGenerator(), staticGenerator, MockUtils.MATCHEABLE_REGISTRY, context.getMethod());
		Object bean = matchingResult.bindBean(annotatedRowProcessorStrategy);
		
		assertNotNull("Result bean must not be null.", bean);
		assertEquals("Result bean class must be equals.", User.class, bean.getClass());
		
		User user = (User) bean;
		assertEquals("Name must be equals.",	"Diego",	user.getName());
		assertEquals("Age must be equals.",		21,			user.getAge());
	}
}
