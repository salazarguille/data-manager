package ar.com.odra.data.mock.strategy.sorted;

import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerators;
import ar.com.odra.data.mock.bean.Tablet;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@DataGenerator
@StaticGenerators(values = {
	@StaticGenerator(name = "createTabletStatic", parameters = { "brand", "inches" }, beanClass = Tablet.class, resource = "Datos_Test.xls", pages = 3),
})
public class Sorted2AppRowStrategy {

}
