/**
 * 
 */
package ar.com.odra.data.mock.strategy;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MockStrategy extends RowProcessorStrategy{
	
	public static final String NOMBRE_ESTRATEGIA = "EstrategiaBoletaID";
	private int cantidadOk;
	
	public MockStrategy(int cantidadOk) {
		super();
		this.cantidadOk = cantidadOk;
	}

	public String getNombre() {
		return NOMBRE_ESTRATEGIA;
	}

	@SuppressWarnings("unchecked")
	public <T> T procesarFila(ImportContext importContext, RowFileImport filaArchivo) {
		if(filaArchivo.getIndice() < this.cantidadOk) {
			return (T) filaArchivo.getValorLong(0);
		} else {
			throw new RuntimeException("Fila Invalida.");
		}
	}

	@SuppressWarnings("unchecked")
	public <H> H procesarFilaErronea(ImportContext importContext, RowFileImport filaArchivo, Exception error) {
		return (H) filaArchivo.getValor(0);
	}
}