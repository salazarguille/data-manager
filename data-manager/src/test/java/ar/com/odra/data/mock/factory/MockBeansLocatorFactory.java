/**
 * 
 */
package ar.com.odra.data.mock.factory;

import ar.com.odra.data.factory.BeansLocatorFactory;
import ar.com.odra.data.locator.BeansLocator;
import ar.com.odra.data.mock.locator.MockBeansLocator;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MockBeansLocatorFactory implements BeansLocatorFactory{

	public BeansLocator createBeansLocator() {
		return new MockBeansLocator();
	}



}
