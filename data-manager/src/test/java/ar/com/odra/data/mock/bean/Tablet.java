package ar.com.odra.data.mock.bean;

import ar.com.odra.data.annotation.bean.DAOBy;
import ar.com.odra.data.annotation.bean.GeneratedBy;
import ar.com.odra.data.mock.strategy.MockAppRowStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@GeneratedBy(values = { MockAppRowStrategy.class })
@DAOBy("tabletDAO")
public class Tablet {

	private String brand;
	private int inches;
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the inches
	 */
	public int getInches() {
		return inches;
	}
	/**
	 * @param inches the inches to set
	 */
	public void setInches(int inches) {
		this.inches = inches;
	}
}
