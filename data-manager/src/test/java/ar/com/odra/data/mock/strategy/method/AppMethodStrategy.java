/**
 * 
 */
package ar.com.odra.data.mock.strategy.method;

import ar.com.odra.data.annotation.generator.BindParam;
import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.MethodGenerator;
import ar.com.odra.data.mock.bean.AppUser;
import ar.com.odra.data.utils.BindMode;

/**
 *
 */
@DataGenerator(resource = "data.xls")
public class AppMethodStrategy {

	@MethodGenerator(hasHeader = true, bindMode = BindMode.BY_ANNOTATION)
	public void createAppUser(AppUser appUser, @BindParam(name = "surname") String surname, 
			@BindParam(name = "name") String name, @BindParam(name = "password") String password){
	}
}
