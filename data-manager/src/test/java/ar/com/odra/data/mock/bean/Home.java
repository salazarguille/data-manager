package ar.com.odra.data.mock.bean;

import ar.com.odra.data.annotation.bean.DAOBy;
import ar.com.odra.data.annotation.bean.GeneratedBy;
import ar.com.odra.data.mock.strategy.MockAppRowStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@GeneratedBy(values = { MockAppRowStrategy.class })
@DAOBy("colorDAO")
public class Home {

	private String color;
	private int deptos;
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the deptos
	 */
	public int getDeptos() {
		return deptos;
	}
	/**
	 * @param deptos the deptos to set
	 */
	public void setDeptos(int deptos) {
		this.deptos = deptos;
	}

	
}
