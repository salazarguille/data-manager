package ar.com.odra.data.mock.bean;

import ar.com.odra.data.annotation.bean.DAOBy;
import ar.com.odra.data.annotation.bean.GeneratedBy;
import ar.com.odra.data.mock.strategy.MockAppRowStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@GeneratedBy(values = { MockAppRowStrategy.class })
@DAOBy("mobileDAO")
public class Mobile {

	private String brand;
	private String color;
	/**
	 * @return the brand
	 */
	public String getBrand() {
		return brand;
	}
	/**
	 * @param brand the brand to set
	 */
	public void setBrand(String brand) {
		this.brand = brand;
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
}
