package ar.com.odra.data.mock.strategy;

import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.MethodGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.mock.bean.NotValidBean;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.utils.BindMode;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@DataGenerator
@StaticGenerator(name = "cannotCreateBean", parameters = { "name", "age" }, beanClass = Object.class,resource="")
public class NoValidAppRowStrategy {

	@MethodGenerator(name = "", bindMode = BindMode.BY_NAME, parameterNames = {"name", "ages"})
	public void invalidParameterName(User bean, String name, Integer age) {
		
	}
	
	@MethodGenerator(name = "", bindMode = BindMode.BY_NAME, parameterNames = {"", "surname"})
	public void emptyParameterName(User bean, String name, Integer age, String surname) {
		
	}
	
	@MethodGenerator(name = "", bindMode = BindMode.BY_NAME, parameterNames = {"name", "age"})
	public void invalidParameterType(User bean, String name, Integer age) {
		
	}
	
	@MethodGenerator(name = "", bindMode = BindMode.BY_NAME, parameterNames = {"dummy"})
	public void cannotCreateBeanInstance(NotValidBean bean, String dummy) {
		
	}
}
