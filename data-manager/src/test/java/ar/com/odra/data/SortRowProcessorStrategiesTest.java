package ar.com.odra.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;

import ar.com.odra.data.exception.NotDependencyFoundException;
import ar.com.odra.data.exception.SelfDependencyException;
import ar.com.odra.data.importer.AnnotatedImporterData;
import ar.com.odra.data.mock.strategy.SelfAppRowStrategy;
import ar.com.odra.data.mock.strategy.cycle.Cycle1AppRowStrategy;
import ar.com.odra.data.mock.strategy.cycle.Cycle2AppRowStrategy;
import ar.com.odra.data.mock.strategy.cycle.Cycle3AppRowStrategy;
import ar.com.odra.data.mock.strategy.sorted.Sorted1AppRowStrategy;
import ar.com.odra.data.mock.strategy.sorted.Sorted2AppRowStrategy;
import ar.com.odra.data.mock.strategy.sorted.Sorted3AppRowStrategy;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class SortRowProcessorStrategiesTest {

	@Test(expected = SelfDependencyException.class)
	public void testAnnotatedImporterData_SelfDependency_Invalid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new SelfAppRowStrategy());
		importadorDatos.sortStrategies();
	}
	
	@Ignore
	@Test
	public void testAnnotatedImporterData_CycleSortRowProcessorStrategies_Invalid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new Cycle1AppRowStrategy());
		importadorDatos.registrarEstrategias(new Cycle2AppRowStrategy());
		importadorDatos.registrarEstrategias(new Cycle3AppRowStrategy());
		RowProcessorStrategy[] sortedStrategies = importadorDatos.sortStrategies();
		
		assertNotNull("Strategies must be not null.", sortedStrategies);
	}
	
	@Test(expected = NotDependencyFoundException.class)
	public void testAnnotatedImporterData_SortaRowProcessorStrategies_Valid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new Sorted1AppRowStrategy());
		importadorDatos.registrarEstrategias(new Sorted3AppRowStrategy());
		RowProcessorStrategy[] sortedStrategies = importadorDatos.sortStrategies();
		
		assertNotNull("Strategies must be not null.", sortedStrategies);
		assertEquals("Strategies must be 3", 3, 	sortedStrategies.length);
	}
	
	@Test
	public void testAnnotatedImporterData_SortEmptyRowProcessorStrategies_Valid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		RowProcessorStrategy[] sortedStrategies = importadorDatos.sortStrategies();
		
		assertNotNull("Strategies must be not null.", sortedStrategies);
		assertEquals("Strategies must be 0", 0, 	sortedStrategies.length);
	}
	@Test
	public void testAnnotatedImporterData_SortRowProcessorStrategies_Valid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new Sorted1AppRowStrategy());
		importadorDatos.registrarEstrategias(new Sorted2AppRowStrategy());
		RowProcessorStrategy[] sortedStrategies = importadorDatos.sortStrategies();
		
		assertNotNull("Strategies must be not null.", sortedStrategies);
		assertEquals("Strategies must be 3", 3, 	sortedStrategies.length);
		assertEquals("Row name must be equals.",	"createTabletStatic",sortedStrategies[0].getNombre());
		assertEquals("Row name must be equals.",	"createUserStatic",sortedStrategies[1].getNombre());
		assertEquals("Row name must be equals.",	"createHomeStatic",sortedStrategies[2].getNombre());
	}
}
