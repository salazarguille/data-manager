package ar.com.odra.data.mock.strategy;

import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerators;
import ar.com.odra.data.mock.bean.Home;
import ar.com.odra.data.mock.bean.User;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@DataGenerator
@StaticGenerators(values = {
	@StaticGenerator(name = "createUserStatic", parameters = { "name", "age" }, beanClass = User.class, resource = "Datos_Test.xls", order = 1),
	@StaticGenerator(name = "createHomeStatic", parameters = { "color", "deptos" }, beanClass = Home.class, resource = "Datos_Test.xls", order = 2, pages = {2})	
})
public class ValidAppRowStrategy {

}
