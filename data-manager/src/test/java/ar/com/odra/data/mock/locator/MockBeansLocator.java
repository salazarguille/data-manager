/**
 * 
 */
package ar.com.odra.data.mock.locator;

import ar.com.odra.data.locator.BeansLocator;
import ar.com.odra.data.locator.Locatable;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MockBeansLocator implements BeansLocator, Locatable {

	public <T> T[] execute(T... beans) {
		return beans;
	}

	public Locatable locateBean(String name) {
		return this;
	}

	@Override
	public Locatable locateBean(Class<?> type) {
		return this;
	}

	@Override
	public boolean canLocateBean(String name) {
		return true;
	}

	@Override
	public boolean canLocateBean(Class<?> type) {
		return true;
	}
}
