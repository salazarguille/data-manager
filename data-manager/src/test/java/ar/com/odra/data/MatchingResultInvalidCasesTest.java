package ar.com.odra.data;

import org.junit.Test;

import ar.com.odra.data.exception.BeanPropertyNotFoundException;
import ar.com.odra.data.exception.InvalidConvertParameterTypeException;
import ar.com.odra.data.exception.NotBeanCreatedException;
import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.mock.strategy.NoValidAppRowStrategy;
import ar.com.odra.data.strategy.AnnotatedMethodRowProcessorStrategy;
import ar.com.odra.data.strategy.AnnotatedRowProcessorStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MatchingResultInvalidCasesTest {

	@Test(expected = BeanPropertyNotFoundException.class)
	public void testMatchingContext_MatchParameterNotFound_Invalid() {
		String row = "Diego,21";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new NoValidAppRowStrategy(), "invalidParameterName", row);
		MatchResult matchingResult = context.match();
		AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy = new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod());
		matchingResult.bindBean(annotatedRowProcessorStrategy);
	}
	
	@Test(expected = BeanPropertyNotFoundException.class)
	public void testMatchingContext_MatchEmptyParameter_Invalid() {
		String row = "Diego,21";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new NoValidAppRowStrategy(), "emptyParameterName", row);
		MatchResult matchingResult = context.match();
		AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy = new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod());
		matchingResult.bindBean(annotatedRowProcessorStrategy);
	}
	
	@Test(expected = InvalidConvertParameterTypeException.class)
	public void testMatchingContext_InvalidParameterType_Invalid() {
		String row = "Diego,21A";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new NoValidAppRowStrategy(), "invalidParameterType", row);
		MatchResult matchingResult = context.match();
		AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy = new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod());
		matchingResult.bindBean(annotatedRowProcessorStrategy);
	}
	
	@Test(expected = NotBeanCreatedException.class)
	public void testMatchingContext_MatchParameterNotBeanCreaded_Invalid() {
		String row = "Diego,21";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new NoValidAppRowStrategy(), "cannotCreateBeanInstance", row);
		MatchResult matchingResult = context.match();
		AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy = new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod());
		matchingResult.bindBean(annotatedRowProcessorStrategy);
	}
}
