package ar.com.odra.data.mock.strategy;

import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerators;
import ar.com.odra.data.mock.bean.User;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@DataGenerator
@StaticGenerators(values = {
	@StaticGenerator(name = "invalid", parameters = { "name", "age" }, beanClass = User.class, resource = "Invalid_Datos_Test.xls"),
})
public class InvalidFileAppRowStrategy {

}
