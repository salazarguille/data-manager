package ar.com.odra.data.mock.strategy;

import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.MethodGenerator;
import ar.com.odra.data.annotation.generator.OnErrorHandlerGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.utils.BindMode;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@DataGenerator
@StaticGenerator(pages = {2}, name = "createUserStatic", parameters = { "name", "age" }, beanClass = User.class, resource = "Importacion_Datos_Test.xls")
public class MockAppRowStrategy {

	@MethodGenerator(name = "", bindMode = BindMode.BY_NAME, parameterNames = {"name", "age"})
	public void createClass(User bean, String name, Integer age) {
		
	}
	
	@MethodGenerator(bindMode = BindMode.BY_NAME, parameterNames = {"name","age"})
	public void createUser(User bean, String name, Integer age, String surname, Long age2) {
		
	}
	
	@MethodGenerator(bindMode = BindMode.BY_NAME)
	public User createUserWithoutUser(String name, Integer age, String surname, Long age2) {
		
		return null;
	}
	
	@MethodGenerator(bindMode = BindMode.BY_NAME)
	public void createUserWithoutParameters(User user) {
		
	}
	
	@MethodGenerator(bindMode = BindMode.BY_NAME)
	public User createUserEmpty() {
		
		return null;
	}
	
	@OnErrorHandlerGenerator(name = "createClass")
	public void createClassError(Exception error, String name, Integer age) {
		
	}
}
