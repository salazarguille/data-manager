package ar.com.odra.data;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.mock.strategy.MockAppRowStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MatchingContextTest {

	@Test
	public void testMatchingContext_Match_Valido() {
		String row = "Diego,21";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createClass", row);
		MatchResult matchingResult = context.match();
		
		assertTrue("Matching result must have bean instance.", matchingResult.hasBeanInstance());
		
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class, String.class, Integer.class);
		assertTrue("Matching result must have same parameter values.", hasParameterValues);
	}

	@Test
	public void testMatchingContext_MatchWithoutBean_Valido() {
		String row = "Diego,21,Sanchez,1234";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createUserWithoutUser", row);
		MatchResult matchingResult = context.match();
		
		assertFalse("Matching result must not have bean instance.", matchingResult.hasBeanInstance());
		
		boolean hasParameterValues = matchingResult.hasParameterTypes(String.class, Integer.class, String.class, Long.class);
		assertTrue("Matching result must have same parameter values.", hasParameterValues);
	}
	
	@Test
	public void testMatchingContext_MatchWithoutParameters_Valido() {
		String row = "";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createUserWithoutParameters", row);
		MatchResult matchingResult = context.match();
		
		assertTrue("Matching result must not have bean instance.", matchingResult.hasBeanInstance());
		
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class);
		assertTrue("Matching result must have same parameter values.", hasParameterValues);
	}
	
	@Test
	public void testMatchingContext_MatchEmpty_Valido() {
		String row = "";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createUserEmpty", row);
		MatchResult matchingResult = context.match();
		
		assertFalse("Matching result must not have bean instance.", matchingResult.hasBeanInstance());
		
		boolean hasParameterValues = matchingResult.hasParameterTypes();
		assertTrue("Matching result must have same parameter values.", hasParameterValues);
	}
	
	@Test
	public void testMatchingContext_Match_MasParametrosValido() {
		String row = "Diego,21,Sanchez,1234";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createUser", row);
		MatchResult matchingResult = context.match();
		
		assertTrue("Matching result must have bean instance.", matchingResult.hasBeanInstance());
		
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class, String.class, Integer.class, String.class, Long.class);
		assertTrue("Matching result must have same parameter values.", hasParameterValues);
	}
	
	@Test
	public void testMatchingContext_MatchLessParameterWithValues_Valid() {
		String row = "Diego,21,Sanchez";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createUser", row);
		MatchResult matchingResult = context.match();
		
		assertTrue("Matching result must have bean instance.", matchingResult.hasBeanInstance());
		System.out.println(matchingResult);
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class, String.class, Integer.class, String.class, Long.class);
		assertTrue("Matching result must have same parameter values.", hasParameterValues);
	}
	
	@Test
	public void testMatchingContext_MatchMoreParameterWithValues_Valid() {
		String row = "Diego,21,Sanchez,123,ValorNoMatcheado";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createUser", row);
		MatchResult matchingResult = context.match();
		
		assertTrue("Matching result must have bean instance.", matchingResult.hasBeanInstance());
		
		boolean hasParameterValues = matchingResult.hasParameterTypes(User.class, String.class, Integer.class, String.class, Long.class);
		assertTrue("Matching result must have same parameter values.", hasParameterValues);
	}	
}
