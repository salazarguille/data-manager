/**
 *
 */
package ar.com.odra.data.mock.bean;

import ar.com.odra.data.annotation.bean.DAOBy;
import ar.com.odra.data.annotation.bean.GeneratedBy;
import ar.com.odra.data.mock.strategy.method.AppMethodStrategy;

/**
 * @author William
 *
 */
@GeneratedBy(values = {AppMethodStrategy.class})
@DAOBy(value = "mockUserDAO")
public class AppUser extends User {

	private String surname;
	private String password;

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
}
