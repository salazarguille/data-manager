package ar.com.odra.data.mock.strategy.sorted;

import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerators;
import ar.com.odra.data.mock.bean.Mobile;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@DataGenerator (dependencies = {Sorted1AppRowStrategy.class})
@StaticGenerators(values = {
	@StaticGenerator(name = "createMobileStatic", parameters = { "color", "brand" }, beanClass = Mobile.class, resource = "Datos_Test.xls", order = 1, pages = 4),
})
public class Sorted3AppRowStrategy {

}
