package ar.com.odra.data.mock.bean;

import java.io.InputStream;
import java.util.Date;

import ar.com.odra.data.annotation.bean.DAOBy;
import ar.com.odra.data.annotation.bean.GeneratedBy;
import ar.com.odra.data.mock.strategy.MockAppRowStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
@GeneratedBy(values = { MockAppRowStrategy.class })
@DAOBy("userDAO")
public class User {

	private String name;
	private int age;
	private String[] numbers;
	private boolean habilitado;
	private Date borned;
	private InputStream image;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * @return the numbers
	 */
	public String[] getNumbers() {
		return numbers;
	}
	/**
	 * @param numbers the numbers to set
	 */
	public void setNumbers(String[] numbers) {
		this.numbers = numbers;
	}
	/**
	 * @return the habilitado
	 */
	public boolean isHabilitado() {
		return habilitado;
	}
	/**
	 * @param habilitado the habilitado to set
	 */
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	/**
	 * @return the borned
	 */
	public Date getBorned() {
		return borned;
	}
	/**
	 * @param borned the borned to set
	 */
	public void setBorned(Date borned) {
		this.borned = borned;
	}
	/**
	 * @return the image
	 */
	public InputStream getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(InputStream image) {
		this.image = image;
	}
}
