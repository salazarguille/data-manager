/**
 * 
 */
package ar.com.odra.data.mock.factory;

import java.util.ArrayList;
import java.util.Collection;

import ar.com.odra.data.factory.StrategiesRowFactory;
import ar.com.odra.data.mock.strategy.IDStrategy;
import ar.com.odra.data.mock.strategy.VacEstrategy;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class Mock2RowStrategiesFactory implements StrategiesRowFactory{

	public Collection<RowProcessorStrategy> crearEstrategias() {
		Collection<RowProcessorStrategy> estrategias = new ArrayList<RowProcessorStrategy>();
		estrategias.add(new IDStrategy());
		estrategias.add(new VacEstrategy());
		return estrategias;
	}



}
