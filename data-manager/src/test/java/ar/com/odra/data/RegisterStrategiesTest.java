package ar.com.odra.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.importer.ImporterData;
import ar.com.odra.data.mock.factory.Mock2RowStrategiesFactory;
import ar.com.odra.data.mock.strategy.IDStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class RegisterStrategiesTest  {

	@Test
	public void testImportadorDatos_RegistrarEstrategia_Valido() {
		ImporterData importadorDatos = new ImporterData();
		importadorDatos.registrarEstrategia(new IDStrategy());

		boolean contieneEstrategia = importadorDatos
				.contieneEstrategia(IDStrategy.NOMBRE_ESTRATEGIA);
		assertTrue("El importador debe contener la estrategia: ",
				contieneEstrategia);

		long cantidadEstrategias = importadorDatos.getCantidadEstrategias();
		assertEquals("La cantidad de estrategias deben ser iguales: ", 1l,
				cantidadEstrategias);
	}

	@Test(expected = ImporterDataException.class)
	public void testImportadorDatos_RegistrarEstrategiaDuplicada_Invalido() {
		ImporterData importadorDatos = new ImporterData();
		importadorDatos.registrarEstrategia(new IDStrategy());
		importadorDatos.registrarEstrategia(new IDStrategy());
		fail("La registracion de la estrategia debi� haber fallado porque se registro dos veces la misma estrategia con el mismo nombre.");
	}
	
	@Test(expected = ImporterDataException.class)
	public void testImportadorDatos_RegistrarEstrategiaDuplicadaPorDosFactory_Invalido() {
		ImporterData importadorDatos = new ImporterData();
		importadorDatos.registrarEstrategias(new Mock2RowStrategiesFactory());
		importadorDatos.registrarEstrategias(new Mock2RowStrategiesFactory());
		fail("La registracion de la estrategia debi� haber fallado porque se registro dos veces la misma estrategia con el mismo nombre.");
	}
	
	@Test(expected = ImporterDataException.class)
	public void testImportadorDatos_RegistrarEstrategiaDuplicadaPorFactoryEIndividual_Invalido() {
		ImporterData importadorDatos = new ImporterData(new Mock2RowStrategiesFactory());
		importadorDatos.registrarEstrategia(new IDStrategy());
		fail("La registracion de la estrategia debi� haber fallado porque se registro dos veces la misma estrategia con el mismo nombre.");
	}
}
