package ar.com.odra.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import ar.com.odra.data.importer.AnnotatedImporterData;
import ar.com.odra.data.importer.data.GroupImportResult;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.mock.bean.AppUser;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.mock.strategy.ValidAppRowStrategy;
import ar.com.odra.data.mock.strategy.method.AppMethodStrategy;
import ar.com.odra.data.mock.strategy.sorted.Sorted1AppRowStrategy;
import ar.com.odra.data.mock.strategy.sorted.Sorted2AppRowStrategy;
import ar.com.odra.data.mock.strategy.sorted.Sorted3AppRowStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * 
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class AnnotatedImporterDataTest {

	@Test
	public void testAnnotatedImporterData_ExcelWithStaticAnnotations_Valid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new ValidAppRowStrategy());
		GroupImportResult result = importadorDatos.importar();
		assertNotNull("Group result must not be null.", result);
		ImportResult<User, String> createUserStaticResult = result.getResultFor("createUserStatic");
		assertNotNull("Import result must not be null.", createUserStaticResult);
		assertEquals("Rows processed ok must be 5", 5, createUserStaticResult.getCantidadProcesada());
		assertEquals("Rows no processed must be 0", 0, createUserStaticResult.getCantidadNoProcesada());
		
		User user1 = createUserStaticResult.getFilaProcesada(0);
		assertEquals("Property must be equals", "User 1", user1.getName());
		assertEquals("Property must be equals", 21, user1.getAge());
		
		User user2 = createUserStaticResult.getFilaProcesada(1);
		assertEquals("Property must be equals", "User 2", user2.getName());
		assertEquals("Property must be equals", 22, user2.getAge());
		
		User user5 = createUserStaticResult.getFilaProcesada(4);
		assertEquals("Property must be equals", "User 5", user5.getName());
		assertEquals("Property must be equals", 25, user5.getAge());
	}
	
	@Test
	public void testAnnotatedImporterData_ExcelWithStaticAnnotationsSorted_Valid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new Sorted1AppRowStrategy(), new Sorted2AppRowStrategy(), new Sorted3AppRowStrategy());
		GroupImportResult result = importadorDatos.importar();
		assertNotNull("Group result must not be null.", result);
		assertEquals("Group result must have 4 import result", 4, result.getGroupImportResultSize());
		
		ImportResult<User, String> createUserStaticResult = result.getResultFor("createUserStatic");
		assertNotNull("Import result must not be null.", createUserStaticResult);
		assertEquals("Rows processed ok must be 5", 5, createUserStaticResult.getCantidadProcesada());
		assertEquals("Rows no processed must be 0", 0, createUserStaticResult.getCantidadNoProcesada());
		
		ImportResult<User, String> createTabletStaticResult = result.getResultFor("createTabletStatic");
		assertNotNull("Import result must not be null.", createTabletStaticResult);
		assertEquals("Rows processed ok must be 5", 9, createTabletStaticResult.getCantidadProcesada());
		assertEquals("Rows no processed must be 0", 0, createTabletStaticResult.getCantidadNoProcesada());
		
		ImportResult<User, String> createHomeStaticResult = result.getResultFor("createHomeStatic");
		assertNotNull("Import result must not be null.", createHomeStaticResult);
		assertEquals("Rows processed ok must be 5", 5, createHomeStaticResult.getCantidadProcesada());
		assertEquals("Rows no processed must be 0", 0, createHomeStaticResult.getCantidadNoProcesada());
		
		ImportResult<User, String> createMobileStaticResult = result.getResultFor("createMobileStatic");
		assertNotNull("Import result must not be null.", createMobileStaticResult);
		assertEquals("Rows processed ok must be 5", 9, createMobileStaticResult.getCantidadProcesada());
		assertEquals("Rows no processed must be 0", 0, createMobileStaticResult.getCantidadNoProcesada());
	}

	
	@Test
	public void testAnnotatedImporterData_ExcelWithMethodGenerator_Valid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new AppMethodStrategy());
		GroupImportResult result = importadorDatos.importar();
		assertNotNull("Group result must not be null.", result);
		
		ImportResult<AppUser, String> userImportResult = result.getResultFor("createAppUser");
		AppUser appUser = userImportResult.getFilaProcesada(0);
		assertNotNull("Import result entity must be not null", appUser);
		assertEquals("Name must be equals", "Name 1", appUser.getName());
		assertEquals("Surname must be equals", "Surname 1", appUser.getSurname());
		assertEquals("Password must be equals", "Password 1", appUser.getPassword());
	}
}
