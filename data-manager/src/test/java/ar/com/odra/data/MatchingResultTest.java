package ar.com.odra.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.mock.bean.User;
import ar.com.odra.data.mock.strategy.MockAppRowStrategy;
import ar.com.odra.data.strategy.AnnotatedMethodRowProcessorStrategy;
import ar.com.odra.data.strategy.AnnotatedRowProcessorStrategy;
import ar.com.odra.data.util.MockUtils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MatchingResultTest {

	@Test
	public void testMatchingResult_BindBeanBasicByName_Valid() {
		String row = "Diego,21";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createClass", row);
		MatchResult matchingResult = context.match();
		AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy = new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod());
		Object bean = matchingResult.bindBean(annotatedRowProcessorStrategy);
		
		assertNotNull("Result bean must not be null.", bean);
		assertEquals("Result bean class must be equals.", User.class, bean.getClass());
		
		User user = (User) bean;
		assertEquals("Name must be equals.",	"Diego",	user.getName());
		assertEquals("Age must be equals.",		21,			user.getAge());
	}

	
	@Test
	public void testMatchingResult_BindBeanBasicByNameWithMoreParameterInMethod_Valid() {
		String row = "Mario,27,Sanchez,123";
		ParametersMatcher context = MockUtils.createParametersContext(MockUtils.CONFIGURATION_FACTORY, new MockAppRowStrategy(), "createUser", row);
		MatchResult matchingResult = context.match();
		AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy = new AnnotatedMethodRowProcessorStrategy(context.getDataGenerator(), MockUtils.MATCHEABLE_REGISTRY, context.getMethod());
		Object bean = matchingResult.bindBean(annotatedRowProcessorStrategy);

		assertNotNull("Result bean must not be null.", bean);
		assertEquals("Result bean class must be equals.", User.class, bean.getClass());
		
		User user = (User) bean;
		assertEquals("Name must be equals.",	"Mario",	user.getName());
		assertEquals("Age must be equals.",		27,			user.getAge());
	}

}
