package ar.com.odra.data;

import static org.junit.Assert.fail;

import org.junit.Test;

import ar.com.odra.data.builder.ImportContextBuilder;
import ar.com.odra.data.exception.ImporterDataInvalidFileException;
import ar.com.odra.data.exception.ImporterDataMaximumNoProcessedException;
import ar.com.odra.data.importer.AnnotatedImporterData;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.mock.factory.MockStrategiesFactory;
import ar.com.odra.data.mock.strategy.InvalidFileAppRowStrategy;
import ar.com.odra.data.mock.strategy.MockStrategy;
import ar.com.odra.data.util.MockUtils;


/**
 * 
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class AnnotatedImporterDataInvalidCasesTest {

	@Test(expected = ImporterDataMaximumNoProcessedException.class)
	public void testAnnotatedImporterData_ExcelMaximumNoProcess_Invalid() {
		byte[] byteDatos = MockUtils.getContenido(MockUtils.EXCEL_FILE_PATH);
		ImportContextBuilder importacionContextoBuilder = ImportContextBuilder.crearContextoTipoExcel(MockUtils.CONFIGURATION_FACTORY,
				MockStrategy.NOMBRE_ESTRATEGIA, "Importacion Por ID.", new Integer[] { 4 });
		importacionContextoBuilder.withMaximumNoProcess(2);
		ImportContext importacionContexto = importacionContextoBuilder.buildImportContext();
		
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY, new MockStrategiesFactory());
		importadorDatos.importar(byteDatos, importacionContexto);

		fail("La importacion debio haber fallado ya que supero el maximo de no procesados.");
	}
	
	@Test(expected = ImporterDataInvalidFileException.class)
	public void testAnnotatedImporterData_NotValidFile_Invalid() {
		AnnotatedImporterData importadorDatos = new AnnotatedImporterData(MockUtils.CONFIGURATION_FACTORY);
		importadorDatos.registrarEstrategias(new InvalidFileAppRowStrategy());
		importadorDatos.importar();
		fail("Import process should be failed because file format is invalid.");
	}
}
