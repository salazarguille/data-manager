/**
 * 
 */
package ar.com.odra.data.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.util.Calendar;
import java.util.Date;

import ar.com.odra.common.helper.AnnotationHelper;
import ar.com.odra.common.helper.ReflectionHelper;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerators;
import ar.com.odra.data.content.impl.TextRowFile;
import ar.com.odra.data.factory.BeansLocatorFactory;
import ar.com.odra.data.factory.ConfigurationFactory;
import ar.com.odra.data.factory.impl.DefaultConfigurationFactory;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.mock.factory.MockBeansLocatorFactory;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MockUtils {
	
	public static final String EXCEL_FILE_PATH = "src/test/resources/Importacion_Datos_Test.xls";
	
	public static final String TEXT_FILE_PATH = "src/test/resources/Importacion_Datos_Test.txt";
	
	public static final ParameterMatcheableRegistry MATCHEABLE_REGISTRY = new ParameterMatcheableRegistry();

	public static final BeansLocatorFactory BEANS_LOCATOR_FACTORY = new MockBeansLocatorFactory();
	
	public static final ConfigurationFactory CONFIGURATION_FACTORY = new DefaultConfigurationFactory(BEANS_LOCATOR_FACTORY);

	protected MockUtils() {
		super();
	}

	public static byte[] getContenido(String nombreArchivo) {
		byte[] byteDatos = null;
		try {
			File file = new File(nombreArchivo);
			InputStream inputStream = new FileInputStream(file);
			byteDatos = org.apache.poi.util.IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			throw new RuntimeException(
					"Error obteniendo contenido del input stream: ["
							+ e.getMessage() + "].", e);
		}
		return byteDatos;
	}
	
	public static ParametersMatcher createParametersContext(ConfigurationFactory configurationFactory, Object dataGenerator, String methodName, String row) {
		ParametersMatcher context = new ParametersMatcher();
		context.setDataGenerator(dataGenerator);
		context.setMethod(ReflectionHelper.getMethods(dataGenerator.getClass(), methodName).get(0));
		context.setRowFileImport(new TextRowFile(row, 1));
		context.setImportContext(createImportContext(configurationFactory));
		return context;
	}
	
	public static ImportContext createImportContext(ConfigurationFactory configurationFactory) {
		return new ImportContext(configurationFactory);
	}
	
	public static Date createDate(int year, int month, int day, int hour, int minute, int second, int amPm) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, day);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.HOUR, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		calendar.set(Calendar.AM_PM, amPm);
		return calendar.getTime();
	}

	public static <T extends Annotation, H> T getAnnotation(Class<H> type, Class<T> annotationType) {
		return AnnotationHelper.getAnnotation(type, annotationType);
	}
	
	public static <H> StaticGenerator getStaticGenerator(Class<H> type, int index) {
		StaticGenerators staticGenerators = getAnnotation(type, StaticGenerators.class);
		return staticGenerators.values()[index];
	}
}
