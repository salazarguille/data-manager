package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class DoubleArrayTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<Double[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Double[], ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public DoubleArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Double[], Integer[]>(){
					public Integer[] transform(Double[] string) {
						return convertArray(Double.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Double[], Integer[]>(){
					public Integer[] transform(Double[] string) {
						return convertArray(Double.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Double[].class, new Transformer<Double[], Long[]>(){

					public Long[] transform(Double[] string) {
						return convertArray(Double.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Double[], Long[]>(){

			public Long[] transform(Double[] string) {
				return convertArray(Double.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<Double[], Double[]>(){
					public Double[] transform(Double[] string) {
						return convertArray(Double.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Double[], Double[]>(){

				public Double[] transform(Double[] string) {
					return convertArray(Double.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Double[], Short[]>(){

					public Short[] transform(Double[] string) {
						return convertArray(Double.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Double[], Short[]>(){

				public Short[] transform(Double[] string) {
					return convertArray(Double.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Double[], Boolean[]>(){

					public Boolean[] transform(Double[] string) {
						return convertArray(Double.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Double[], Boolean[]>(){

				public Boolean[] transform(Double[] string) {
					return convertArray(Double.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Double[], Character[]>(){
					public Character[] transform(Double[] string) {
						return convertArray(Double.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Double[], Character[]>(){

				public Character[] transform(Double[] string) {
					return convertArray(Double.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Double[].class, new Transformer<Double[], String[]>(){

					public String[] transform(Double[] string) {
						return convertArray(Double.class, string, String.class, new String[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Double[], Float[]>(){

					public Float[] transform(Double[] string) {
						return convertArray(Double.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<Double[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}

}
