package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class LongArrayTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<Long[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Long[], ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public LongArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Long[], Integer[]>(){
					public Integer[] transform(Long[] string) {
						return convertArray(Long.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Long[], Integer[]>(){
					public Integer[] transform(Long[] string) {
						return convertArray(Long.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Long[].class, new Transformer<Long[], Long[]>(){

					public Long[] transform(Long[] string) {
						return convertArray(Long.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Long[], Long[]>(){

			public Long[] transform(Long[] string) {
				return convertArray(Long.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<Long[], Double[]>(){
					public Double[] transform(Long[] string) {
						return convertArray(Long.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Long[], Double[]>(){

				public Double[] transform(Long[] string) {
					return convertArray(Long.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Long[], Short[]>(){

					public Short[] transform(Long[] string) {
						return convertArray(Long.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Long[], Short[]>(){

				public Short[] transform(Long[] string) {
					return convertArray(Long.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Long[], Boolean[]>(){

					public Boolean[] transform(Long[] string) {
						return convertArray(Long.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Long[], Boolean[]>(){

				public Boolean[] transform(Long[] string) {
					return convertArray(Long.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Long[], Character[]>(){
					public Character[] transform(Long[] string) {
						return convertArray(Long.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Long[], Character[]>(){

				public Character[] transform(Long[] string) {
					return convertArray(Long.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Long[].class, new Transformer<Long[], String[]>(){

					public String[] transform(Long[] string) {
						return convertArray(Long.class, string, String.class, new String[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Long[], Float[]>(){

					public Float[] transform(Long[] string) {
						return convertArray(Long.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<Long[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}

}
