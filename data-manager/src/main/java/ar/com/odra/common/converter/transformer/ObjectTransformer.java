/**
 *
 */
package ar.com.odra.common.converter.transformer;

import org.apache.commons.collections15.Transformer;

/**
 * @author William
 *
 */
public interface ObjectTransformer {

	public Transformer<? extends Object, ? extends Object> getTransformer(Class<?> clazz);

}
