/**
 * 
 */
package ar.com.odra.common.helper;

/**
 * @author HP
 *
 */
public class CollectionHelper {

	/** Get size from an array entities.
	 *
	 * @param entities to get its size.
	 * @return entities size.
	 */
	public static Integer getSizeFromArray(Object[] entities){
		return isNotEmpty(entities) ? entities.length : 0;
	}
	
	/** Test if entities array is not empty.
	 *
	 * @param entities to test if is not empty.
	 * @return true if entities is not null and its length is more than zero.
	 */
	public static boolean isNotEmpty(Object[] entities){
		return !isEmpty(entities);
	}
	
	/** Test if entities array is empty.
	 *
	 * @param entities to test if it is empty.
	 * @return true if entities array is null or entities array length is zero.
	 */
	public static boolean isEmpty(Object[] entities){
		return entities == null || ( entities != null && entities.length == 0);
	}
}
