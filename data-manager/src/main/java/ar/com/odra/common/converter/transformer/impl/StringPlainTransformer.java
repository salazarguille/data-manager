package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectPlainTransformer;


/**
 *
 * @author William
 *
 */
public class StringPlainTransformer implements ObjectPlainTransformer {

	public static Map<Class<?>, Transformer<String, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<String, ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public StringPlainTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<String, Integer[]>(){
					public Integer[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, Integer.class, new Integer[values.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<String, int[]>(){
					public int[] transform(String string) {
						String[] valuesToConvert = string.split(_separator);
						int[] values = new int[valuesToConvert.length];
						for(int i = 0; i < valuesToConvert.length ; i++){
							values[i] = Converter.convertGeneric(String.class, valuesToConvert[i], int.class);
						}
						return values;
					}
				}
			);
		TRANSFORMERS.put(Long[].class, new Transformer<String, Long[]>(){

					public Long[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, Long.class, new Long[values.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<String, long[]>(){

			public long[] transform(String string) {
				String[] valuesToConvert = string.split(_separator);
				long[] values = new long[valuesToConvert.length];
				for(int i = 0; i < valuesToConvert.length ; i++){
					values[i] = Converter.convertGeneric(String.class, valuesToConvert[i], long.class);
				}
				return values;
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<String, Double[]>(){
					public Double[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, Double.class, new Double[values.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<String, double[]>(){

				public double[] transform(String string) {
					String[] valuesToConvert = string.split(_separator);
					double[] values = new double[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(String.class, valuesToConvert[i], double.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<String, Short[]>(){

					public Short[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, Short.class, new Short[values.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<String, short[]>(){

				public short[] transform(String string) {
					String[] valuesToConvert = string.split(_separator);
					short[] values = new short[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(String.class, valuesToConvert[i], short.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<String, Boolean[]>(){

					public Boolean[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, Boolean.class, new Boolean[values.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<String, boolean[]>(){

				public boolean[] transform(String string) {
					String[] valuesToConvert = string.split(_separator);
					boolean[] values = new boolean[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(String.class, valuesToConvert[i], boolean.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<String, Character[]>(){
					public Character[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, Character.class, new Character[values.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<String, char[]>(){
				public char[] transform(String string) {
					String[] valuesToConvert = string.split(_separator);
					char[] values = new char[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(String.class, valuesToConvert[i], char.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(String[].class, new Transformer<String, String[]>(){

					public String[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, String.class, new String[values.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<String, Float[]>(){

					public Float[] transform(String string) {
						String[] values = string.split(_separator);
						return convertArray(String.class, values, Float.class, new Float[values.length]);
					}
				}
			);
	}
	
	private static String _separator;
	public Transformer<String, ? extends Object> getTransformer(Class<?> clazz, String separator){
		_separator = separator;
		Transformer<String, ? extends Object> transformer = TRANSFORMERS.get(clazz);
		return transformer;
	}

	@Override
	public Transformer<? extends Object, ? extends Object> getTransformer(Class<?> clazz) {
		return TRANSFORMERS.get(clazz);
	}

}
