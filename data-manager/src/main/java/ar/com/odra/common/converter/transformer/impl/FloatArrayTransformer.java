package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class FloatArrayTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<Float[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Float[], ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public FloatArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Float[], Integer[]>(){
					public Integer[] transform(Float[] string) {
						return convertArray(Float.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Float[], Integer[]>(){
					public Integer[] transform(Float[] string) {
						return convertArray(Float.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Float[], Long[]>(){

					public Long[] transform(Float[] string) {
						return convertArray(Float.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Float[], Long[]>(){

			public Long[] transform(Float[] string) {
				return convertArray(Float.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Float[].class, new Transformer<Float[], Double[]>(){
					public Double[] transform(Float[] string) {
						return convertArray(Float.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Float[], Double[]>(){

				public Double[] transform(Float[] string) {
					return convertArray(Float.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Float[].class, new Transformer<Float[], Short[]>(){

					public Short[] transform(Float[] string) {
						return convertArray(Float.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Float[], Short[]>(){

				public Short[] transform(Float[] string) {
					return convertArray(Float.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Float[], Boolean[]>(){

					public Boolean[] transform(Float[] string) {
						return convertArray(Float.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Float[], Boolean[]>(){

				public Boolean[] transform(Float[] string) {
					return convertArray(Float.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Float[], Character[]>(){
					public Character[] transform(Float[] string) {
						return convertArray(Float.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Float[], Character[]>(){

				public Character[] transform(Float[] string) {
					return convertArray(Float.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Float[].class, new Transformer<Float[], String[]>(){

					public String[] transform(Float[] string) {
						return convertArray(Float.class, string, String.class, new String[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Float[], Float[]>(){

					public Float[] transform(Float[] string) {
						return convertArray(Float.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<Float[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}

}
