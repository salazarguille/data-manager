package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class CharacterTransformer implements ObjectTransformer{

	public static Map<Class<?>, Transformer<Character, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Character, ? extends Object>>();

	public CharacterTransformer(){
		super();
		TRANSFORMERS.put(Integer.class, new Transformer<Character, Integer>(){

					public Integer transform(Character object) {
						return Integer.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Long.class, new Transformer<Character, Long>(){

					public Long transform(Character object) {
						return Long.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Double.class, new Transformer<Character, Double>(){

					public Double transform(Character object) {
						return Double.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Short.class, new Transformer<Character, Short>(){

					public Short transform(Character object) {
						return Short.valueOf(String.valueOf(object));
					}
				}
			);
		TRANSFORMERS.put(Boolean.class, new Transformer<Character, Boolean>(){

					public Boolean transform(Character object) {
						return Boolean.valueOf(object.toString().equalsIgnoreCase("1")? true: false);
					}
				}
			);
		TRANSFORMERS.put(Character.class, new Transformer<Character, Character>(){

					public Character transform(Character object) {
						return Character.valueOf(String.valueOf(object).charAt(0));
					}
				}
			);
		TRANSFORMERS.put(String.class, new Transformer<Character, String>(){

					public String transform(Character object) {
						return object.toString();
					}
				}
			);
		TRANSFORMERS.put(Float.class, new Transformer<Character, Float>(){

					public Float transform(Character object) {
						return Float.valueOf(object.toString());
					}
				}
			);
	}

	public Transformer<Character, ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
