package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectPlainTransformer;


/**
 *
 * @author William
 *
 */
public class IntegerPlainTransformer implements ObjectPlainTransformer {

	public static Map<Class<?>, Transformer<Integer, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Integer, ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public IntegerPlainTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Integer, Integer[]>(){
					public Integer[] transform(Integer string) {
						return new Integer[]{string};
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Integer, int[]>(){
					public int[] transform(Integer string) {
						return new int[]{string};
					}
				}
			);
		TRANSFORMERS.put(Long[].class, new Transformer<Integer, Long[]>(){

					public Long[] transform(Integer string) {
						Integer[] values = new Integer[]{string};
						return convertArray(Integer.class, values, Long.class, new Long[values.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Integer, long[]>(){

			public long[] transform(Integer string) {
				Integer[] valuesToConvert = new Integer[]{string};

				long[] values = new long[valuesToConvert.length];
				for(int i = 0; i < valuesToConvert.length ; i++){
					values[i] = Converter.convertGeneric(Integer.class, valuesToConvert[i], long.class);
				}
				return values;
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<Integer, Double[]>(){
					public Double[] transform(Integer string) {
						Integer[] values = new Integer[]{string};
						return convertArray(Integer.class, values, Double.class, new Double[values.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Integer, double[]>(){

				public double[] transform(Integer string) {
					Integer[] valuesToConvert = new Integer[]{string};

					double[] values = new double[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(Integer.class, valuesToConvert[i], double.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Integer, Short[]>(){

					public Short[] transform(Integer string) {
						Integer[] values = new Integer[]{string};
						return convertArray(Integer.class, values, Short.class, new Short[values.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Integer, short[]>(){

				public short[] transform(Integer string) {
					Integer[] valuesToConvert = new Integer[]{string};

					short[] values = new short[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(Integer.class, valuesToConvert[i], short.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Integer, Boolean[]>(){

					public Boolean[] transform(Integer string) {
						Integer[] values = new Integer[]{string};
						return convertArray(Integer.class, values, Boolean.class, new Boolean[values.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Integer, boolean[]>(){

				public boolean[] transform(Integer string) {
					Integer[] valuesToConvert = new Integer[]{string};

					boolean[] values = new boolean[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(Integer.class, valuesToConvert[i], boolean.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Integer, Character[]>(){
					public Character[] transform(Integer string) {
						Integer[] values = new Integer[]{string};
						return convertArray(Integer.class, values, Character.class, new Character[values.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Integer, char[]>(){

				public char[] transform(Integer string) {
					Integer[] valuesToConvert = new Integer[]{string};

					char[] values = new char[valuesToConvert.length];
					for(int i = 0; i < valuesToConvert.length ; i++){
						values[i] = Converter.convertGeneric(Integer.class, valuesToConvert[i], char.class);
					}
					return values;
				}
			}
		);
		TRANSFORMERS.put(float[].class, new Transformer<Integer, float[]>(){
					public float[] transform(Integer string) {
						Integer[] valuesToConvert = new Integer[]{string};

						float[] values = new float[valuesToConvert.length];
						for(int i = 0; i < valuesToConvert.length ; i++){
							values[i] = Converter.convertGeneric(Integer.class, valuesToConvert[i], float.class);
						}
						return values;
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Integer, Float[]>(){

					public Float[] transform(Integer string) {
						Integer[] values = new Integer[]{string};
						return convertArray(Integer.class, values, Float.class, new Float[values.length]);
					}
				}
			);
	}
	
	public Transformer<Integer, ? extends Object> getTransformer(Class<?> clazz, String separator){
		return TRANSFORMERS.get(clazz);
	}

	@Override
	public Transformer<? extends Object, ? extends Object> getTransformer(Class<?> clazz) {
		return TRANSFORMERS.get(clazz);
	}

}
