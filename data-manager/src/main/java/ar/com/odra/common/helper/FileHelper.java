/**
 * 
 */
package ar.com.odra.common.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @author HP
 *
 */
public class FileHelper {

	private static final String getSystemCurrentDirectory(){
		String javaClassPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();//System.getProperty("java.class.path");
		int pos = javaClassPath.indexOf(";");
		if(pos != -1){
			javaClassPath = javaClassPath.substring(0, pos);
		}
		return javaClassPath;
	}
	
	public static InputStream findInputStreamInClasspath(String fullPathFile) throws FileNotFoundException{
		String classPath = getSystemCurrentDirectory() + fullPathFile;
		File file = new File(classPath);
		InputStream inputStream = new FileInputStream(file);
		return inputStream;
	}
}
