/**
 * 
 */
package ar.com.odra.common.helper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * @author HP
 *
 */
public class AnnotationHelper {

	public static boolean hasAnnotation(Method method, Class<? extends Annotation> annotation){
		return method != null && method.isAnnotationPresent(annotation);
	}
	
	public static boolean hasAnnotation(Class<?> clazz, Class<? extends Annotation> annotation){
		return clazz != null &&clazz.isAnnotationPresent(annotation);
	}
	
	public static <T extends Annotation> T getAnnotation(Class<?> clazz, Class<T> annotationClass){
		T annotation = null;
		if(hasAnnotation(clazz, annotationClass)){
			annotation = clazz.getAnnotation(annotationClass);
		}
		return annotation;
	}
}
