package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class ShortArrayTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<Short[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Short[], ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public ShortArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Short[], Integer[]>(){
					public Integer[] transform(Short[] string) {
						return convertArray(Short.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Short[], Integer[]>(){
					public Integer[] transform(Short[] string) {
						return convertArray(Short.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Short[].class, new Transformer<Short[], Long[]>(){

					public Long[] transform(Short[] string) {
						return convertArray(Short.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Short[], Long[]>(){

			public Long[] transform(Short[] string) {
				return convertArray(Short.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Short[].class, new Transformer<Short[], Double[]>(){
					public Double[] transform(Short[] string) {
						return convertArray(Short.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Short[], Double[]>(){

				public Double[] transform(Short[] string) {
					return convertArray(Short.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Short[], Short[]>(){

					public Short[] transform(Short[] string) {
						return convertArray(Short.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Short[], Short[]>(){

				public Short[] transform(Short[] string) {
					return convertArray(Short.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Short[], Boolean[]>(){

					public Boolean[] transform(Short[] string) {
						return convertArray(Short.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Short[], Boolean[]>(){

				public Boolean[] transform(Short[] string) {
					return convertArray(Short.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Short[], Character[]>(){
					public Character[] transform(Short[] string) {
						return convertArray(Short.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Short[], Character[]>(){

				public Character[] transform(Short[] string) {
					return convertArray(Short.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Short[], String[]>(){

					public String[] transform(Short[] string) {
						return convertArray(Short.class, string, String.class, new String[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Short[], Float[]>(){

					public Float[] transform(Short[] string) {
						return convertArray(Short.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<Short[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}

}
