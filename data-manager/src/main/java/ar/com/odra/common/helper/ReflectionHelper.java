/**
 * 
 */
package ar.com.odra.common.helper;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.log4j.Logger;

/**
 * @author HP
 *
 */
public class ReflectionHelper {
	
	public static final Logger LOGGER = Logger.getLogger(ReflectionHelper.class);

	/** Find all method for class.
	 *
	 * @param clazz to find its methods.
	 * @return a methods' array.
	 */
	public static Method[] getAllMethodFor(final Class<?> clazz){
		if(clazz != null){
			return clazz.getMethods();
		}
		return new Method[]{};
	}

	/** Find all method with a specific modifier at class.
	 *
	 * @param modifier to filter method.
	 * @param clazz to find its methods.
	 * @return methods' array.
	 */
	public static Method[] getAllMethodWith(final Modifier modifier, final Class<?> clazz){
		Method[] methods = getAllMethodFor(clazz);
		List<Method> listMethods = new ArrayList<Method>();
		for(Method m: methods){
			if(modifier.equals(m.getModifiers())){
				listMethods.add(m);
			}
		}
		return (Method[]) listMethods.toArray();
	}

	public static Method[] getAllMethodWith(final Class<? extends Annotation> annotation, final Class<?> clazz){
		Method[] methods = getAllMethodFor(clazz);
		List<Method> listMethods = new ArrayList<Method>();
		for(Method m: methods){
			if(AnnotationHelper.hasAnnotation(m, annotation)){
				listMethods.add(m);
			}
		}
		Method[] a = new Method[listMethods.size()];
		int i = 0;
		for(Method m : listMethods){
			a[i] = m;
			i++;
		}
		return a;
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Annotation> T getAnnotationOfType(final Collection<Annotation> annotationList, final Class<? extends Annotation> annotationClass) {
		return (T) CollectionUtils.find(annotationList, new Predicate<Annotation>() {
			@Override
			public boolean evaluate(final Annotation annotation) {
				return annotationClass.isAssignableFrom(annotation.getClass());
			}
		});
	}
	
	/** Create a new instance from clazz.
	 *
	 * @param clazz
	 * @return a new object.
	 */
	public static <T> T createObject(final Class<T> clazz) {
		T object = null;
		try {
			object = clazz.newInstance();
		} catch (InstantiationException e) {
			LOGGER.error("An error occurred at instatiation object. May be " + clazz + " haven't a default constructor.");
		} catch (IllegalAccessException e) {
			LOGGER.error("An illegal access occurred at object at class: '" + clazz + "'.");
		}
		return object;
	}

	/** Returns if clazz is a java primitive class. If clazz is a java primitive class, it returns true, otherwise
	 * it returns false.
	 *
	 * @param clazz
	 * @return true if a class is a primitive class. Otherwise it returns false.
	 */
	public static boolean isAJavaPrimitiveClass(final Class<?> clazz){
		return getAllJavaPrimitiveClasses().contains(clazz);
	}
	
	/** Get all numbers java primitives classes. They are:
	 * - int.class
	 * - long.class
	 * - dounle.class
	 * - short.class
	 *
	 * @return a list with all java number classes. It includes primitive number classes.
	 */
	public static List<Class<?>> getNumbersJavaPrimitiveClasses(){
		List<Class<?>> listClasses = new ArrayList<Class<?>>();
		listClasses.add(int.class);
		listClasses.add(Integer.class);
		listClasses.add(long.class);
		listClasses.add(Long.class);
		listClasses.add(double.class);
		listClasses.add(Double.class);
		listClasses.add(short.class);
		listClasses.add(Short.class);
		listClasses.add(float.class);
		listClasses.add(Float.class);
		return listClasses;
	}

	/** Get all other java primitives classes. They are:
	 *  - char.class
	 *  - boolean.class
	 *  - String.class
	 *
	 * @return a list with character, boolean and string classes. It includes primitives.
	 */
	public static List<Class<?>> getOtherJavaPrimitiveClasses(){
		List<Class<?>> listClasses = new ArrayList<Class<?>>();
		listClasses.add(char.class);
		listClasses.add(Character.class);
		listClasses.add(boolean.class);
		listClasses.add(Boolean.class);
		listClasses.add(String.class);
		return listClasses;
	}
	
	public static List<Class<?>> getAllJavaPrimitiveArrayClasses(){
		List<Class<?>> listAllClasses = getNumbersJavaPrimitiveArrayClasses();
		listAllClasses.addAll(getOtherJavaPrimitiveArrayClasses());
		return listAllClasses;
	}
	
	public static List<Class<?>> getNumbersJavaPrimitiveArrayClasses(){
		List<Class<?>> listClasses = new ArrayList<Class<?>>();
		listClasses.add(int[].class);
		listClasses.add(Integer[].class);
		listClasses.add(long[].class);
		listClasses.add(Long[].class);
		listClasses.add(double[].class);
		listClasses.add(Double[].class);
		listClasses.add(short[].class);
		listClasses.add(Short[].class);
		listClasses.add(float[].class);
		listClasses.add(Float[].class);
		return listClasses;
	}
	
	public static List<Class<?>> getOtherJavaPrimitiveArrayClasses(){
		List<Class<?>> listClasses = new ArrayList<Class<?>>();
		listClasses.add(char[].class);
		listClasses.add(Character[].class);
		listClasses.add(boolean[].class);
		listClasses.add(Boolean[].class);
		listClasses.add(String[].class);
		return listClasses;
	}
	
	/**
	 * 
	 * @param type
	 * @return true if class is a primitive array class. Otherwise it returns false.
	 */
	public static boolean isAJavaPrimitiveArrayClass(final Class<?> type) {
		return getAllJavaPrimitiveArrayClasses().contains(type);
	}
	/** Gett all java primitives classes. They are:
	 * - char.class
	 * - boolean.class
	 * - int.class
	 * - long.class
	 * - double.class
	 * - short.class
	 *
	 * @return a list with all basic java primitives classes.
	 */
	public static List<Class<?>> getAllJavaPrimitiveClasses(){
		List<Class<?>> listAllClasses = getNumbersJavaPrimitiveClasses();
		listAllClasses.addAll(getOtherJavaPrimitiveClasses());
		return listAllClasses;
	}
	
	/** Find at the object all method where its name be equals to the method name as parameter.
	 * 	It returns a list with all methods with the same name, but with differents parameters.
	 *
	 * @param obj
	 * @param methodName
	 * @return a list's method with method name as parameter.
	 */
	public static List<Method> getMethods(final Object obj, final String methodName){
		return getMethods(obj.getClass(), methodName);
	}
	
	/** Find at the class all methods where its name be equals to the method name as parameter.
	 * It returns a list with all methods with the same name, but with differents parameteres.
	 *
	 * @param clazz
	 * @param methodName
	 * @return a list's method with method name as parameter.
	 */
	public static List<Method> getMethods(final Class<?> clazz, final String methodName){
		Method[] arrayMethod = clazz.getMethods();
		List<Method> resultMethodList = new ArrayList<Method>();
		for(Method currentMethod: arrayMethod){
			if(currentMethod.getName().equalsIgnoreCase(methodName)){
				resultMethodList.add(currentMethod);
			}
		}
		return resultMethodList;
	}
}
