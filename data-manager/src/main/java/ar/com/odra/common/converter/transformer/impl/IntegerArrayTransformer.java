package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class IntegerArrayTransformer implements ObjectTransformer{

	public static Map<Class<?>, Transformer<Integer[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Integer[], ? extends Object>>();
	
	@SuppressWarnings("unchecked")
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convert(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public IntegerArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Integer[], Integer[]>(){
					public Integer[] transform(Integer[] string) {
						return convertArray(Integer.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Integer[], Integer[]>(){
					public Integer[] transform(Integer[] string) {
						return convertArray(Integer.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Long[].class, new Transformer<Integer[], Long[]>(){

					public Long[] transform(Integer[] string) {
						return convertArray(Integer.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Integer[], Long[]>(){

			public Long[] transform(Integer[] string) {
				return convertArray(Integer.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<Integer[], Double[]>(){
					public Double[] transform(Integer[] string) {
						return convertArray(Integer.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Integer[], Double[]>(){

				public Double[] transform(Integer[] string) {
					return convertArray(Integer.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Integer[], Short[]>(){

					public Short[] transform(Integer[] string) {
						return convertArray(Integer.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Integer[], Short[]>(){

				public Short[] transform(Integer[] string) {
					return convertArray(Integer.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Integer[], Boolean[]>(){

					public Boolean[] transform(Integer[] string) {
						return convertArray(Integer.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Integer[], Boolean[]>(){

				public Boolean[] transform(Integer[] string) {
					return convertArray(Integer.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Integer[], Character[]>(){
					public Character[] transform(Integer[] string) {
						return convertArray(Integer.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Integer[], Character[]>(){

				public Character[] transform(Integer[] string) {
					return convertArray(Integer.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(String[].class, new Transformer<Integer[], String[]>(){

					public String[] transform(Integer[] string) {
						return convertArray(Integer.class, string, String.class, new String[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Integer[], Float[]>(){

					public Float[] transform(Integer[] string) {
						return convertArray(Integer.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<Integer[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
