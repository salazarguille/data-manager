package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class LongTransformer implements ObjectTransformer{

	public static Map<Class<?>, Transformer<Long, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Long, ? extends Object>>();

	public LongTransformer(){
		super();
		TRANSFORMERS.put(Integer.class,
				new Transformer<Long, Integer>(){

					public Integer transform(Long object) {
						return Integer.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Long.class, new Transformer<Long, Long>(){

					public Long transform(Long object) {
						return Long.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Double.class, new Transformer<Long, Double>(){

					public Double transform(Long object) {
						return Double.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Short.class, new Transformer<Long, Short>(){

					public Short transform(Long object) {
						return Short.valueOf(String.valueOf(object));
					}
				}
			);
		TRANSFORMERS.put(Boolean.class, new Transformer<Long, Boolean>(){

					public Boolean transform(Long object) {
						return Boolean.valueOf(object.intValue() == 1? true: false);
					}
				}
			);
		TRANSFORMERS.put(Character.class, new Transformer<Long, Character>(){

					public Character transform(Long object) {
						return Character.valueOf(String.valueOf(object).charAt(0));
					}
				}
			);
		TRANSFORMERS.put(String.class, new Transformer<Long, String>(){

					public String transform(Long object) {
						return object.toString();
					}
				}
			);
		TRANSFORMERS.put(Float.class, new Transformer<Long, Float>(){

					public Float transform(Long object) {
						return Float.valueOf(object.toString());
					}
				}
			);
	}

	public Transformer<Long, ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
