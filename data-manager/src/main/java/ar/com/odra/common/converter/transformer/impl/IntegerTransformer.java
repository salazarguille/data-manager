package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class IntegerTransformer implements ObjectTransformer{

	public static Map<Class<?>, Transformer<Integer, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Integer, ? extends Object>>();

	public IntegerTransformer(){
		super();
		TRANSFORMERS.put(Integer.class,
				new Transformer<Integer, Integer>(){

					public Integer transform(Integer object) {
						return Integer.valueOf(object);
					}
				}
			);
		TRANSFORMERS.put(Long.class, new Transformer<Integer, Long>(){

					public Long transform(Integer object) {
						return Long.valueOf(object);
					}
				}
			);
		TRANSFORMERS.put(Double.class, new Transformer<Integer, Double>(){

					public Double transform(Integer object) {
						return Double.valueOf(object);
					}
				}
			);
		TRANSFORMERS.put(Short.class, new Transformer<Integer, Short>(){

					public Short transform(Integer object) {
						return Short.valueOf(String.valueOf(object));
					}
				}
			);
		TRANSFORMERS.put(Boolean.class, new Transformer<Integer, Boolean>(){

					public Boolean transform(Integer object) {
						return Boolean.valueOf(object.intValue() == 1? true: false);
					}
				}
			);
		TRANSFORMERS.put(Character.class, new Transformer<Integer, Character>(){

					public Character transform(Integer object) {
						return Character.valueOf(String.valueOf(object).charAt(0));
					}
				}
			);
		TRANSFORMERS.put(String.class, new Transformer<Integer, String>(){

					public String transform(Integer object) {
						return object.toString();
					}
				}
			);
		TRANSFORMERS.put(Float.class, new Transformer<Integer, Float>(){

					public Float transform(Integer object) {
						return Float.valueOf(object.toString());
					}
				}
			);
	}

	public Transformer<Integer, ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
