package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class DoubleTransformer implements ObjectTransformer{

	public static Map<Class<?>, Transformer<Double, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Double, ? extends Object>>();

	public DoubleTransformer(){
		super();
		TRANSFORMERS.put(Integer.class,
				new Transformer<Double, Integer>(){

					public Integer transform(Double object) {
						return Integer.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Long.class, new Transformer<Double, Long>(){

					public Long transform(Double object) {
						return Long.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Double.class, new Transformer<Double, Double>(){

					public Double transform(Double object) {
						return Double.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Short.class, new Transformer<Double, Short>(){

					public Short transform(Double object) {
						return Short.valueOf(String.valueOf(object));
					}
				}
			);
		TRANSFORMERS.put(Boolean.class, new Transformer<Double, Boolean>(){

					public Boolean transform(Double object) {
						return Boolean.valueOf(object.intValue() == 1? true: false);
					}
				}
			);
		TRANSFORMERS.put(Character.class, new Transformer<Double, Character>(){

					public Character transform(Double object) {
						return Character.valueOf(String.valueOf(object).charAt(0));
					}
				}
			);
		TRANSFORMERS.put(String.class, new Transformer<Double, String>(){

					public String transform(Double object) {
						return object.toString();
					}
				}
			);
		TRANSFORMERS.put(Float.class, new Transformer<Double, Float>(){

					public Float transform(Double object) {
						return Float.valueOf(object.toString());
					}
				}
			);
	}

	public Transformer<Double, ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
