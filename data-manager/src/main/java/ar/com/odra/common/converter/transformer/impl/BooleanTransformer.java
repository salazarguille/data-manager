package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class BooleanTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<Boolean, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Boolean, ? extends Object>>();

	public BooleanTransformer(){
		super();
		TRANSFORMERS.put(Integer.class,
				new Transformer<Boolean, Integer>(){

					public Integer transform(Boolean object) {
						return Integer.valueOf(object ? 1: 0);
					}
				}
			);
		TRANSFORMERS.put(Long.class, new Transformer<Boolean, Long>(){

					public Long transform(Boolean object) {
						return Long.valueOf(object ? 1: 0);
					}
				}
			);
		TRANSFORMERS.put(Double.class, new Transformer<Boolean, Double>(){

					public Double transform(Boolean object) {
						return Double.valueOf(object ? 1: 0);
					}
				}
			);
		TRANSFORMERS.put(Short.class, new Transformer<Boolean, Short>(){

					public Short transform(Boolean object) {
						return Short.valueOf((short) (object? 1:0));
					}
				}
			);
		TRANSFORMERS.put(Boolean.class, new Transformer<Boolean, Boolean>(){

					public Boolean transform(Boolean object) {
						return Boolean.valueOf(object != null ? Boolean.TRUE: false);
					}
				}
			);
		TRANSFORMERS.put(Character.class, new Transformer<Boolean, Character>(){

					public Character transform(Boolean object) {
						return Character.valueOf(object ? '1' : '0');
					}
				}
			);
		TRANSFORMERS.put(String.class, new Transformer<Boolean, String>(){

					public String transform(Boolean object) {
						return object.toString();
					}
				}
			);
		TRANSFORMERS.put(Float.class, new Transformer<Boolean, Float>(){

					public Float transform(Boolean object) {
						return Float.valueOf(object ? 1: 0);
					}
				}
			);
	}

	public Transformer<Boolean, ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
