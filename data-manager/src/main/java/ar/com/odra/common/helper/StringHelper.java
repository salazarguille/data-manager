/**
 * 
 */
package ar.com.odra.common.helper;

/**
 * @author HP
 *
 */
public class StringHelper {

	/** Define a Constant for a empty string. */
	public static final String EMPTY_STRING = "";
	
	/**  Verify it the string is empty. It returns true if the string is null or if string is equals "".
	 *
	 * @param string
	 * @return It returns true if the string is null or equals to "". Otherwise it return false.
	 */
	public static boolean isEmpty(String string){
		return string == null || string.equals(EMPTY_STRING) || (string!= null && string.trim().equals(EMPTY_STRING));
	}
	
	/** Test a string if empty o not.
	 *
	 * @param value to test if it is not empty.
	 * @return true it value is not empty. Otherwise it returns false.
	 */
	public static boolean isNotEmpty(String value){
		return !isEmpty(value);
	}
	
	public static String replaceAllAtLast(String value, String find, String toReplace) {
		if(StringHelper.isNotEmpty(value)){
			boolean ends = value.endsWith(find);
			while(value.length() > 0 && ends){
				value = replaceAtLast(value, find, toReplace);
				ends = endsWith(value, find);
			}
		}
		return value;
	}
	
	/** Verify if the string as parameter contains the character "character".
	 *
	 * @param string
	 * @param character
	 * @return Return true if the string contains the character "character". Otherwise it returns false.
	 */
	public static boolean containsCharacter(String string, Character character){
		boolean contains = false;
		int size = string != null ? string.length(): 0;
		char currentChar;
		for(int i = 0; i < size; i++){
			currentChar = string.charAt(i);
			if(Character.valueOf(currentChar).equals(character)){
				contains = true;
				i = size;
			}
		}
		return contains;
	}
	
	public static String replaceAllFirst(String string, String find, String toReplace){
		boolean begins = StringHelper.beginsAtLeast(string, find);
		while(string.length() > 0 && begins){
			string = StringHelper.replaceFirst(string, find, toReplace);
			begins = StringHelper.beginsAtLeast(string, find);
		}
		return string;
	}
	
	/** Verify if the string contains at least one character in the characterList.
	 *
	 * @param string
	 * @param charactersList
	 * @return It returns true if the string contains at least one character in the characterList. Otherwise
	 * 		   it returns false.
	 */
	public static boolean containsAtLeast(String string, String charactersList){
		boolean contains = false;
		CharSequence charSeq = charactersList.subSequence(0, charactersList.length());
		int size = charSeq.length();
		for(int i = 0; i < size; i++){
			if(containsCharacter(string, charSeq.charAt(i))){
			contains =  true;
			i = size;
			}
		}
		return contains;
	}
	
	public static boolean beginsAtLeast(String string, String charactersList){
		boolean begins = false;
		if(!isEmpty(string)){
			String firstChar = string.substring(0, 1);
			begins = containsAtLeast(firstChar, charactersList);
		}
		return begins;
	}
	
	/** Replace at the string the first string find by the string to replace.
	 *
	 * @param string
	 * @param find
	 * @param toReplace
	 * @return The string with its string replaced.
	 */
	public static String replaceFirst(String string, String find, String toReplace){
		if(!isEmpty(string)){
			int pos = string.indexOf(find);
			if(pos != -1){
				String begin = string.substring(0, pos);
				String end = string.substring(pos + find.length(), string.length() );
				return begin + toReplace + end;
			}
		}
		return string;
	}
	
	public static String replaceAtLast(String value, String find, String toReplace) {
		if(StringHelper.isNotEmpty(value)){
			boolean ends = value.endsWith(find);
			if(ends){
				String beginValue = value.substring(0, value.lastIndexOf(find));
				String endValue = toReplace; 
				value = beginValue + endValue;
			}
		}
		return value;
	}
	
	private static boolean endsWith(String value, String find) {
		return value != null ? value.endsWith(find): false;
	}
}
