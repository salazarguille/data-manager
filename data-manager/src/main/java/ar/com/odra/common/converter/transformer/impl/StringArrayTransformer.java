package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class StringArrayTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<String[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<String[], ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public StringArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<String[], Integer[]>(){
					public Integer[] transform(String[] string) {
						return convertArray(String.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<String[], Integer[]>(){
					public Integer[] transform(String[] string) {
						return convertArray(String.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Long[].class, new Transformer<String[], Long[]>(){

					public Long[] transform(String[] string) {
						return convertArray(String.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<String[], Long[]>(){

			public Long[] transform(String[] string) {
				return convertArray(String.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<String[], Double[]>(){
					public Double[] transform(String[] string) {
						return convertArray(String.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<String[], Double[]>(){

				public Double[] transform(String[] string) {
					return convertArray(String.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<String[], Short[]>(){

					public Short[] transform(String[] string) {
						return convertArray(String.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<String[], Short[]>(){

				public Short[] transform(String[] string) {
					return convertArray(String.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<String[], Boolean[]>(){

					public Boolean[] transform(String[] string) {
						return convertArray(String.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<String[], Boolean[]>(){

				public Boolean[] transform(String[] string) {
					return convertArray(String.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<String[], Character[]>(){
					public Character[] transform(String[] string) {
						return convertArray(String.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<String[], Character[]>(){

				public Character[] transform(String[] string) {
					return convertArray(String.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(String[].class, new Transformer<String[], String[]>(){

					public String[] transform(String[] string) {
						return string;
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<String[], Float[]>(){

					public Float[] transform(String[] string) {
						return convertArray(String.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<String[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}

}
