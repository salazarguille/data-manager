package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectTransformer;
import ar.com.odra.common.helper.StringHelper;


/**
 *
 * @author William
 *
 */
public class StringTransformer implements ObjectTransformer{

	public static Map<Class<?>, Transformer<String, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<String, ? extends Object>>();

	public StringTransformer(){
		super();
		TRANSFORMERS.put(Integer.class,
				new Transformer<String, Integer>(){

					public Integer transform(String string) {
						string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
						if(StringHelper.containsCharacter(string, '.')){
							string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
						}
						return !StringHelper.isEmpty(string) ? Integer.valueOf(string): 0;
					}
				}
			);
		TRANSFORMERS.put(int.class,
				new Transformer<String, Integer>(){

					public Integer transform(String string) {
						string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
						if(StringHelper.containsCharacter(string, '.')){
							string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
						}
						return !StringHelper.isEmpty(string) ? Integer.valueOf(string): 0;
					}
				}
			);
		TRANSFORMERS.put(Long.class, new Transformer<String, Long>(){

					public Long transform(String string) {
						string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
						if(StringHelper.containsCharacter(string, '.')){
							string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
						}
						return !StringHelper.isEmpty(string) ? Long.valueOf(string): 0L;
					}
				}
			);
		TRANSFORMERS.put(long.class, new Transformer<String, Long>(){

			public Long transform(String string) {
				string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
				if(StringHelper.containsCharacter(string, '.')){
					string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
				}
				return !StringHelper.isEmpty(string) ? Long.valueOf(string): 0L;
			}
		}
	);
		TRANSFORMERS.put(Double.class, new Transformer<String, Double>(){

					public Double transform(String string) {
						string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
						if(StringHelper.containsCharacter(string, '.')){
							string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
						}
						return !StringHelper.isEmpty(string) ? Double.valueOf(string): 0.0;
					}
				}
			);
		TRANSFORMERS.put(double.class, new Transformer<String, Double>(){

				public Double transform(String string) {
					string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
					if(StringHelper.containsCharacter(string, '.')){
						string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
					}
					return !StringHelper.isEmpty(string) ? Double.valueOf(string): 0.0;
				}
			}
		);
		TRANSFORMERS.put(Short.class, new Transformer<String, Short>(){

					public Short transform(String string) {
						string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
						if(StringHelper.containsCharacter(string, '.')){
							string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
						}
						return !StringHelper.isEmpty(string) ? Short.valueOf(string): 0;
					}
				}
			);
		TRANSFORMERS.put(short.class, new Transformer<String, Short>(){

				public Short transform(String string) {
					string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
					if(StringHelper.containsCharacter(string, '.')){
						string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
					}
					return !StringHelper.isEmpty(string) ? Short.valueOf(string): 0;
				}
			}
		);
		TRANSFORMERS.put(Boolean.class, new Transformer<String, Boolean>(){

					public Boolean transform(String string) {
						return Boolean.valueOf(string);
					}
				}
			);
		TRANSFORMERS.put(boolean.class, new Transformer<String, Boolean>(){

				public Boolean transform(String string) {
					return Boolean.valueOf(string);
				}
			}
		);
		TRANSFORMERS.put(Character.class, new Transformer<String, Character>(){

					public Character transform(String string) {
						return Character.valueOf( !StringHelper.isEmpty(string) ? string.charAt(0): ' ');
					}
				}
			);
		TRANSFORMERS.put(char.class, new Transformer<String, Character>(){

				public Character transform(String string) {
					return Character.valueOf( !StringHelper.isEmpty(string) ? string.charAt(0): ' ');
				}
			}
		);
		TRANSFORMERS.put(String.class, new Transformer<String, String>(){

					public String transform(String string) {
						return string.toString();
					}
				}
			);
		TRANSFORMERS.put(Float.class, new Transformer<String, Float>(){

					public Float transform(String string) {
						string = StringHelper.replaceAllFirst(string, "0", StringHelper.EMPTY_STRING);
						if(StringHelper.containsCharacter(string, '.')){
							string = StringHelper.replaceAllAtLast(string, "0", StringHelper.EMPTY_STRING);
						}
						return !StringHelper.isEmpty(string) ? Float.valueOf(string.toString()): 0;
					}
				}
			);
	}

	public Transformer<String, ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
