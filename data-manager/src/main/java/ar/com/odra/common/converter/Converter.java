package ar.com.odra.common.converter;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectPlainTransformer;
import ar.com.odra.common.converter.transformer.ObjectTransformer;
import ar.com.odra.common.converter.transformer.impl.BooleanArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.BooleanTransformer;
import ar.com.odra.common.converter.transformer.impl.CharacterArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.CharacterTransformer;
import ar.com.odra.common.converter.transformer.impl.DoubleArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.DoubleTransformer;
import ar.com.odra.common.converter.transformer.impl.FloatArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.FloatTransformer;
import ar.com.odra.common.converter.transformer.impl.IntegerArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.IntegerPlainTransformer;
import ar.com.odra.common.converter.transformer.impl.IntegerTransformer;
import ar.com.odra.common.converter.transformer.impl.LongArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.LongTransformer;
import ar.com.odra.common.converter.transformer.impl.ShortArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.ShortTransformer;
import ar.com.odra.common.converter.transformer.impl.StringArrayTransformer;
import ar.com.odra.common.converter.transformer.impl.StringPlainTransformer;
import ar.com.odra.common.converter.transformer.impl.StringTransformer;
import ar.com.odra.common.exception.CommonException;


/**
 *
 * @author William
 *
 */
public class Converter {
	private static BooleanTransformer BOOLEAN_TRANSFORMER = new BooleanTransformer();
	private static CharacterTransformer CHARACTER_TRANSFORMER = new CharacterTransformer();
	private static DoubleTransformer DOUBLE_TRANSFORMER = new DoubleTransformer();
	private static FloatTransformer FLOAT_TRANSFORMER = new FloatTransformer();
	private static IntegerTransformer INTEGER_TRANSFORMER = new IntegerTransformer();
	private static LongTransformer LONG_TRANSFORMER = new LongTransformer();
	private static ShortTransformer SHORT_TRANSFORMER = new ShortTransformer();
	private static StringTransformer STRING_TRANSFORMER = new StringTransformer();

	private static Map<Class<?>,ObjectTransformer> TRANSFORMERS = new HashMap<Class<?>, ObjectTransformer>();
	
	private static Map<Class<?>,ObjectPlainTransformer> PLAIN_TRANSFORMERS = new HashMap<Class<?>, ObjectPlainTransformer>();

	static {
		TRANSFORMERS.put(Boolean.class, new BooleanTransformer());
		TRANSFORMERS.put(boolean.class, new BooleanTransformer());
		TRANSFORMERS.put(Character.class, new CharacterTransformer());
		TRANSFORMERS.put(char.class, new CharacterTransformer());
		TRANSFORMERS.put(Double.class, new DoubleTransformer());
		TRANSFORMERS.put(double.class, new DoubleTransformer());
		TRANSFORMERS.put(Float.class, new FloatTransformer());
		TRANSFORMERS.put(float.class, new FloatTransformer());
		TRANSFORMERS.put(Integer.class, new IntegerTransformer());
		TRANSFORMERS.put(int.class, new IntegerTransformer());
		TRANSFORMERS.put(Long.class, new LongTransformer());
		TRANSFORMERS.put(long.class, new LongTransformer());
		TRANSFORMERS.put(Short.class, new ShortTransformer());
		TRANSFORMERS.put(short.class, new ShortTransformer());
		TRANSFORMERS.put(String.class, new StringTransformer());
		
		TRANSFORMERS.put(String[].class, new StringArrayTransformer());
		TRANSFORMERS.put(Boolean[].class, new BooleanArrayTransformer());
		TRANSFORMERS.put(boolean[].class, new BooleanArrayTransformer());
		TRANSFORMERS.put(Character[].class, new CharacterArrayTransformer());
		TRANSFORMERS.put(char[].class, new CharacterArrayTransformer());
		TRANSFORMERS.put(Double[].class, new DoubleArrayTransformer());
		TRANSFORMERS.put(double[].class, new DoubleArrayTransformer());
		TRANSFORMERS.put(Float[].class, new FloatArrayTransformer());
		TRANSFORMERS.put(float[].class, new FloatArrayTransformer());
		TRANSFORMERS.put(Integer[].class, new IntegerArrayTransformer());
		TRANSFORMERS.put(int[].class, new IntegerArrayTransformer());
		TRANSFORMERS.put(Long[].class, new LongArrayTransformer());
		TRANSFORMERS.put(long[].class, new LongArrayTransformer());
		TRANSFORMERS.put(Short[].class, new ShortArrayTransformer());
		TRANSFORMERS.put(short[].class, new ShortArrayTransformer());
		
		PLAIN_TRANSFORMERS.put(String.class, new StringPlainTransformer());
		PLAIN_TRANSFORMERS.put(Integer.class, new IntegerPlainTransformer());
	}

	private Converter(){
		super();
	}

	@SuppressWarnings("unchecked")
	public static <T extends Object, H extends Object> T convertGeneric(Class<H> primitiveClass, H object, Class<T> convertTo, String separator){
		ObjectPlainTransformer objectTransformer = (ObjectPlainTransformer) PLAIN_TRANSFORMERS.get(primitiveClass);
		Transformer<Object, Object> transformerImpl = (Transformer<Object, Object>) objectTransformer.getTransformer(convertTo, separator);
		if(transformerImpl == null){
			throw new CommonException("Cannot find transformer impl for convert [" + primitiveClass.getCanonicalName() + " to "+ convertTo.getCanonicalName() + "] in object class [" + object.getClass().getCanonicalName() + "].");
		}
		return (T) transformerImpl.transform(object);
	}
	
	@SuppressWarnings("unchecked")
	public static Object convert(Class<?> primitiveClass, Object object, Class<? extends Object> convertTo){
		ObjectTransformer objectTransformer = TRANSFORMERS.get(primitiveClass);
		Transformer<Object, Object> transformerImpl = (Transformer<Object, Object>) objectTransformer.getTransformer(convertTo);
		if(transformerImpl == null){
			objectTransformer = PLAIN_TRANSFORMERS.get(primitiveClass);
			transformerImpl = (Transformer<Object, Object>) objectTransformer.getTransformer(convertTo);
			if(transformerImpl == null){
				throw new CommonException("Cannot find transformer impl for convert [" + primitiveClass.getCanonicalName() + " to "+ convertTo.getCanonicalName() + "] in object class [" + object.getClass().getCanonicalName() + "].");
			}
		}
		return transformerImpl.transform(object);
	}

	@SuppressWarnings("unchecked")
	public static <T extends Object, H extends Object> T convertGeneric(Class<H> primitiveClass, H object, Class<T> convertTo){
		ObjectTransformer objectTransformer = TRANSFORMERS.get(primitiveClass);
		Transformer<Object, Object> transformerImpl = (Transformer<Object, Object>) objectTransformer.getTransformer(convertTo);
		if(transformerImpl == null){
			throw new CommonException("Cannot find transformer impl for convert [" + primitiveClass.getCanonicalName() + " to "+ convertTo.getCanonicalName() + "] in object class [" + object.getClass().getCanonicalName() + "].");
		}
		return (T) transformerImpl.transform(object);
	}

	public static Object convertTo(Boolean object, Class<? extends Object> converterTo){
		Transformer<Boolean, ? extends Object> c = BOOLEAN_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	public static Object convertTo(Character object, Class<? extends Object> converterTo){
		Transformer<Character, ? extends Object> c = CHARACTER_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	public static Object convertTo(Double object, Class<? extends Object> converterTo){
		Transformer<Double, ? extends Object> c = DOUBLE_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	public static Object convertTo(Float object, Class<? extends Object> converterTo){
		Transformer<Float, ? extends Object> c = FLOAT_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	public static Object convertTo(Integer object, Class<? extends Object> converterTo){
		Transformer<Integer, ? extends Object> c = INTEGER_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	public static Object convertTo(Long object, Class<? extends Object> converterTo){
		Transformer<Long, ? extends Object> c = LONG_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	public static Object convertTo(Short object, Class<? extends Object> converterTo){
		Transformer<Short, ? extends Object> c = SHORT_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	public static Object convertTo(String object, Class<? extends Object> converterTo){
		Transformer<String, ? extends Object> c = STRING_TRANSFORMER.getTransformer(converterTo);
		return  c.transform(object) ;
	}

	@SuppressWarnings("unchecked")
	public static Object convert(Class<?> importValueClass, Object object, Class<?> parameterClass, String separator) {
		ObjectPlainTransformer objectTransformer = (ObjectPlainTransformer) PLAIN_TRANSFORMERS.get(importValueClass);
		Transformer<Object, Object> transformerImpl = (Transformer<Object, Object>) objectTransformer.getTransformer(parameterClass, separator);
		if(transformerImpl == null){
			throw new CommonException("Cannot find transformer impl for convert [" + importValueClass.getCanonicalName() + " to "+ parameterClass.getCanonicalName() + "] in object class [" + object.getClass().getCanonicalName() + "].");
		}
		return transformerImpl.transform(object);
	}
}
