package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class CharacterArrayTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<Character[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Character[], ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public CharacterArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Character[], Integer[]>(){
					public Integer[] transform(Character[] string) {
						return convertArray(Character.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Character[], Integer[]>(){
					public Integer[] transform(Character[] string) {
						return convertArray(Character.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Long[].class, new Transformer<Character[], Long[]>(){

					public Long[] transform(Character[] string) {
						return convertArray(Character.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Character[], Long[]>(){

			public Long[] transform(Character[] string) {
				return convertArray(Character.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<Character[], Double[]>(){
					public Double[] transform(Character[] string) {
						return convertArray(Character.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Character[], Double[]>(){

				public Double[] transform(Character[] string) {
					return convertArray(Character.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Character[], Short[]>(){

					public Short[] transform(Character[] string) {
						return convertArray(Character.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Character[], Short[]>(){

				public Short[] transform(Character[] string) {
					return convertArray(Character.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Character[], Boolean[]>(){

					public Boolean[] transform(Character[] string) {
						return convertArray(Character.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Character[], Boolean[]>(){

				public Boolean[] transform(Character[] string) {
					return convertArray(Character.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Character[], Character[]>(){
					public Character[] transform(Character[] string) {
						return convertArray(Character.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Character[], Character[]>(){

				public Character[] transform(Character[] string) {
					return convertArray(Character.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Character[], Character[]>(){

					public Character[] transform(Character[] string) {
						return string;
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Character[], Float[]>(){

					public Float[] transform(Character[] string) {
						return convertArray(Character.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<Character[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}

}
