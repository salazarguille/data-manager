/**
 * 
 */
package ar.com.odra.common.exception;

/**
 * @author c.gusala
 *
 */
public class CommonException extends RuntimeException {

	private static final long serialVersionUID = 3364691575275781989L;

	public CommonException() {
		super();
	}
	
	public CommonException(String message) {
		super(message);
	}
	
	public CommonException(String message, Throwable cause) {
		super(message, cause);
	}
}
