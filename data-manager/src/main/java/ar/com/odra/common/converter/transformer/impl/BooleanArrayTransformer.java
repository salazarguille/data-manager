package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class BooleanArrayTransformer implements ObjectTransformer {

	public static Map<Class<?>, Transformer<Boolean[], ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Boolean[], ? extends Object>>();
	
	protected static <T extends Object, H extends Object> T[] convertArray(Class<H> classToConvert, H[] valuesToConvert, Class<T> valuesConverted, T[] values){
		for(int i = 0; i < valuesToConvert.length ; i++){
			values[i] = (T) Converter.convertGeneric(classToConvert, valuesToConvert[i], valuesConverted);
		}
		return values;
	}
	
	public BooleanArrayTransformer(){
		super();
		TRANSFORMERS.put(Integer[].class,
				new Transformer<Boolean[], Integer[]>(){
					public Integer[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, Integer.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(int[].class,
				new Transformer<Boolean[], Integer[]>(){
					public Integer[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, int.class, new Integer[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Long[].class, new Transformer<Boolean[], Long[]>(){

					public Long[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, Long.class, new Long[string.length]);
					}
				}
			);
		TRANSFORMERS.put(long[].class, new Transformer<Boolean[], Long[]>(){

			public Long[] transform(Boolean[] string) {
				return convertArray(Boolean.class, string, long.class, new Long[string.length]);
			}
		}
	);
		TRANSFORMERS.put(Double[].class, new Transformer<Boolean[], Double[]>(){
					public Double[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, Double.class, new Double[string.length]);
					}
				}
			);
		TRANSFORMERS.put(double[].class, new Transformer<Boolean[], Double[]>(){

				public Double[] transform(Boolean[] string) {
					return convertArray(Boolean.class, string, Double.class, new Double[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Short[].class, new Transformer<Boolean[], Short[]>(){

					public Short[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, Short.class, new Short[string.length]);
					}
				}
			);
		TRANSFORMERS.put(short[].class, new Transformer<Boolean[], Short[]>(){

				public Short[] transform(Boolean[] string) {
					return convertArray(Boolean.class, string, Short.class, new Short[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Boolean[], Boolean[]>(){

					public Boolean[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, Boolean.class, new Boolean[string.length]);
					}
				}
			);
		TRANSFORMERS.put(boolean[].class, new Transformer<Boolean[], Boolean[]>(){

				public Boolean[] transform(Boolean[] string) {
					return convertArray(Boolean.class, string, Boolean.class, new Boolean[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Character[].class, new Transformer<Boolean[], Character[]>(){
					public Character[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, Character.class, new Character[string.length]);
					}
				}
			);
		TRANSFORMERS.put(char[].class, new Transformer<Boolean[], Character[]>(){

				public Character[] transform(Boolean[] string) {
					return convertArray(Boolean.class, string, Character.class, new Character[string.length]);
				}
			}
		);
		TRANSFORMERS.put(Boolean[].class, new Transformer<Boolean[], String[]>(){

					public String[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, String.class, new String[string.length]);
					}
				}
			);
		TRANSFORMERS.put(Float[].class, new Transformer<Boolean[], Float[]>(){

					public Float[] transform(Boolean[] string) {
						return convertArray(Boolean.class, string, Float.class, new Float[string.length]);
					}
				}
			);
	}

	public Transformer<Boolean[], ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}

}
