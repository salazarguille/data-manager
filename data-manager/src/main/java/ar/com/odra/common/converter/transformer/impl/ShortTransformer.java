package ar.com.odra.common.converter.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import ar.com.odra.common.converter.transformer.ObjectTransformer;


/**
 *
 * @author William
 *
 */
public class ShortTransformer implements ObjectTransformer{

	public static Map<Class<?>, Transformer<Short, ? extends Object>> TRANSFORMERS = new HashMap<Class<?>, Transformer<Short, ? extends Object>>();

	public ShortTransformer(){
		super();
		TRANSFORMERS.put(Integer.class,
				new Transformer<Short, Integer>(){

					public Integer transform(Short object) {
						return Integer.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Long.class, new Transformer<Short, Long>(){

					public Long transform(Short object) {
						return Long.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Double.class, new Transformer<Short, Double>(){

					public Double transform(Short object) {
						return Double.valueOf(object.toString());
					}
				}
			);
		TRANSFORMERS.put(Short.class, new Transformer<Short, Short>(){

					public Short transform(Short object) {
						return Short.valueOf(String.valueOf(object));
					}
				}
			);
		TRANSFORMERS.put(Boolean.class, new Transformer<Short, Boolean>(){

					public Boolean transform(Short object) {
						return Boolean.valueOf(object.intValue() == 1? true: false);
					}
				}
			);
		TRANSFORMERS.put(Character.class, new Transformer<Short, Character>(){

					public Character transform(Short object) {
						return Character.valueOf(String.valueOf(object).charAt(0));
					}
				}
			);
		TRANSFORMERS.put(String.class, new Transformer<Short, String>(){

					public String transform(Short object) {
						return object.toString();
					}
				}
			);
		TRANSFORMERS.put(Float.class, new Transformer<Short, Float>(){

					public Float transform(Short object) {
						return Float.valueOf(object.toString());
					}
				}
			);
	}

	public Transformer<Short, ? extends Object> getTransformer(Class<?> clazz){
		return TRANSFORMERS.get(clazz);
	}
}
