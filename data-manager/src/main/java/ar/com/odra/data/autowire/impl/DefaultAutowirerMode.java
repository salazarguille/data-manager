/**
 * 
 */
package ar.com.odra.data.autowire.impl;

import ar.com.odra.data.autowire.AutowirerMode;

/**
 * @author c.gusala
 *
 */
public class DefaultAutowirerMode implements AutowirerMode {

	@Override
	public void autowire(Object resultado) {
		//DO Nothing.
	}

}
