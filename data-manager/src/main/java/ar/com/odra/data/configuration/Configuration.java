/**
 * 
 */
package ar.com.odra.data.configuration;

import java.lang.annotation.Annotation;
import java.util.Collection;

import ar.com.odra.data.autowire.AutowirerMode;
import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.factory.BeansLocatorFactory;
import ar.com.odra.data.factory.RowProcessorStrategyFactory;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.locator.BeansLocator;
import ar.com.odra.data.locator.Locatable;
import ar.com.odra.data.matcher.ParameterMatcheable;
import ar.com.odra.data.register.ConfigurationBeansRegister;
import ar.com.odra.data.register.FileTypesSupportableRegister;
import ar.com.odra.data.register.ParameterMatcheablesRegister;
import ar.com.odra.data.register.RowProcessorStrategyFactoryRegister;
import ar.com.odra.data.registry.FileTypeSupportableRegistry;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;
import ar.com.odra.data.registry.RowProcessorStrategyFactoryRegistry;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.Enviroment;
import ar.com.odra.data.utils.FileTypeSupportable;
import ar.com.odra.data.utils.Utils;



/**
 * Esta clase se encarga de tener toda la configuracion necesaria para poder ejecutar la importacion. En esta clase se define el
 * registro de los tipos de archivos reconocidos por el proceso, los parametros que son procesables (o matcheables) en los generadores
 * de datos, y los creadores de las estrategias de procesadores de registros de archivos.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see RowProcessorStrategyFactory
 * @see FileTypeSupportable
 * @see ParameterMatcheable
 */
public class Configuration {

	private RowProcessorStrategyFactoryRegistry strategyFactoryRegistry;
	private ParameterMatcheableRegistry			parameterMatcheableRegistry;
	private FileTypeSupportableRegistry			fileTypeRegistry;
	private BeansLocator						beansLocator;
	private AutowirerMode 						autowirerMode;
	private Enviroment							enviroment;
	
	public Configuration(BeansLocatorFactory beansLocatorFactory, ParameterMatcheablesRegister matcheablesRegister, FileTypesSupportableRegister fileTypesRegister, RowProcessorStrategyFactoryRegister strategyFactoryRegister, AutowirerMode autowirerMode, Enviroment enviroment) {
		super();
		this.setBeansLocator(beansLocatorFactory.createBeansLocator());
		this.setParameterMatcheableRegistry(new ParameterMatcheableRegistry());
		this.setFileTypeRegistry(new FileTypeSupportableRegistry());
		this.setStrategyFactoryRegistry(new RowProcessorStrategyFactoryRegistry());
		this.setEnviroment(enviroment);

		strategyFactoryRegister.registerRowProcessorStrategyFactory(this.getStrategyFactoryRegistry());
		matcheablesRegister.registerParameters(this.getParameterMatcheableRegistry());
		fileTypesRegister.registerFileType(this.getFileTypeRegistry());
		this.setAutowirerMode(autowirerMode);
	}
	
	public Configuration(ConfigurationBeansRegister configurationBeansRegister) {
		this(configurationBeansRegister, configurationBeansRegister, configurationBeansRegister, configurationBeansRegister, configurationBeansRegister, Utils.DUMMY_ENVIROMENT);
	}

	/**
	 * @return the autowirerMode
	 */
	public AutowirerMode getAutowirerMode() {
		return autowirerMode;
	}

	/**
	 * @param autowirerMode the autowirerMode to set
	 */
	public void setAutowirerMode(AutowirerMode autowirerMode) {
		this.autowirerMode = autowirerMode;
	}

	/**
	 * @return the strategyFactoryRegistry
	 */
	public RowProcessorStrategyFactoryRegistry getStrategyFactoryRegistry() {
		return strategyFactoryRegistry;
	}


	/**
	 * @param strategyFactoryRegistry the strategyFactoryRegistry to set
	 */
	public void setStrategyFactoryRegistry(RowProcessorStrategyFactoryRegistry strategyFactoryRegistry) {
		this.strategyFactoryRegistry = strategyFactoryRegistry;
	}


	/**
	 * @return the fileTypeRegistry
	 */
	public FileTypeSupportableRegistry getFileTypeRegistry() {
		return fileTypeRegistry;
	}

	/**
	 * @param fileTypeRegistry the fileTypeRegistry to set
	 */
	protected void setFileTypeRegistry(FileTypeSupportableRegistry fileTypeRegistry) {
		this.fileTypeRegistry = fileTypeRegistry;
	}

	/**
	 * @return the parameterMatcheableRegistry
	 */
	public ParameterMatcheableRegistry getParameterMatcheableRegistry() {
		return parameterMatcheableRegistry;
	}

	/**
	 * @param parameterMatcheableRegistry the parameterMatcheableRegistry to set
	 */
	protected void setParameterMatcheableRegistry(ParameterMatcheableRegistry parameterMatcheableRegistry) {
		this.parameterMatcheableRegistry = parameterMatcheableRegistry;
	}

	/**
	 * @return the beansLocator
	 */
	public BeansLocator getBeansLocator() {
		return beansLocator;
	}

	/**
	 * @param beansLocator the beansLocator to set
	 */
	public void setBeansLocator(BeansLocator beansLocator) {
		this.beansLocator = beansLocator;
	}

	public ParameterMatcheable getParameterMatcheable(ImportContext importContext, Class<?> parameterType, Annotation[] annotations, RowFileImport rowFileImport, int parameterPosition) {
		return this.getParameterMatcheableRegistry().getParameterMatcheable(importContext, parameterType, annotations, rowFileImport, parameterPosition);
	}

	public Collection<RowProcessorStrategy> createRowProcessorStrategies(Object rowStrategy) {
		return this.getStrategyFactoryRegistry().createRowProcessorStrategies(this.getParameterMatcheableRegistry(), rowStrategy);
	}

	public Locatable getBean(String value) {
		return this.getBeansLocator().locateBean(value);
	}
	
	public Locatable getBean(Class<?> type) {
		return this.getBeansLocator().locateBean(type);
	}
	
	public boolean hasBean(Class<?> type) {
		return this.getBeansLocator().canLocateBean(type);
	}
	
	public boolean hasBean(String value) {
		return this.getBeansLocator().canLocateBean(value);
	}

	public FileTypeSupportable getFileTypeSupportable(String contentType) {
		return getFileTypeRegistry().getFileTypeSupportable(contentType);
	}

	/**
	 * @return the enviroment
	 */
	protected Enviroment getEnviroment() {
		return enviroment;
	}

	/**
	 * @param enviroment the enviroment to set
	 */
	public void setEnviroment(Enviroment enviroment) {
		this.enviroment = enviroment;
	}
	
	public Enviroment getCurrentEnviroment() {
		if(this.getEnviroment() != null) {
			return this.getEnviroment();
		} else {
			return Utils.DUMMY_ENVIROMENT;
		}
	}
}
