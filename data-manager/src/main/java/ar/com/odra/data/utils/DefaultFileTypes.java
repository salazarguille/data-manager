/**
 * 
 */
package ar.com.odra.data.utils;

import java.util.Arrays;

import ar.com.odra.data.processor.FileProcessor;
import ar.com.odra.data.processor.RowProcessor;
import ar.com.odra.data.processor.impl.ExcelFileProcessor;
import ar.com.odra.data.processor.impl.ExcelRowProcessor;
import ar.com.odra.data.processor.impl.TextFileProcessor;
import ar.com.odra.data.processor.impl.TextRowProcessor;

/**
 * Enumerador que tiene la responsabilidad de centralizar los diferentes tipos de archivos que se pueden procesar.
 * Cada tipo de archivo existente debe saber crear un procesador para el archivo que maneja, y un procesador de fila (o registro)
 * del archivo. Ademas debe poseer un nombre identificatorio.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see FileProcessor
 * @see RowProcessor
 *
 */
public enum DefaultFileTypes implements FileTypeSupportable{
	/**
	 * Archivo de tipo texto. Instancia que procesa los archivos planos.
	 */
	TIPO_TEXTO("TIPO_TEXTO") {
		@Override
		public String getNombre() {
			return "Texto";
		}

		@Override
		public FileProcessor getProcesadorArchivo() {
			return new TextFileProcessor();
		}

		@Override
		public RowProcessor getProcesadorFila(FieldSeparator fieldSeparator) {
			return new TextRowProcessor(fieldSeparator);
		}
		
		@Override
		public String[] getFileExtensions() {
			return new String[] { "csv", "dat", "txt" };
		}
	},
	/**
	 * Archivo de tipo de excel. Instancia que procesa los archivos de microsoft excel, ya sea .xls, o xlsx.
	 */
	TIPO_EXCEL_97("TIPO_EXCEL_97") {
		@Override
		public FileProcessor getProcesadorArchivo() {
			return new ExcelFileProcessor();
		}

		@Override
		public String getNombre() {
			return "Excel97";
		}

		@Override
		public RowProcessor getProcesadorFila(FieldSeparator fieldSeparator) {
			return new ExcelRowProcessor();
		}
		
		@Override
		public String[] getFileExtensions() {
			return new String[] { "xls" };
		}
	},
	
	TIPO_EXCEL_2010("TIPO_EXCEL_2010") {
		@Override
		public FileProcessor getProcesadorArchivo() {
			return new ExcelFileProcessor();
		}

		@Override
		public String getNombre() {
			return "Excel2010";
		}

		@Override
		public RowProcessor getProcesadorFila(FieldSeparator fieldSeparator) {
			return new ExcelRowProcessor();
		}
		
		@Override
		public String[] getFileExtensions() {
			return new String[] { "xlsx" };
		}
	};

	/**
	 * Obtiene el nombre que identifica a este tipo de archivo.
	 * 
	 * @return un nombre identificatorio para este tipo de archivo.
	 */
	public abstract String getNombre();
	
	private String valor;
	
	DefaultFileTypes(String valor){
		this.valor = valor;
	}
	
	public String getValor() {
		return this.valor;
	}
	
	public abstract String[] getFileExtensions();

	public boolean supportFile(String fileName) {
		String fileExtension = Utils.getFileExtension(fileName);
		int index = Arrays.binarySearch(this.getFileExtensions(), fileExtension);
		return index > -1;
	}
}
