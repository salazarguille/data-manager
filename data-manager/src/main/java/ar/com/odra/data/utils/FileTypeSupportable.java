/**
 * 
 */
package ar.com.odra.data.utils;

import ar.com.odra.data.processor.FileProcessor;
import ar.com.odra.data.processor.RowProcessor;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface FileTypeSupportable {

	public String[] getFileExtensions();
	
	public boolean supportFile(String fileName);
	
	/**
	 * Obtiene un procesador de fila o registro del archivo que representa.
	 * 
	 * @return una instancia de procesador de registro o fila para el archivo que representa.
	 */
	public RowProcessor getProcesadorFila(FieldSeparator fieldSeparator);
	
	/**
	 * Obtiene un procesador del archivo que representa.
	 * 
	 * @return una instancia nueva del procesador del archivo que representa.
	 */
	public FileProcessor getProcesadorArchivo();

}
