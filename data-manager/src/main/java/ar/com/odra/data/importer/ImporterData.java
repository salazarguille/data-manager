/**
 * 
 */
package ar.com.odra.data.importer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.exception.ImporterDataInvalidFileException;
import ar.com.odra.data.exception.ImporterDataMaximumNoProcessedException;
import ar.com.odra.data.factory.StrategiesRowFactory;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.processor.FileProcessor;
import ar.com.odra.data.processor.RowProcessor;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.FileTypeSupportable;

/**
 * Clase principal del proceso de importacion de datos en objetos. Esta clase representa al proceso de importacion de datos en 
 * diferentes tipos de archivos, por ejemplo, excel, o planos. </br>
 * En el mismo se definen estrategias ({@link RowProcessorStrategy}) para procesar cada registro de los diferentes archivos.
 * NO es necesario crear una misma estrategia para los diferentes tipos de archivos. El importador de datos soporta la importacion 
 * de diferentes archivos con solo una estrategia.
 * </br>
 * </br>
 * Dado que un sistema puede utilizar muchas estrategias de procesadores de registros (o filas), se puede optar por utilizar un factory
 * {@link StrategiesRowFactory}, el cual permite crear una colecci�n de estrategias, facilitando la creacion del importador.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see RowProcessorStrategy
 * @see StrategiesRowFactory
 * 
 */
public class ImporterData {
	
	private static Logger LOGGER = Logger.getLogger(ImporterData.class);

	private Map<String, RowProcessorStrategy> estrategias;
	
	/**
	 * Crea una instancia del importador de datos, a partir del factory de estrategias de registros. Dichas 
	 * estrategias seran registradas en este importador.
	 * 
	 * @param estrategiasFactory creador de las estrategias de procesadores de registros (o filas).
	 */
	public ImporterData(StrategiesRowFactory estrategiasFactory) {
		this();
		this.registrarEstrategias(estrategiasFactory);
	}
	
	/**
	 * Crea una instancia del importador de datos sin ninguna estrategia de procesador de filas registrada.
	 * Para comenzar a utilizar la importacion se debera registrar una estrategia.
	 * 
	 * @see #registrarEstrategia(RowProcessorStrategy)
	 */
	public ImporterData() {
		super();
		this.setEstrategias(new HashMap<String, RowProcessorStrategy>());
	}
	
	/**
	 * @return the estrategias
	 */
	protected Map<String, RowProcessorStrategy> getEstrategias() {
		return estrategias;
	}

	/**
	 * @param estrategias the estrategias to set
	 */
	protected void setEstrategias(Map<String, RowProcessorStrategy> estrategias) {
		this.estrategias = estrategias;
	}

	/**
	 * Registra las estrategias de procesadores de registros (o filas) creadas a partir del factory. En el caso de no poseer
	 * una instancia del factory, se puede optar por utilizar {@link #registrarEstrategia(RowProcessorStrategy)}.
	 * Internamente por cada estrategia que creo el factory, se llama a la registracion {@link #registrarEstrategia(RowProcessorStrategy)}.
	 * 
	 * @param estrategiasFactory que crea la coleccion de estrategias.
	 * @throws RuntimeException cuando se registra una estrategia con un nombre ya existente.
	 * @see #registrarEstrategia(RowProcessorStrategy)
	 */
	public void registrarEstrategias(StrategiesRowFactory estrategiasFactory) {
		if(estrategiasFactory != null) {
			Collection<RowProcessorStrategy> estrategias = estrategiasFactory.crearEstrategias();
			for (RowProcessorStrategy rowProcessorStrategy : estrategias) {
				LOGGER.info("Registering strategy name [" + rowProcessorStrategy.getNombre() + "] for [" + rowProcessorStrategy + "].");
				this.registrarEstrategia(rowProcessorStrategy);
			}
		}
	}

	/**
	 * Registra una estrategia de procesador de registro (o fila) para poder ser utilizada en el proceso de importacion de datos.
	 * Internamente se verifica si el nombre de la estrategia ya fue registrada. En caso afirmativo, se lanza una excepcion.
	 *   
	 * @param estrategiaFila instancia de la estrategia de procesador de fila (o registro).
	 * @throws RuntimeException cuando se registra una estrategia ya existente.
	 */
	public void registrarEstrategia(RowProcessorStrategy estrategiaFila) {
		String nombreEstrategia = estrategiaFila.getNombre();
		boolean contieneEstrategia = this.contieneEstrategia(nombreEstrategia);
		if (contieneEstrategia) {
			throw new ImporterDataException(
					"Ya existe una estrategia de archivo con ese nombre ["
							+ nombreEstrategia + "].");
		}
		this.getEstrategias().put(nombreEstrategia, estrategiaFila);
	}

	protected void autowireProperties(Object dataGeneratorInstance) {
		//DO Nothing.
	}

	/**
	 * Importa los bytes de datos (de un archivo), y obtiene una coleccion de objetos (de tipo segun definida por la estrategia a utilizar).
	 * Internamente se crea una instancia de  {@link ByteArrayInputStream} y se llama a {@link #importar(InputStream, ImportContext)}.
	 * 
	 * @param <T> tipo de los elementos en la coleccion de datos importados.
	 * @param byteDatos a utilizar para el proceso de importacion.
	 * @param contexto de la importacion de datos.
	 * @return una instancia de resultados de importacion.
	 * @see ImportContext
	 */
	public <T, H> ImportResult<T, H> importar(byte[] byteDatos, ImportContext contexto) {
		return this.importar(new ByteArrayInputStream(byteDatos), contexto);
	}
	
	/**
	 * Importa el stream de datos (de un archivo), y obtiene una coleccion de objetos (de tipo segun definida por la estrategia
	 * a utilizar).
	 * 
	 * @param <T> tipo de los elementos en la coleccion de datos importados en el resultados de importacion.
	 * @param <H> tipo de los elementos no procesados de los registros de importacion.
	 * @param inputStream a utilizar para el proceso de importacion.
	 * @param contexto de la importacion de datos.
	 * @return una instancia de resultados de importacion.
	 */
	public <T, H> ImportResult<T, H> importar(InputStream inputStream, ImportContext contexto) {
		try {
			LOGGER.info("Iniciando importacion generica [" + contexto + "].");
			String nombreEstrategia = contexto.getNombreEstrategia();

			LOGGER.info("Obteniendo estrategia de importacion [" + nombreEstrategia + "].");
			RowProcessorStrategy estrategiaFila = this.getEstrategia(nombreEstrategia);

			FileTypeSupportable tipoArchivo = contexto.getTipoArchivo();

			FileProcessor procesadorArchivo = estrategiaFila.getFileProcessor(tipoArchivo);

			RowProcessor procesadorFila = estrategiaFila.getRowProcessor(tipoArchivo);
			
			procesadorArchivo.iniciarProceso(inputStream);
			
			ImportResult<T, H> datosImportados = procesadorArchivo.procesarArchivo(contexto, procesadorFila, estrategiaFila);
	
			procesadorArchivo.finalizarProceso();

			LOGGER.info("Finalizando importacion generica [" + contexto + "].");
			return datosImportados;
		} catch (ImporterDataInvalidFileException e){
			throw e;
		} catch(ImporterDataMaximumNoProcessedException e) {
			throw e;
		} catch (Exception e){
			throw new ImporterDataException("Error importando datos: [" + e.getMessage() + "].", e);
		}
	}

	/**
	 * Obtiene una estrategia de procesador de registro a partir de un nombre. En caso de no existir se arroja una excepcion.
	 * 
	 * @param nombre de la estrategia a buscar.
	 * @return una instancia de una estrategia de procesador de fila con el nombre buscado.
	 * @throws RuntimeException cuando no existe una estrategia con dicho nombre.
	 */
	protected RowProcessorStrategy getEstrategia(String nombre) {
		RowProcessorStrategy estrategiaProcesadorFila = this.getEstrategias().get(nombre);
		if (estrategiaProcesadorFila == null) {
			throw new ImporterDataException("Nombre de estrategia no encontrada [" + nombre + "].");
		}
		return estrategiaProcesadorFila;
	}

	/**
	 * Testea si existe una estrategia de procesador de registro (o fila) con un nombre determinado. 
	 * 
	 * @param nombre de la estrategia a testear.
	 * @return true si existe una estrategia registrada con dicho nombre. En otro caso, retorna false.
	 */
	public boolean contieneEstrategia(String nombre) {
		return this.getEstrategias().containsKey(nombre);
	}


	/**
	 * Obtiene la cantidad total de estrategias registradas actualmente.
	 * 
	 * @return la cantidad total de estrategias registradas.
	 */
	public int getCantidadEstrategias() {
		return this.getEstrategias().size();
	}
	
	protected RowProcessorStrategy[] getRowProcessorStrategiesArray() {
		int length = this.getCantidadEstrategias();
		RowProcessorStrategy[] sortStrategies = this.getEstrategias().values().toArray(new RowProcessorStrategy[length]);
		LOGGER.debug("Sorted: " + Arrays.toString(sortStrategies));
		return sortStrategies;
	}
}
