/**
 * 
 */
package ar.com.odra.data.utils;

import ar.com.odra.data.importer.ImporterData;

/**
 * El objetivo de esta clase es la ejecuccion de una accion para una instancia de tipo T, permitiendo que dicha accion
 * pueda devolver otra instancia de otro tipo H. Dado esto ademas de permitir ejecutar una accion para cada item, se 
 * puede transformar ese tipo T en otro tipo H.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see ImporterData
 *
 */
public interface ActionResult {

	/**
	 * Ejecuta una accion para un objeto (de tipo T) importado por el importador de datos. 
	 *  
	 * @param <T> tipo del elemento importado a ejecutar la accion.
	 * @param <H> tipo de retorno una vez ejecutada la accion. Puede ser del mismo tipo que el elemento importado.
	 * @param resultado instancia del elemento.
	 * @return una instancia de tipo H que es el resultado de ejecutar esta accion.
	 */
	public <T, H> H ejecutarAccion(T resultado);
}
