/**
 * 
 */
package ar.com.odra.data.autowire;


/**
 *
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 */
public interface AutowirerMode {

	public void autowire(Object bean);
}
