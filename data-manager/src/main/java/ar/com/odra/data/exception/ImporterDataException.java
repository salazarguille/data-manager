/**
 * 
 */
package ar.com.odra.data.exception;

/**
 * Excepcion no chequeada que se utiliza en el proceso de importacion.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class ImporterDataException extends RuntimeException {

	private static final long serialVersionUID = 6402251471604180102L;

	/**
	 * 
	 */
	public ImporterDataException() {
		super();
	}

	/**
	 * @param message
	 */
	public ImporterDataException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ImporterDataException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ImporterDataException(String message, Throwable cause) {
		super(message, cause);
	}
}
