package ar.com.odra.data.annotation.as;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ar.com.odra.data.utils.FieldSeparator;

/**
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE})
public @interface AsPlainSource {

	FieldSeparator	separator()		default FieldSeparator.COMMA;
	
	boolean			fixedWidth()	default false;
	
}
