/**
 * 
 */
package ar.com.odra.data.utils;

import org.apache.log4j.Logger;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.matcher.MatchParameterResult;
import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.strategy.AnnotatedRowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public enum BindMode {

	NONE("NONE") {
		@Override
		public boolean allowBinding() {
			return false;
		}

	},
	BY_NAME("BY_NAME"), BY_ANNOTATION("BY_ANNOTATION");

	private static Logger LOGGER = Logger.getLogger(BindMode.class);
	
	private String name;

	private BindMode(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public boolean allowBinding() {
		return true;
	}

	protected boolean appliesBinding(MatchResult matchResult, AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy, Object bean,
			RowFileImport rowFileImport, int position) {
		boolean allowBinding = this.allowBinding();
		boolean canUseBindMode = annotatedRowProcessorStrategy.canUseBindMode(matchResult, this, bean, rowFileImport, position);
		LOGGER.debug("Applies binding [" + position + "] ? - binding mode allow binding [" + allowBinding + "] - Row processor can use bind mode [" + canUseBindMode + "].");
		return allowBinding && canUseBindMode;
	}

	protected String getPropertyNameAt(MatchResult matchResult, AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy, Object bean,
			RowFileImport rowFileImport, int position) {
		return annotatedRowProcessorStrategy.getPropertyNameAt(matchResult, this, bean, rowFileImport, position);
	}

	public void bind(MatchResult matchResult, AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy, Object bean) {
		RowFileImport rowFileImport = matchResult.getRowFileImport();
		int parameterSize = annotatedRowProcessorStrategy.getBindParametersLength(matchResult);
		int currentPosition = 0;
		for (int position = 0; position <= parameterSize; position++) {
			MatchParameterResult matchParameterResult = matchResult.getParameterResultAt(position);
			if(matchParameterResult != null && matchParameterResult.appliesAsParameter()) {
				if (this.appliesBinding(matchResult, annotatedRowProcessorStrategy, bean, rowFileImport, currentPosition)) {
					String propertyName = this.getPropertyNameAt(matchResult, annotatedRowProcessorStrategy, bean, rowFileImport, currentPosition);
					LOGGER.info("Binding property [" + propertyName + "] in position [" + position + "].");
					Object value = matchParameterResult.getValue();
					LOGGER.debug("Setting property [" + propertyName + "] with value [" + value + "] in bean class instance [" + bean.getClass() + "]");
					Utils.setFieldValue(bean, propertyName, value);
				}
				currentPosition++;
			}
		}
	}
}
