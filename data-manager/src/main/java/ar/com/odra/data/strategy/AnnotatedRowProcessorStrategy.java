/**
 * 
 */
package ar.com.odra.data.strategy;

import java.io.InputStream;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import ar.com.odra.data.annotation.as.AsPlainSource;
import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.OnErrorHandlerGenerator;
import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.processor.RowProcessor;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;
import ar.com.odra.data.utils.BindMode;
import ar.com.odra.data.utils.Enviroment;
import ar.com.odra.data.utils.FieldSeparator;
import ar.com.odra.data.utils.FileTypeSupportable;
import ar.com.odra.data.utils.InvocationMethod;
import ar.com.odra.data.utils.Utils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public abstract class AnnotatedRowProcessorStrategy extends RowProcessorStrategy {

	private static final Logger 		LOGGER = Logger.getLogger(AnnotatedRowProcessorStrategy.class);

	private Object						dataGeneratorInstance;
	private DataGenerator				dataGenerator;
	private AsPlainSource				asPlainSource;
	private ParameterMatcheableRegistry	matcheableRegistry;
	private OnErrorHandlerGenerator		errorHandlerGenerator;
	private Method 						errorMethod;

	protected abstract	String	getAnnotatedSourceName();
	
	public abstract String getPropertyNameAt(MatchResult matchResult, BindMode bindMode, Object bean, RowFileImport rowFileImport, int position);
	
	public AnnotatedRowProcessorStrategy(Object dataGeneratorInstance, ParameterMatcheableRegistry matcheableRegistry, Method errorMethod) {
		super();
		this.setDataGeneratorInstance(dataGeneratorInstance);
		this.setMatcheableRegistry(matcheableRegistry);
		this.setErrorMethod(errorMethod);
		if(this.hasErrorMethod()) {
			this.setErrorHandlerGenerator(errorMethod.getAnnotation(OnErrorHandlerGenerator.class));
		}
		this.setDataGenerator(this.getDataGeneratorInstance().getClass().getAnnotation(DataGenerator.class));
		this.setAsPlainSource(this.getDataGeneratorInstance().getClass().getAnnotation(AsPlainSource.class));
	}
	
	/**
	 * @return the asPlainSource
	 */
	public AsPlainSource getAsPlainSource() {
		return asPlainSource;
	}

	/**
	 * @param asPlainSource the asPlainSource to set
	 */
	public void setAsPlainSource(AsPlainSource asPlainSource) {
		this.asPlainSource = asPlainSource;
	}

	/**
	 * @return the dataGenerator
	 */
	public DataGenerator getDataGenerator() {
		return dataGenerator;
	}

	/**
	 * @param dataGenerator the dataGenerator to set
	 */
	public void setDataGenerator(DataGenerator dataGenerator) {
		this.dataGenerator = dataGenerator;
	}

	/**
	 * @return the errorHandlerGenerator
	 */
	public OnErrorHandlerGenerator getErrorHandlerGenerator() {
		return errorHandlerGenerator;
	}

	/**
	 * @param errorHandlerGenerator the errorHandlerGenerator to set
	 */
	public void setErrorHandlerGenerator(OnErrorHandlerGenerator errorHandlerGenerator) {
		this.errorHandlerGenerator = errorHandlerGenerator;
	}

	/**
	 * @return the errorMethod
	 */
	protected Method getErrorMethod() {
		return errorMethod;
	}

	/**
	 * @param errorMethod the errorMethod to set
	 */
	public void setErrorMethod(Method errorMethod) {
		this.errorMethod = errorMethod;
	}

	protected boolean hasErrorMethod() {
		return this.getErrorMethod() != null;
	}
	
	/**
	 * @return the dataGeneratorInstance
	 */
	public Object getDataGeneratorInstance() {
		return dataGeneratorInstance;
	}

	/**
	 * @param dataGeneratorInstance the dataGeneratorInstance to set
	 */
	protected void setDataGeneratorInstance(Object dataGeneratorInstance) {
		this.dataGeneratorInstance = dataGeneratorInstance;
	}

	/**
	 * @return the matcheableRegistry
	 */
	public ParameterMatcheableRegistry getMatcheableRegistry() {
		return matcheableRegistry;
	}

	/**
	 * @param matcheableRegistry the matcheableRegistry to set
	 */
	protected void setMatcheableRegistry(ParameterMatcheableRegistry matcheableRegistry) {
		this.matcheableRegistry = matcheableRegistry;
	}
	
	@Override
	public Class<?> getDataGeneratorInstanceClass() {
		return this.getDataGeneratorInstance().getClass();
	}
	
	protected DataGenerator getDataGeneratorAnnotation() {
		return this.getDataGeneratorInstanceClass().getAnnotation(DataGenerator.class);
	}
	
	public BindMode getDataGeneratorBindMode() {
		return this.getDataGeneratorAnnotation().bindMode();
	}
	
	public BindMode getConfiguredBindMode() {
		return this.getDataGeneratorBindMode();
	}
	
	public String getNombre() {
		return this.getAnnotatedSourceName();
	}
	
	protected boolean canInvokeMethodGenerator() {
		return true;
	}
	
	protected boolean canInvokeErrorMethodGenerator() {
		return true;
	}

	@SuppressWarnings("unchecked")
	public <T> T procesarFila(ImportContext importContext, RowFileImport rowFileImport) {
		LOGGER.debug("Processing row for row file [" + rowFileImport + "].");
		Object bean = null;
		ParametersMatcher parametersMatcher = null;
		try {
			parametersMatcher = this.createParametersMatcher(importContext, rowFileImport);
			LOGGER.debug("Creating parameters matcher [" + parametersMatcher + "] for context and row.");
			MatchResult matchingResult = this.matchParameterMethod(parametersMatcher);
			bean = this.bindBeanProperties(matchingResult);
			LOGGER.debug("Binding parameters for bean [" + bean + "] with matching result [" + matchingResult + "].");
			if(this.canInvokeMethodGenerator()) {
				bean = this.invokeMethod(bean, parametersMatcher, matchingResult);
			}
		} catch (Exception e) {
			LOGGER.error("Error processing row [" + e.getMessage() + "].", e);
			throw new ImporterDataException("Error processing row [" + parametersMatcher + "]", e);
		}
		return (T) bean;
	}

	/**
	 * @param matchingResult
	 * @return
	 */
	protected Object bindBeanProperties(MatchResult matchingResult) {
		return matchingResult.bindBean(this);
	}

	/**
	 * @param parametersMatcher
	 * @return
	 */
	protected MatchResult matchParameterMethod(ParametersMatcher parametersMatcher) {
		return parametersMatcher.match();
	}

	/**
	 * @param bean
	 * @param parametersMatcher
	 * @param matchingResult
	 * @return
	 */
	protected Object invokeMethod(Object bean, ParametersMatcher parametersMatcher, MatchResult matchingResult) {
		Object dataGenerator = parametersMatcher.getDataGenerator();
		Method method = parametersMatcher.getMethod();
		Object[] values = matchingResult.getParameterValues();
		InvocationMethod invocationMethod = new InvocationMethod(bean, dataGenerator, method, values);
		bean = invocationMethod.invoke();
		return bean;
	}

	protected ParametersMatcher createParametersMatcher(ImportContext importContext, RowFileImport rowFileImport) {
		ParametersMatcher parametersMatcher = new ParametersMatcher();
		parametersMatcher.setDataGenerator(this.getDataGeneratorInstance());
		parametersMatcher.setRowFileImport(rowFileImport);
		parametersMatcher.setImportContext(importContext);
		return parametersMatcher;
	}
	
	protected ParametersMatcher createErrorParametersMatcher(ImportContext importContext, RowFileImport rowFileImport) {
		ParametersMatcher parametersMatcher = this.createParametersMatcher(importContext, rowFileImport);
		parametersMatcher.setMethod(this.getErrorMethod());
		return parametersMatcher;
	}

	@SuppressWarnings("unchecked")
	public <H> H procesarFilaErronea(ImportContext importContext, RowFileImport rowFileImport, Exception error) {
		Object bean = null;
		ParametersMatcher matchingContext = this.createErrorParametersMatcher(importContext, rowFileImport);
		if(matchingContext.hasMethod()) {
			MatchResult matchingResult = this.matchParameterMethod(matchingContext);
			if(this.canInvokeErrorMethodGenerator()) {
				bean = this.invokeErrorMethod(matchingContext, matchingResult);
			}
			return (H) bean;
		}
		return null;
	}

	/**
	 * @param matchingContext
	 * @param matchingResult
	 * @return
	 */
	protected Object invokeErrorMethod(ParametersMatcher matchingContext, MatchResult matchingResult) {
		Object dataGenerator = matchingContext.getDataGenerator();
		Method method = matchingContext.getMethod();
		Object[] values = matchingResult.getParameterValues();
		InvocationMethod invocationMethod = new InvocationMethod(dataGenerator, method, values);
		return invocationMethod.invoke();
	}

	public boolean canUseBindMode(MatchResult matchResult, BindMode bindMode, Object bean, RowFileImport rowFileImport, int position) {
		return true;
	}

	public int getBindParametersLength(MatchResult matchResult) {
		return matchResult.getParametersLength();
	}
	
	protected boolean hasDataGenerator() {
		return this.getDataGenerator() != null;
	}
	
	@Override
	public Class<?>[] getDependencies() {	
		return this.getDataGenerator().dependencies();
	}

	@Override
	public int compareTo(AnnotatedRowProcessorStrategy other) {
		if(this.hasDependency(other.getDataGeneratorInstanceClass())) {
			return 1;
		} else {
			if(other.hasDependency(this.getDataGeneratorInstanceClass())) {
				return -1;
			} else {
				return this.getOrder().compareTo(other.getOrder());
			}
		}
	}
	
	protected boolean hasInputStream() {
		return !Utils.isEmptyONull(this.getDataGenerator().source());
	}
	
	@Override
	public InputStream getSourceInputStream(Enviroment enviroment) {
		DataGenerator dataGenerator = this.getDataGenerator();
		return this.hasDataGenerator() ? super.getInputStream(enviroment, dataGenerator.source(), dataGenerator.resource()) : null;
	}
	
	@Override
	public String getResource() {
		return this.getDataGenerator().resource();
	}
	
	@Override
	public boolean hasHeader() {
		return this.getDataGenerator().hasHeader();
	}
	
	@Override
	public String getDescription() {
		return this.getDataGenerator().description();
	}
	
	@Override
	public boolean isSecurityFileError() {
		return this.getDataGenerator().isSecurityFileError();
	}
	
	@Override
	public int getMaximumNoProcess() {
		return this.getDataGenerator().maximumNoProcess();
	}
	
	protected boolean hasAsPlainSource() {
		return this.getAsPlainSource() != null;
	}
	
	@Override
	public RowProcessor getRowProcessor(FileTypeSupportable fileTypeSupportable) {
		FieldSeparator fieldSeparator = this.hasAsPlainSource() ? this.getAsPlainSource().separator() : FieldSeparator.COMMA;
		return fileTypeSupportable.getProcesadorFila(fieldSeparator);
	}
}
