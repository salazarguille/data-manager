/**
 * 
 */
package ar.com.odra.data.processor;

import java.io.InputStream;

import org.apache.log4j.Logger;

import ar.com.odra.data.importer.ImporterData;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public abstract class FileProcessor {

	private static Logger LOGGER = Logger.getLogger(ImporterData.class);

	public abstract void iniciarProceso(InputStream inputStream);
	
	protected abstract <T,H> void procesarPagina(int numeroPagina, ImportResult<T, H> resultadoImportacion, RowProcessor procesadorFila, RowProcessorStrategy estrategiaFila, ImportContext contexto);
	
	public void finalizarProceso() {
		//Do nothing.
	}
	
	public <T,H> ImportResult<T, H> procesarArchivo(ImportContext contexto, RowProcessor procesadorFila, RowProcessorStrategy estrategiaFila) {
		LOGGER.debug("Creando instancia de resultado de importacion para el contexto.");
		ImportResult<T, H> resultadoImportacion = this.createResultadoImportacion(contexto);
		
		Integer[] numerosPaginas = contexto.getPaginasAsArray();
		for(int numeroPagina : numerosPaginas) {
			LOGGER.debug("Procesando pagina [" + numeroPagina + "] con estrategia [" + estrategiaFila.getNombre() + "].");
			this.procesarPagina(numeroPagina, resultadoImportacion, procesadorFila, estrategiaFila, contexto);
		}
		return resultadoImportacion;
	}
	
	protected <T,H> ImportResult<T, H> createResultadoImportacion(ImportContext contexto) {
		return new ImportResult<T, H>(contexto);
	}
}
