/**
 * 
 */
package ar.com.odra.data.exception;

/**
 * Excepcion no chequeada que se utiliza en el proceso de importacion. La misma es lanzada
 * cuando hay problemas con el archivo a importar.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class IllegalAccessPropertyException extends InvalidSettingPropertyException {

	private static final long serialVersionUID = 6402251471604180102L;

	/**
	 * 
	 */
	public IllegalAccessPropertyException() {
		super();
	}

	/**
	 * @param message
	 */
	public IllegalAccessPropertyException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public IllegalAccessPropertyException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public IllegalAccessPropertyException(String message, Throwable cause) {
		super(message, cause);
	}
}
