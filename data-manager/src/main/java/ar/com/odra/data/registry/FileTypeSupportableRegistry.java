/**
 * 
 */
package ar.com.odra.data.registry;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ar.com.odra.data.exception.NotFileTypeSupportableException;
import ar.com.odra.data.utils.FileTypeSupportable;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class FileTypeSupportableRegistry {

	private Set<FileTypeSupportable> fileTypes;
	
	public FileTypeSupportableRegistry(FileTypeSupportable...fileTypeSupportables) {
		super();
		if(fileTypeSupportables != null && fileTypeSupportables.length > 0) {
			this.setFileTypes(new HashSet<FileTypeSupportable>(Arrays.asList(fileTypeSupportables)));
		} else {
			this.setFileTypes(new HashSet<FileTypeSupportable>());
		}
	}
	
	/**
	 * @return the fileTypes
	 */
	public Set<FileTypeSupportable> getFileTypes() {
		return fileTypes;
	}

	/**
	 * @param fileTypes the fileTypes to set
	 */
	protected void setFileTypes(Set<FileTypeSupportable> fileTypes) {
		this.fileTypes = fileTypes;
	}
	
	public void registerFileTypeSupportable(FileTypeSupportable fileTypeSupportable) {
		this.getFileTypes().add(fileTypeSupportable);
	}
	
	public void removeFileTypeSupportable(FileTypeSupportable fileTypeSupportable) {
		this.getFileTypes().remove(fileTypeSupportable);
	}

	public FileTypeSupportable getFileTypeSupportable(String contentType) {
		for (FileTypeSupportable fileTypeSupportable : this.getFileTypes()) {
			if(fileTypeSupportable.supportFile(contentType)) {
				return fileTypeSupportable;
			}
		}
		throw new NotFileTypeSupportableException("File type [" + contentType + "] is not supportable. Please, register others file type supportables.");
	}
}
