/**
 * 
 */
package ar.com.odra.data.exception;

/**
 * Excepcion no chequeada que se utiliza en el proceso de importacion.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class CannotAccessResourceException extends ImporterDataException {

	private static final long serialVersionUID = -7706358945209586045L;

	/**
	 * 
	 */
	public CannotAccessResourceException() {
		super();
	}

	/**
	 * @param message
	 */
	public CannotAccessResourceException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public CannotAccessResourceException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CannotAccessResourceException(String message, Throwable cause) {
		super(message, cause);
	}
}
