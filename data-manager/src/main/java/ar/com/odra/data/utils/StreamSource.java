/**
 * 
 */
package ar.com.odra.data.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;

import ar.com.odra.common.helper.FileHelper;
import ar.com.odra.data.exception.CannotAccessResourceException;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public enum StreamSource {

	FILE("FILE"),
	CLASSPATH("CLASSPATH") {
		@Override
		public InputStream getInputStream(String location) {
			try {
				return FileHelper.findInputStreamInClasspath("//" + location);
			} catch (FileNotFoundException e) {
				throw new CannotAccessResourceException("Cannot access resource in classpath [" + location + "].", e);
			}
		}
	},
	URL("URL") {
		@Override
		public InputStream getInputStream(String location) {
			ByteArrayOutputStream outputStream = null;
			InputStream urlInputStream = null;
			URL url = null;
			try {
				outputStream = new ByteArrayOutputStream();
				url = new URL(location);
				URLConnection conn = url.openConnection();
				urlInputStream = conn.getInputStream();
				IOUtils.copy(urlInputStream, outputStream);
				return new ByteArrayInputStream(outputStream.toByteArray());
			} catch (MalformedURLException e) {
				throw new CannotAccessResourceException("Cannot access resource [" + location + "].", e);
			} catch (IOException e) {
				throw new CannotAccessResourceException("Cannot access resource [" + location + "].", e);
			} finally {
				if(urlInputStream != null) {
					IOUtils.closeQuietly(urlInputStream);
				}
				if(outputStream != null) {
					IOUtils.closeQuietly(outputStream);
				}
			}
		}
	};

	private String name;

	private StreamSource(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public InputStream getInputStream(String location) {
		try {
			return new FileInputStream(location);
		} catch (FileNotFoundException e) {
			throw new CannotAccessResourceException("Cannot access resource [" + location + "].", e);
		}
	}
}
