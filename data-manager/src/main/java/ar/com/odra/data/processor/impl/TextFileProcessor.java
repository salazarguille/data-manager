/**
 * 
 */
package ar.com.odra.data.processor.impl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.processor.FileProcessor;
import ar.com.odra.data.processor.RowProcessor;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * El objetivo de esta clase es procesar archivos de texto para la importacion de datos.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see FileProcessor
 * 
 */
public class TextFileProcessor extends FileProcessor {

	private static Logger LOGGER = Logger.getLogger(TextFileProcessor.class);

	private BufferedReader bufferedReader;
	private DataInputStream dataInputStream;

	public void iniciarProceso(InputStream inputStream) {
		dataInputStream = new DataInputStream(inputStream);
		bufferedReader = new BufferedReader(new InputStreamReader(
				dataInputStream));
	}

	protected <T,H> void procesarPagina(int numeroPagina, ImportResult<T, H> resultadoImportacion, RowProcessor procesadorFila, RowProcessorStrategy estrategiaFila, ImportContext contexto){
		String linea = null;
		try {
			// Read File Line By Line
			int indice = 1;
			while ((linea = bufferedReader.readLine()) != null) {
				LOGGER.debug("Creando instancia de registro numero [" + indice + "].");
				RowFileImport filaArchivo = procesadorFila.crearFilaArchivo(linea, indice);
				
				LOGGER.debug("Procesando instancia de registro numero [" + indice + "].");
				procesadorFila.procesarFila(resultadoImportacion, contexto, filaArchivo, estrategiaFila);
				
				indice++;
			}
		} catch (Exception e) {
			throw new ImporterDataException("Error leyendo archivo de texto: ["
					+ e.getMessage() + "].", e);
		} finally {
			// Close the input stream
			try {
				dataInputStream.close();
				bufferedReader.close();
			} catch (IOException e) {
				throw new ImporterDataException(
						"Error cerrandon archivo de texto: [" + e.getMessage()
								+ "].", e);
			}
		}

	}
}
