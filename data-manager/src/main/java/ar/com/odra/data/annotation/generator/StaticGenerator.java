package ar.com.odra.data.annotation.generator;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import ar.com.odra.data.utils.StreamSource;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface StaticGenerator {

	String			name();
	
	Class<?>		beanClass();
	
	String			resource();
	
	StreamSource 	source()	default StreamSource.CLASSPATH;
	
	int 			order()		default -1;
	
	int[] 			pages()		default {1};
	
	String[] 		parameters();

	boolean			hasHeader()		default true;

	String			description()	default "";
}
