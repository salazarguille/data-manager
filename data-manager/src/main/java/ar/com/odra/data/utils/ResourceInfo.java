/**
 * 
 */
package ar.com.odra.data.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import ar.com.odra.data.configuration.Configuration;
import ar.com.odra.data.exception.CannotAccessResourceException;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public class ResourceInfo {
	
	private static final Logger LOGGER = Logger.getLogger(ResourceInfo.class);

	private ByteArrayOutputStream outputStream;
	
	private String resource;

	public ResourceInfo(Configuration configuration, RowProcessorStrategy rowProcessorStrategy) {
		super();
		this.findSource(configuration, rowProcessorStrategy);
	}

	/**
	 * @return the resource
	 */
	public String getResource() {
		return resource;
	}

	/**
	 * @param resource the resource to set
	 */
	protected void setResource(String resource) {
		this.resource = resource;
	}

	protected void findSource(Configuration configuration, RowProcessorStrategy rowProcessorStrategy) {
		InputStream inputStream = null;
		try {
			LOGGER.info("Getting resource for row strategy [" + rowProcessorStrategy + "].");
			String resource = rowProcessorStrategy.getResource();
			this.setResource(resource);
			Enviroment enviroment = configuration.getCurrentEnviroment();
			LOGGER.debug("Enviroment selected [" + enviroment + "] to find resource.");
			inputStream = rowProcessorStrategy.getSourceInputStream(enviroment);
			
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			IOUtils.copy(inputStream, outputStream);
			this.setOutputStream(outputStream);
		} catch (IOException e) {
			LOGGER.error("Cannot access resource [" + e.getMessage() + "].", e);
			throw new CannotAccessResourceException(e);
		} finally {
			if (inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
		}
	}

	/**
	 * @return the outputStream
	 */
	protected ByteArrayOutputStream getOutputStream() {
		return outputStream;
	}

	/**
	 * @param outputStream
	 *            the outputStream to set
	 */
	protected void setOutputStream(ByteArrayOutputStream outputStream) {
		this.outputStream = outputStream;
	}

	public InputStream createInputStream() {
		return new ByteArrayInputStream(this.getOutputStream().toByteArray());
	}

	public String getExtension() {
		return Utils.getFileExtension(this.getResource());
	}

	protected boolean hasStream() {
		return this.getOutputStream() != null;
	}

	public void close() {
		if (this.hasStream()) {
			IOUtils.closeQuietly(this.getOutputStream());
		}
	}
}
