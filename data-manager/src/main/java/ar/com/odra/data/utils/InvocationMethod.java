/**
 * 
 */
package ar.com.odra.data.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import ar.com.odra.data.exception.InvalidInvocationGeneratorException;
import ar.com.odra.data.exception.NotBeanCreatedException;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class InvocationMethod {

	private Object		bean;
	private Object		dataGenerator;
	private Method		method;
	private Object[]	values;
	
	public InvocationMethod(Object bean, Object dataGenerator, Method method, Object[] values) {
		super();
		this.setBean(bean);
		this.setDataGenerator(dataGenerator);
		this.setMethod(method);
		this.setValues(values);
	}
	
	public InvocationMethod(Object dataGenerator, Method method, Object[] values) {
		this(null, dataGenerator, method, values);
	}

	/**
	 * @return the bean
	 */
	public Object getBean() {
		return bean;
	}

	/**
	 * @param bean the bean to set
	 */
	public void setBean(Object bean) {
		this.bean = bean;
	}

	/**
	 * @return the dataGenerator
	 */
	public Object getDataGenerator() {
		return dataGenerator;
	}

	/**
	 * @param dataGenerator the dataGenerator to set
	 */
	public void setDataGenerator(Object dataGenerator) {
		this.dataGenerator = dataGenerator;
	}

	/**
	 * @return the method
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(Method method) {
		this.method = method;
	}

	/**
	 * @return the values
	 */
	public Object[] getValues() {
		return values;
	}

	/**
	 * @param values the values to set
	 */
	public void setValues(Object[] values) {
		this.values = values;
	}
	
	public Object invoke() {
		Object result = null;
		Object[] values = this.getValues();
		String valuesString = Arrays.toString(values);
		try {
			result = this.getMethod().invoke(this.getDataGenerator(), values );
			if(result == null && !this.hasBean()) {
				throw new NotBeanCreatedException("Not bean created in [" + this.getMethodName() + "].");
			}
			return this.hasBean() ? this.getBean() : result;
		} catch (IllegalArgumentException e) {
			throw new InvalidInvocationGeneratorException("Illegal arguments in [" + this.getMethodName() + "] - Values [" + valuesString + "].", e);
		} catch (IllegalAccessException e) {
			throw new InvalidInvocationGeneratorException("Illegal access in [" + this.getMethodName() + "] - Values [" + valuesString + "].", e);
		} catch (InvocationTargetException e) {
			throw new InvalidInvocationGeneratorException("Invocation target exception [" + this.getMethodName() + "] - Values [" + valuesString + "].", e);
		}
	}

	public boolean hasBean() {
		return this.getBean() != null;
	}

	public String getMethodName() {
		return "" + this.getMethod();
	}
}
