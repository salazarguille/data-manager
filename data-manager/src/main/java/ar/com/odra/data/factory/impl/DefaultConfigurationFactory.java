/**
 * 
 */
package ar.com.odra.data.factory.impl;

import ar.com.odra.data.autowire.AutowirerMode;
import ar.com.odra.data.autowire.impl.DefaultAutowirerMode;
import ar.com.odra.data.configuration.Configuration;
import ar.com.odra.data.factory.BeansLocatorFactory;
import ar.com.odra.data.factory.ConfigurationFactory;
import ar.com.odra.data.register.FileTypesSupportableRegister;
import ar.com.odra.data.register.ParameterMatcheablesRegister;
import ar.com.odra.data.register.RowProcessorStrategyFactoryRegister;
import ar.com.odra.data.register.impl.DefaultFileTypesSupportableRegister;
import ar.com.odra.data.register.impl.DefaultParameterMatcheablesRegister;
import ar.com.odra.data.register.impl.DefaultRowProcessorStrategyFactoryRegister;
import ar.com.odra.data.utils.Enviroment;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class DefaultConfigurationFactory implements ConfigurationFactory {

	private ParameterMatcheablesRegister 		matcheablesRegister;
	private FileTypesSupportableRegister 		fileTypesRegister;
	private RowProcessorStrategyFactoryRegister	strategyFactoryRegister;
	private BeansLocatorFactory 				beansLocatorFactory;
	private AutowirerMode						autowirerMode;
	private Enviroment							currentEnviroment;
	
	public DefaultConfigurationFactory(BeansLocatorFactory beansLocatorFactory, ParameterMatcheablesRegister matcheablesRegister, FileTypesSupportableRegister fileTypesRegister, RowProcessorStrategyFactoryRegister strategyFactoryRegister, AutowirerMode autowirerMode) {
		super();
		this.beansLocatorFactory = beansLocatorFactory;
		this.matcheablesRegister = matcheablesRegister;
		this.fileTypesRegister = fileTypesRegister;
		this.strategyFactoryRegister = strategyFactoryRegister;
		this.autowirerMode = autowirerMode;
	}
	
	public DefaultConfigurationFactory(BeansLocatorFactory beansLocatorFactory) {
		this(beansLocatorFactory, new DefaultParameterMatcheablesRegister(), new DefaultFileTypesSupportableRegister(), new DefaultRowProcessorStrategyFactoryRegister(), new DefaultAutowirerMode());
	}
	
	public Configuration createConfiguration() {
		return new Configuration(this.beansLocatorFactory, this.matcheablesRegister, this.fileTypesRegister, this.strategyFactoryRegister, this.autowirerMode, this.getCurrentEnviroment());
	}
	

	/**
	 * @return the currentEnviroment
	 */
	public Enviroment getCurrentEnviroment() {
		return currentEnviroment;
	}

	/**
	 * @param currentEnviroment the currentEnviroment to set
	 */
	public void setCurrentEnviroment(Enviroment currentEnviroment) {
		this.currentEnviroment = currentEnviroment;
	}

	/**
	 * @return the matcheablesRegister
	 */
	public ParameterMatcheablesRegister getMatcheablesRegister() {
		return matcheablesRegister;
	}

	/**
	 * @param matcheablesRegister the matcheablesRegister to set
	 */
	public void setMatcheablesRegister(
			ParameterMatcheablesRegister matcheablesRegister) {
		this.matcheablesRegister = matcheablesRegister;
	}

	/**
	 * @return the fileTypesRegister
	 */
	public FileTypesSupportableRegister getFileTypesRegister() {
		return fileTypesRegister;
	}

	/**
	 * @param fileTypesRegister the fileTypesRegister to set
	 */
	public void setFileTypesRegister(FileTypesSupportableRegister fileTypesRegister) {
		this.fileTypesRegister = fileTypesRegister;
	}

	/**
	 * @return the strategyFactoryRegister
	 */
	public RowProcessorStrategyFactoryRegister getStrategyFactoryRegister() {
		return strategyFactoryRegister;
	}

	/**
	 * @param strategyFactoryRegister the strategyFactoryRegister to set
	 */
	public void setStrategyFactoryRegister(
			RowProcessorStrategyFactoryRegister strategyFactoryRegister) {
		this.strategyFactoryRegister = strategyFactoryRegister;
	}

	/**
	 * @return the beansLocatorFactory
	 */
	public BeansLocatorFactory getBeansLocatorFactory() {
		return beansLocatorFactory;
	}

	/**
	 * @param beansLocatorFactory the beansLocatorFactory to set
	 */
	public void setBeansLocatorFactory(BeansLocatorFactory beansLocatorFactory) {
		this.beansLocatorFactory = beansLocatorFactory;
	}

	/**
	 * @return the autowirerMode
	 */
	public AutowirerMode getAutowirerMode() {
		return autowirerMode;
	}

	/**
	 * @param autowirerMode the autowirerMode to set
	 */
	public void setAutowirerMode(AutowirerMode autowirerMode) {
		this.autowirerMode = autowirerMode;
	}
}
