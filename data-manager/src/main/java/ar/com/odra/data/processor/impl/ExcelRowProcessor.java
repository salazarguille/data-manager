/**
 * 
 */
package ar.com.odra.data.processor.impl;

import org.apache.poi.ss.usermodel.Row;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.content.impl.ExcelRowFile;
import ar.com.odra.data.processor.RowProcessor;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class ExcelRowProcessor extends RowProcessor {

	public RowFileImport crearFilaArchivo(Object fila, int indice) {
		return new ExcelRowFile((Row) fila);
	}
}
