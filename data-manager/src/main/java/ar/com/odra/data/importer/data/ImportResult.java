/**
 * 
 */
package ar.com.odra.data.importer.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.exception.ImporterDataMaximumNoProcessedException;

/**
 * Clase que representa un resultado de importacion de datos. Una instancia de esta clase contendra todos los registros del archivo
 * importado, ya sean procesados, o no procesados por algun error.</br>
 * Opcionalmente se puede setear un maximo para los registros (o filas) que se aceptan como no procesadas. Al llegar a dicho maximo
 * y ocurrir un error en algun registro, la importacion finalizara por medio de una excepcion {@link ImporterDataException}.
 * Si el maximo no procesa es -1, no se tomara en cuenta el chequeo de las filas no procesadas.
 * 
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see ImporterDataException
 * 
 */
public class ImportResult<T, H> {

	private Collection<T> filasProcesadas;
	
	private Collection<H> filasNoProcesadas;
	
	private long maximoNoProcesado;
	
	private ImportContext contexto;
	
	/**
	 * Crea una instancia del resutado de imptoracion para un determinado contexto de importacion. Ademas se le setea un maximo
	 * para filas no procesadas.
	 * 
	 * @param contexto de la importacion de datos.
	 */
	public ImportResult( ImportContext contexto) {
		super();
		this.setMaximoNoProcesado(contexto.getMaximoNoProcesado());
		this.setFilasNoProcesadas(new ArrayList<H>());
		this.setFilasProcesadas(new ArrayList<T>());
		this.setContexto(contexto);
	}
	
	/**
	 * @return the contexto
	 */
	protected ImportContext getContexto() {
		return contexto;
	}

	/**
	 * @param contexto the contexto to set
	 */
	protected void setContexto(ImportContext contexto) {
		this.contexto = contexto;
	}

	/**
	 * @return the maximoNoProcesado
	 */
	protected long getMaximoNoProcesado() {
		return maximoNoProcesado;
	}

	/**
	 * @param maximoNoProcesado the maximoNoProcesado to set
	 */
	protected void setMaximoNoProcesado(long maximoNoProcesado) {
		this.maximoNoProcesado = maximoNoProcesado;
	}

	/**
	 * @return the filasProcesadas
	 */
	public Collection<T> getFilasProcesadas() {
		return filasProcesadas;
	}

	/**
	 * @param filasProcesadas the filasProcesadas to set
	 */
	protected void setFilasProcesadas(Collection<T> filasProcesadas) {
		this.filasProcesadas = filasProcesadas;
	}

	/**
	 * @return the filasNoProcesadas
	 */
	public Collection<H> getFilasNoProcesadas() {
		return filasNoProcesadas;
	}

	/**
	 * @param filasNoProcesadas the filasNoProcesadas to set
	 */
	protected void setFilasNoProcesadas(Collection<H> filasNoProcesadas) {
		this.filasNoProcesadas = filasNoProcesadas;
	}
	
	/**
	 * Testea si la cantidad actual de registros no procesados es masyor o igual que la maxima configurada.
	 * 
	 * @return true si la cantidad maxima no procesada es menor o igual que la cantidad actual (de no procesadas).
	 * 	En otro caso retorna false.
	 */
	public boolean superoMaximoNoProcesado() {
		return this.getMaximoNoProcesado() != -1 && this.getCantidadNoProcesada() >= this.getMaximoNoProcesado();
	}
	
	/**
	 * Agrega una fila (o registro) a la coleccion de no procesadas. Inicialmente testea si supera el maximo de registros no
	 * procesados.
	 * 
	 * @param fila a agregar a la coleccion de no procesados.
	 * @throws ImporterDataException cuando se supera la cantidad maxima de no procesados.
	 */
	public void agregarFilaNoProcesada(H fila) {
		if(this.superoMaximoNoProcesado()) {
			throw new ImporterDataMaximumNoProcessedException("Se alcanzo el maximo de filas no procesadas [" + this.getMaximoNoProcesado() + "].");
		}
		this.getFilasNoProcesadas().add(fila);
	}
	
	/**
	 * Agrega una fila a la lista de procesados.
	 * 
	 * @param fila a agregar a la lista de procesados.
	 */
	public void agregarFilaProcesada(T fila) {
		this.getFilasProcesadas().add(fila);
	}
	
	/**
	 * Obtiene la cantidad de filas actuales no procesadas.
	 * 
	 * @return la cantidad de registros (o filas) no procesadas.
	 */
	public long getCantidadNoProcesada() {
		return Long.valueOf(String.valueOf(this.getFilasNoProcesadas().size()));
	}
	
	/**
	 * Obtiene la cantidad de filas actuales procesadas.
	 * 
	 * @return la cantidad de registros (o filas) procesadas.
	 */
	public long getCantidadProcesada() {
		return Long.valueOf(String.valueOf(this.getFilasProcesadas().size()));
	}
	
	/**
	 * Testea si la cantidad de registros procesados es igual a un valor determinado.
	 * 
	 * @param valor de cantidad de registros procesados a testear.
	 * @return true si la cantidad de registros procesados es igual.
	 */
	public boolean tieneCantidadProcesada(long valor){
		return this.getCantidadProcesada() == valor;
	}
	
	/**
	 * Testea si la cantidad de registros no procesados es igual a un valor determinado.
	 * @param valor
	 * @return
	 */
	public boolean tieneCantidadNoProcesada(long valor){
		return this.getCantidadNoProcesada() == valor;
	}
	
	/**
	 * Agrega una fila a la lista de no procesadas, si y solo si la fila no es nula.
	 * 
	 * @param fila a agregar a la lista de no procesadas.
	 */
	public void agregarFilaNoProcesadaSiNoEsNula(H fila){
		if(fila != null) {
			this.agregarFilaNoProcesada(fila);
		}
	}
	
	/**
	 * Agrega una fila a la lista de procesadas, si y solo si la fila no es nula.
	 * @param fila
	 */
	public void agregarFilaProcesadaSiNoEsNula(T fila){
		if(fila != null) {
			this.agregarFilaProcesada(fila);
		}
	}
	
	/**
	 * Obtiene un iterador de las filas no procesadas.
	 * 
	 * @return un iterados de las filas no procesadas.
	 * @see Iterator
	 */
	public Iterator<H> getFilasNoProcesadasIterator() {
		return this.getFilasNoProcesadas().iterator();
	}
	
	/**
	 * Obtiene un iterador de las filas procesadas.
	 * 
	 * @return un iterador de las filas procesadas.
	 * @see Iterator
	 */
	public Iterator<T> getFilasProcesadasIterator() {
		return this.getFilasProcesadas().iterator();
	}
	
	public H getFilaNoProcesada(int posicion) {
		Iterator<H> iterator = this.getFilasNoProcesadasIterator();
		int pos = 0;
		while(iterator.hasNext()) {
			if(pos == posicion) {
				return iterator.next();
			}
			iterator.next();
			pos++;
		}
		return null;
	}
	
	public T getFilaProcesada(int posicion) {
		Iterator<T> iterator = this.getFilasProcesadasIterator();
		int pos = 0;
		while(iterator.hasNext()) {
			if(pos == posicion) {
				return iterator.next();
			}
			iterator.next();
			pos++;
		}
		return null;
	}
	
	/**
	 * Testea si la cantidad de filas no procesadas es mayor a cero.
	 * 
	 * @return true si hay mas de cero filas no procesadas. En otro caso retorna false.
	 */
	public boolean tieneFilasNoProcesadas() {
		return this.getFilasNoProcesadas().size() > 0;
	}
	
	/**
	 * Testea si la cantidad de filas procesadas es mayor a cero.
	 * 
	 * @return true si hay mas de cero filas procesadas. En otro caso retorna false.
	 */
	public boolean tieneFilasProcesadas() {
		return this.getFilasProcesadas().size() > 0;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Contexto: " + this.getContexto() + "\n.");
		buffer.append("Cantidad Procesada: " + this.getCantidadProcesada() + "\n.");
		buffer.append("Cantidad No Procesada: " + this.getCantidadNoProcesada() + "\n.");
		buffer.append("Maximo No Procesado: " + this.getMaximoNoProcesado() + "\n.");
		return buffer.toString();
	}

	@SuppressWarnings("unchecked")
	public T[] getFilasProcesadasArray() {
		return (T[]) this.getFilasProcesadas().toArray();
	}
}
