package ar.com.odra.data.content.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.utils.FieldSeparator;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
public class TextRowFile implements RowFileImport {

	private FieldSeparator separador;
	private String[] cols;
	private int indice = -1;

	public TextRowFile(String fila, int indice) {
		this(fila, FieldSeparator.COMMA, indice);
	}

	public TextRowFile(FieldSeparator separadorColumna,
			Integer cantidadColumnas, int indice) {
		this.separador = separadorColumna;
		this.cols = new String[cantidadColumnas.intValue()];
		this.indice = indice;
	}

	public TextRowFile(String row, FieldSeparator separadorColumna, int indice) {
		this.separador = separadorColumna;
		this.cols = separadorColumna.split(row);
		this.indice = indice;
	}

	public Float getValorFloat(int posicion) {
		chequearPosicion(posicion);
		return Float.valueOf(this.cols[posicion]);
	}

	protected void chequearPosicion(int posicion) {
		if (((this.cols != null) && (this.cols.length < posicion))
				|| (this.cols == null))
			throw new RuntimeException("No se puede acceder a posicion ["
					+ posicion + "]. Fila contiene ["
					+ (this.cols != null ? this.cols.length : 0)
					+ "] posiciones.");
	}

	public Float[] getValoresFloat() {
		Float[] floatsValues = new Float[this.cols.length];
		for (int i = 0; i < getRows().length; i++) {
			floatsValues[i] = Float.valueOf(this.cols[i]);
		}
		return floatsValues;
	}

	public Integer getValorInteger(int posicion) {
		chequearPosicion(posicion);
		return Integer.valueOf(this.cols[posicion]);
	}

	public Integer[] getValoresInteger() {
		Integer[] integersValues = new Integer[this.cols.length];
		for (int i = 0; i < getRows().length; i++) {
			integersValues[i] = Integer.valueOf(this.cols[i]);
		}
		return integersValues;
	}

	public String getValorString(int posicion) {
		chequearPosicion(posicion);
		return this.cols[posicion];
	}

	public String[] getValoresString() {
		return this.cols;
	}

	public Object getValor(int posicion) {
		chequearPosicion(posicion);
		return this.cols[posicion];
	}

	public Object[] getValores() {
		Object[] objectValues = new Object[this.cols.length];
		for (int i = 0; i < getRows().length; i++) {
			objectValues[i] = this.cols[i];
		}
		return objectValues;
	}

	protected FieldSeparator getSeparador() {
		return this.separador;
	}

	protected void setSeparador(FieldSeparator separador) {
		this.separador = separador;
	}

	protected String[] getRows() {
		return this.cols;
	}

	public int cantidadValores() {
		return this.cols.length;
	}

	public Date getValorDate(int posicion, DateFormat dateFormat) {
		String value = getValorString(posicion);
		try {
			return dateFormat.parse(value);
		} catch (ParseException localParseException) {
		}
		return new Date();
	}

	public String toString() {
		return Arrays.toString(this.cols);
	}

	public boolean tieneValores() {
		for (String value : this.cols) {
			if (value != null && value.trim().length() > 0) {
				return false;
			}
		}
		return true;
	}

	public Double getValorDouble(int posicion) {
		chequearPosicion(posicion);
		return Double.valueOf(this.cols[posicion]);
	}

	public Long getValorLong(int posicion) {
		chequearPosicion(posicion);
		return Long.valueOf(this.cols[posicion]);
	}

	public Long[] getValoresLong() {
		Long[] integersValues = new Long[this.cols.length];
		for (int i = 0; i < getRows().length; i++) {
			integersValues[i] = Long.valueOf(this.cols[i]);
		}
		return integersValues;
	}
	
	public int getIndice() {
		return this.indice;
	}

	public Date getValorDate(int posicion) {
		return this.getValorDate(posicion, new SimpleDateFormat("dd/MM/yyyy"));
	}
	
	public boolean tieneIndice(int indice) {
		return this.getIndice() - 1 == indice;
	}
}
