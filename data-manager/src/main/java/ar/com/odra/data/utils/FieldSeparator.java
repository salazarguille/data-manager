/**
 * 
 */
package ar.com.odra.data.utils;

import ar.com.odra.common.converter.Converter;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public enum FieldSeparator {

	  COMMA(","), 
	  PIPE("|"), 
	  POINT_AND_COMMA(";"), 
	  TWO_POINTS(":"), 
	  POINT("."), 
	  MIDDLE_DASH("-"), 
	  DOWN_DASH("_"), 
	  SLASH("\\"), 
	  INVERT_SLASH("/"), 
	  ENTER("\n"), 
	  TAB("\t"), 
	  ASTERIC("*"), 
	  PLUS("+"), 
	  EQUALS("="), 
	  PERCENT("%"), 
	  AMPERSON("&"), 
	  NUMERAL("#"), 
	  DOUBLE_QUOTES("\""), 
	  QUESTION("?"), 
	  AT("@");

	  private String value;

	  private FieldSeparator(String value) { 
		  this.value = value;
	  }

	  public String getValue() {
	    return this.value;
	  }
	  
	  @SuppressWarnings("unchecked")
	public <H> H[] split(Class<H> type, String row) {
		  String[] rowString = this.split(row);
		  H[] rowConverter = (H[]) Converter.convert(rowString.getClass(), rowString, type);
		  return rowConverter;
	  }
	  
	  public String[] split(String row) {
		  return row.split(this.getValue());
	  }
}
