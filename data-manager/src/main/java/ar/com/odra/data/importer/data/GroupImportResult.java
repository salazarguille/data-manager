/**
 * 
 */
package ar.com.odra.data.importer.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
public class GroupImportResult {

	private Map<String, ImportResult<Object, Object>> groupResults;
	
	public GroupImportResult() {
		super();
		this.setGroupResults(new HashMap<String, ImportResult<Object, Object>>());
	}

	/**
	 * @return the groupResults
	 */
	protected Map<String, ImportResult<Object, Object>> getGroupResults() {
		return groupResults;
	}

	/**
	 * @param groupResults the groupResults to set
	 */
	protected void setGroupResults(Map<String, ImportResult<Object, Object>> groupResults) {
		this.groupResults = groupResults;
	}
	
	@SuppressWarnings("unchecked")
	public <T, H> ImportResult<T, H> getResultFor(String strategyName) {
		return (ImportResult<T, H>) this.getGroupResults().get(strategyName);
	}
	
	public boolean hasResultFor(String strategyName) {
		return this.getGroupResults().containsKey(strategyName);
	}
	
	public <T, H> boolean hasResultOkFor(String strategyName, long quantity) {
		ImportResult<T, H> result = this.getResultFor(strategyName);
		return result != null ? result.tieneCantidadProcesada(quantity) : false;
	}
	
	public <T, H> boolean hasResultErrorFor(String strategyName, long quantity) {
		ImportResult<T, H> result = this.getResultFor(strategyName);
		return result != null ? result.tieneCantidadNoProcesada(quantity) : false;
	}
	
	@SuppressWarnings("unchecked")
	public <T, H> void addImportResult(String name, ImportResult<T, H> importResult) {
		this.getGroupResults().put(name, (ImportResult<Object, Object>) importResult);
	}

	public long getGroupImportResultSize() {
		return this.getGroupResults().size();
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for(Entry<String, ImportResult<Object, Object>> result: groupResults.entrySet()) {
			buffer.append(result.getKey());
			buffer.append(":\t");
			buffer.append(result.getValue());
			buffer.append("\n");
		}
		return buffer.toString();
	}

	public <T, H> T getResultValueAt(String strategyName, int index) {
		ImportResult<T, H> result = this.getResultFor(strategyName);
		return result.getFilaProcesada(index);
	}
	
	public <T, H> H getErrorValueAt(String strategyName, int index) {
		ImportResult<T, H> result = this.getResultFor(strategyName);
		return result.getFilaNoProcesada(index);
	}

	public <T, H> long getResultOkSizeFor(String strategyName) {
		ImportResult<T, H> result = this.getResultFor(strategyName);
		return result != null ? result.getCantidadProcesada() : 0;
	}
	
	public <T, H> long getResultErrorSizeFor(String strategyName) {
		ImportResult<T, H> result = this.getResultFor(strategyName);
		return result != null ? result.getCantidadNoProcesada() : 0;
	}

	public boolean hasGroupImportResultSize(long size) {
		return this.getGroupImportResultSize() == size;
	}
}
