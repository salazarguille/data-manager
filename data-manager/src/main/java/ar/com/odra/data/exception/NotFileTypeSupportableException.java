/**
 * 
 */
package ar.com.odra.data.exception;

/**
 * Excepcion no chequeada que se utiliza en el proceso de importacion.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public class NotFileTypeSupportableException extends ImporterDataException {

	private static final long serialVersionUID = -7706358945209586045L;

	/**
	 * 
	 */
	public NotFileTypeSupportableException() {
		super();
	}

	/**
	 * @param message
	 */
	public NotFileTypeSupportableException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NotFileTypeSupportableException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotFileTypeSupportableException(String message, Throwable cause) {
		super(message, cause);
	}
}
