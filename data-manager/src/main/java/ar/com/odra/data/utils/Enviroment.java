/**
 * 
 */
package ar.com.odra.data.utils;

import ar.com.odra.common.helper.StringHelper;



/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class Enviroment {

	private String name;

	private String baseLocation;
	
	public Enviroment(String name, String baseLocation) {
		super();
		this.setName(name);
		this.setBaseLocation(baseLocation);
	}

	/**
	 * @return the baseLocation
	 */
	public String getBaseLocation() {
		return baseLocation;
	}

	/**
	 * @param baseLocation the baseLocation to set
	 */
	public void setBaseLocation(String baseLocation) {
		this.baseLocation = baseLocation;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public boolean hasName(String name) {
		return this.getName().equalsIgnoreCase(name);
	}
	
	public String getPath(String relativePath) {
		if(StringHelper.isEmpty(this.getBaseLocation())) {
			return "\\" + relativePath;
		} else {
			if(this.getBaseLocation().endsWith("\\")) {
				return this.getBaseLocation() + relativePath;
			} else {
				return this.getBaseLocation() + "\\" + relativePath;
			}
		}
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Enviroment: ");
		buffer.append("Name [" + this.getName() + "] | ");
		buffer.append("Base Location [" + this.getBaseLocation() + "] | ");
		buffer.append(".");
		return buffer.toString();
	}
}
