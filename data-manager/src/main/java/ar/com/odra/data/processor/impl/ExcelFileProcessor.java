/**
 * 
 */
package ar.com.odra.data.processor.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.exception.ImporterDataInvalidFileException;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.processor.FileProcessor;
import ar.com.odra.data.processor.RowProcessor;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class ExcelFileProcessor extends FileProcessor {

	private static Logger LOGGER = Logger.getLogger(ExcelFileProcessor.class);

	private Workbook workbook;
	
	public void iniciarProceso(InputStream inputStream) {
		try {
			workbook = new HSSFWorkbook(inputStream);
		} catch (IOException e) {
			throw new ImporterDataInvalidFileException("Error al crear el workbook del excel: [" + e.getMessage() + "].", e);
		}
	}
	
	/**
	 * @return the workbook
	 */
	protected Workbook getWorkbook() {
		return workbook;
	}

	/**
	 * @param workbook the workbook to set
	 */
	protected void setWorkbook(HSSFWorkbook workbook) {
		this.workbook = workbook;
	}

	protected <T,H> void procesarPagina(int numeroPagina, ImportResult<T, H> resultadoImportacion, RowProcessor procesadorFila, RowProcessorStrategy estrategiaFila, ImportContext contexto){
		Sheet pagina = workbook.getSheetAt(numeroPagina - 1);
		Iterator<Row> filaIterator = pagina.rowIterator();
		LOGGER.debug("Obteniendo registros [" + pagina.getLastRowNum() + "] del archivo.");
		int indice = 1;
		while(filaIterator.hasNext()) {
			Row fila = filaIterator.next();
			LOGGER.debug("Creando instancia de registro numero [" + indice + "].");
			RowFileImport filaArchivo = procesadorFila.crearFilaArchivo(fila, indice);
			LOGGER.debug("Procesando instancia de registro numero [" + indice + "].");
			procesadorFila.procesarFila(resultadoImportacion, contexto, filaArchivo, estrategiaFila);
			LOGGER.debug("Registro numero [" + indice + "] procesado correctamente.");
			indice++;
		}
	}
}
