/**
 * 
 */
package ar.com.odra.data.matcher;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.odra.common.helper.ReflectionHelper;
import ar.com.odra.data.configuration.Configuration;
import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public class ParametersMatcher {
	
	private static final Logger LOGGER = Logger.getLogger(ParametersMatcher.class);

	private Object dataGenerator;
	private RowFileImport rowFileImport;
	private Method method;
	private ImportContext importContext;
	
	/**
	 * @return the importContext
	 */
	public ImportContext getImportContext() {
		return importContext;
	}

	/**
	 * @param importContext the importContext to set
	 */
	public void setImportContext(ImportContext importContext) {
		this.importContext = importContext;
	}

	/**
	 * @return the dataGenerator
	 */
	public Object getDataGenerator() {
		return dataGenerator;
	}

	/**
	 * @param dataGenerator
	 *            the dataGenerator to set
	 */
	public void setDataGenerator(Object dataGenerator) {
		this.dataGenerator = dataGenerator;
	}

	/**
	 * @return the rowFileImport
	 */
	public RowFileImport getRowFileImport() {
		return rowFileImport;
	}

	/**
	 * @param rowFileImport
	 *            the rowFileImport to set
	 */
	public void setRowFileImport(RowFileImport rowFileImport) {
		this.rowFileImport = rowFileImport;
	}

	/**
	 * @return the method
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * @param method
	 *            the method to set
	 */
	public void setMethod(Method method) {
		this.method = method;
	}
	
	
	public boolean isPrimitiveParameterType(int parameterIndex) {
		return ReflectionHelper.isAJavaPrimitiveClass(this.getParameterType(parameterIndex));
	}

	protected Class<?> getParameterType(int parameterIndex) {
		return this.getParameterTypeList().get(parameterIndex);
	}
	
	protected List<Class<?>> getParameterTypeList() {
		return Arrays.asList(this.getMethod().getParameterTypes());
	}
	
	protected Configuration getConfiguration() {
		return this.getImportContext().getConfiguration();
	}
	
	protected Annotation[] getMethodAnnotationsFor(int indexParameter) {
		return this.getMethod().getParameterAnnotations()[indexParameter];
	}
	
	public MatchResult match() {
		MatchResult matchingResult = new MatchResult(this.getRowFileImport());
		int parameterPosition 	= 0;
		for(Class<?> parameterType: this.getParameterTypeList()) {
			int indexParameterPosition = this.getParameterTypeList().indexOf(parameterType);
			Annotation[] annotations = this.getMethodAnnotationsFor(indexParameterPosition);
			LOGGER.debug("Method parameter type [" + parameterType + "] in position [" + indexParameterPosition+ "] has annotations [" + Arrays.toString(annotations) + "].");
			ParameterMatcheable matcheable = this.getConfiguration().getParameterMatcheable(this.getImportContext(), parameterType, annotations, this.getRowFileImport(), parameterPosition);
			LOGGER.info("Parameter type [" + parameterType + "] in position [" + parameterPosition + "] is matched with parameter matcheable [" + matcheable + "].");
			Object value = null;
			if(matcheable != null) {
				value = matcheable.matchParameter(this.getImportContext(), parameterType, annotations, this.getRowFileImport(), parameterPosition);
				if(matcheable.matchDomainClass()) {
					matchingResult.setBeanInstance(value);
				}
				if(matcheable.appliesAsParameter()) {
					parameterPosition++;
				}
			}
			matchingResult.addMatchParameter(parameterType, matcheable, value);
		}
		return matchingResult;
	}

	public boolean hasMethod() {
		return this.getMethod() != null;
	}

	public Class<?> getDomainClass() {
		int parameterPosition 	= 0;
		for(Class<?> parameterType: this.getParameterTypeList()) {
			int indexParameterPosition = this.getParameterTypeList().indexOf(parameterType);
			Annotation[] annotations = this.getMethod().getParameterAnnotations()[indexParameterPosition];
			ParameterMatcheable matcheable = this.getConfiguration().getParameterMatcheable(this.getImportContext(), parameterType, annotations, this.getRowFileImport(), parameterPosition);
			if(matcheable != null) {
				if(matcheable.matchDomainClass()) {
					return parameterType;
				}
			}
		}
		return null;
	}
}
