/**
 * 
 */
package ar.com.odra.data.factory.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import ar.com.odra.common.helper.AnnotationHelper;
import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.annotation.generator.StaticGenerators;
import ar.com.odra.data.factory.RowProcessorStrategyFactory;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;
import ar.com.odra.data.strategy.AnnotatedStaticRowProcessorStrategy;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.Utils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class AnnotatedStaticRowProcessorStrategyFactory implements RowProcessorStrategyFactory {

	public boolean canCreateRowProcessorStrategies(Object bean) {
		boolean hasDataGeneratorAnnotation = AnnotationHelper.hasAnnotation(bean.getClass(), DataGenerator.class);
		boolean hasStaticAnnotation = AnnotationHelper.hasAnnotation(bean.getClass(), StaticGenerator.class);
		boolean hasStaticAnnotations = AnnotationHelper.hasAnnotation(bean.getClass(), StaticGenerators.class);
		return hasDataGeneratorAnnotation && (hasStaticAnnotation || hasStaticAnnotations);
	}

	public void createRowProcessorStrategies(Object bean, ParameterMatcheableRegistry matcheableRegistry, Collection<RowProcessorStrategy> strategies) {
		List<StaticGenerator> staticGeneratorList = new ArrayList<StaticGenerator>();
		StaticGenerator staticGenerator = AnnotationHelper.getAnnotation(bean.getClass(), StaticGenerator.class);
		if(staticGenerator != null) {
			staticGeneratorList.add(staticGenerator);
		}
		StaticGenerators staticGenerators = AnnotationHelper.getAnnotation(bean.getClass(), StaticGenerators.class);
		if(staticGenerators != null) {
			staticGeneratorList.addAll(Arrays.asList(staticGenerators.values()));
		}
		
		for (StaticGenerator sg : staticGeneratorList) {
			String strategyName = sg.name();
			Method errorMethod = Utils.getMethodWithOnErrorHandlerGeneratorAnnotation(bean, strategyName);
			strategies.add(new AnnotatedStaticRowProcessorStrategy(bean, sg, matcheableRegistry, errorMethod));
		}
	}

}
