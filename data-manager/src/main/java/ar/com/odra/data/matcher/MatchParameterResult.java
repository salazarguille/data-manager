/**
 * 
 */
package ar.com.odra.data.matcher;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MatchParameterResult {

	private Class<?>	type;
	private Object		value;
	private ParameterMatcheable parameterMatcheable;
	
	public MatchParameterResult(Class<?> type, ParameterMatcheable parameterMatcheable, Object value) {
		super();
		this.setType(type);
		this.setValue(value);
		this.setParameterMatcheable(parameterMatcheable);
	}

	/**
	 * @return the parameterMatcheable
	 */
	public ParameterMatcheable getParameterMatcheable() {
		return parameterMatcheable;
	}

	/**
	 * @param parameterMatcheable the parameterMatcheable to set
	 */
	public void setParameterMatcheable(ParameterMatcheable parameterMatcheable) {
		this.parameterMatcheable = parameterMatcheable;
	}

	/**
	 * @return the type
	 */
	public Class<?> getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Class<?> type) {
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	public boolean hasValue() {
		return this.getValue() != null;
	}
	
	public boolean hasValue(Object value) {
		return this.hasValue() && this.getValue().equals(value);
	}
	
	public boolean hasType(Class<?> type) {
		return this.getType().equals(type);
	}
	
	public boolean hasParameterMatcheable() {
		return this.getParameterMatcheable() != null;
	}
	
	public boolean hasParameterMatcheable(ParameterMatcheable parameterMatcheable) {
		return this.hasParameterMatcheable() && this.getParameterMatcheable().equals(parameterMatcheable);
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Value: [");
		buffer.append(this.getValue());
		buffer.append("] - Type: [");
		buffer.append(this.getType());
		buffer.append("] - Matcheable: [");
		buffer.append(this.getParameterMatcheable());
		buffer.append("].");
		return buffer.toString();
	}

	public boolean appliesAsParameter() {
		return this.hasParameterMatcheable() && this.getParameterMatcheable().appliesAsParameter();
	}
	
	public Class<?> getValueType() {
		return this.getValue().getClass();
	}

	public boolean hasValueType(Class<?> value) {
		return  value == null || this.getType().equals(value);
	}
	
	public boolean matchDomainClass() {
		return this.hasParameterMatcheable() ? this.getParameterMatcheable().matchDomainClass() : false;
	}
}
