package ar.com.odra.data.content.impl;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import ar.com.odra.data.content.RowFileImport;

/**
 * Clase que representa a un registro (o fila) de un archivo excel.
 *
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * @see RowFileImport
 */
public class ExcelRowFile implements RowFileImport {

	private final Row	row;

	public ExcelRowFile(Row fila) {
		super();
		this.row = fila;
	}

	private String getStringValueCell(Row row, int position, boolean nullSafe) {
		String value = "";
		//short posicion = (short) position;
		Cell cell = row.getCell(position);
		if (cell != null) {
			if (cell.getCellType() == 0) {
				DecimalFormat decimalFormat = new DecimalFormat("00000.#");
				value = decimalFormat.format(cell.getNumericCellValue());
			} else if (cell.getCellType() == 1) {
				value = cell.getStringCellValue();
			} else if (cell.getCellType() == 2) {
				value = cell.getStringCellValue();
			} else if (cell.getCellType() == 3) {
				String stringValue = cell.getStringCellValue();
				if (nullSafe)
					// StringUtil.esStringVacioONull(stringValue)
					value = stringValue != null && stringValue.trim().length() > 0 ? stringValue : "";
				else
					value = stringValue != null && stringValue.trim().length() > 0 ? stringValue : null;
			} else if (cell.getCellType() == 4) {
				value = String.valueOf(cell.getBooleanCellValue());
			} else if (cell.getCellType() == 5) {
				value = String.valueOf(cell.getErrorCellValue());
			} else {
				value = nullSafe ? "" : null;
			}
		}
		return value;
	}

	public Float getValorFloat(int position) {
		String value = getStringValueCell(getRow(), position, true);
		return Float.valueOf((value != null && value.trim().length() > 0) ? "0" : value);
	}

	public Float[] getValoresFloat() {
		int quantity = cantidadValores();
		Float[] floatsValues = new Float[quantity];
		Iterator<Cell> cells = this.row.cellIterator();
		int i = 0;
		while (cells.hasNext()) {
			Cell cell = cells.next();
			floatsValues[i] = Float.valueOf(getStringValueCell(getRow(), cell.getColumnIndex(), true));
		}
		return floatsValues;
	}

	public Integer getValorInteger(int position) {
		String value = getStringValueCell(getRow(), position, true);
		return Integer.valueOf(value);
	}

	public Integer[] getValoresInteger() {
		int quantity = cantidadValores();
		Integer[] integerValues = new Integer[quantity];
		Iterator<Cell> cells = this.row.cellIterator();
		int i = 0;
		while (cells.hasNext()) {
			Cell cell = cells.next();
			integerValues[i] = Integer.valueOf(getStringValueCell(getRow(), cell.getColumnIndex(), true));
		}
		return integerValues;
	}

	public String getValorString(int position) {
		return getStringValueCell(getRow(), position, true);
	}

	public String[] getValoresString() {
		int quantity = cantidadValores();
		String[] stringValues = new String[quantity];
		int i = 0;
		for (i = 0; i < quantity; i++) {
			stringValues[i] = getStringValueCell(getRow(), i, true);
		}
		return stringValues;
	}

	public Object getValor(int position) {
		String value = getStringValueCell(getRow(), position, true);
		return value;
	}

	public Object[] getValores() {
		int quantity = cantidadValores();
		Object[] objectValues = new Object[quantity];
		Iterator<Cell> cells = this.row.cellIterator();
		int i = 0;
		while (cells.hasNext()) {
			Cell cell = cells.next();
			objectValues[i] = getStringValueCell(getRow(), cell.getColumnIndex(), true);// TODO se cambio cell num por column index.
			i++;
		}
		return objectValues;
	}

	public Row getRow() {
		return this.row;
	}

	public Date getValorDate(int position, DateFormat dateFormat) {
		String value = getStringValueCell(getRow(), position, true);
		try {
			if (value != null)
				return dateFormat.parse(value);
		} catch (ParseException e) {
			throw new RuntimeException("Invalid date format dateFormat: [" + dateFormat.getNumberFormat() + "]" + " for date: [" + value + "].", e);
		}
		return new Date();
	}

	public int cantidadValores() {
		int columnsQuantity = 0;
		int lastCellPosition = this.row.getLastCellNum();
		for (int i = 0; i < lastCellPosition; i++) {
			String value = getStringValueCell(getRow(), i, false);
			if (value != null) {
				columnsQuantity++;
			}
		}

		return columnsQuantity;
	}

	@Override
	public String toString() {
		String allValues = "";
		for (String value : getValoresString()) {
			allValues = allValues.concat(value + " - ");
		}
		return allValues;
	}

	public Double getValorDouble(int posicion) {
		String value = getStringValueCell(getRow(), posicion, true);
		return Double.valueOf(value);
	}

	public Long getValorLong(int posicion) {
		String value = getStringValueCell(getRow(), posicion, true);
		return Long.valueOf(value);
	}

	public Long[] getValoresLong() {
		int quantity = cantidadValores();
		Long[] values = new Long[quantity];
		Iterator<Cell> cells = this.row.cellIterator();
		int i = 0;
		while (cells.hasNext()) {
			Cell cell = cells.next();
			values[i] = Long.valueOf(getStringValueCell(getRow(), cell.getColumnIndex(), true));
		}
		return values;
	}

	public boolean tieneValores() {
		return this.cantidadValores() > 0;
	}

	public int getIndice() {
		return this.row.getRowNum();
	}

	public Date getValorDate(int posicion) {
		return this.getValorDate(posicion, new SimpleDateFormat("dd/MM/yyyy"));
	}

	public boolean tieneIndice(int indice) {
		return this.getIndice() == indice;
	}
}
