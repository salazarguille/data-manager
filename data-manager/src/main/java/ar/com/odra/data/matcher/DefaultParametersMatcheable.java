/**
 * 
 */
package ar.com.odra.data.matcher;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import ar.com.odra.common.converter.Converter;
import ar.com.odra.common.helper.ReflectionHelper;
import ar.com.odra.data.annotation.as.AsArray;
import ar.com.odra.data.annotation.as.AsBoolean;
import ar.com.odra.data.annotation.as.AsCustom;
import ar.com.odra.data.annotation.as.AsDate;
import ar.com.odra.data.annotation.as.AsEnum;
import ar.com.odra.data.annotation.as.AsInputStream;
import ar.com.odra.data.annotation.generator.BindParam;
import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.exception.InvalidConvertParameterTypeException;
import ar.com.odra.data.exception.NotBeanCreatedException;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.utils.BooleanFilter;
import ar.com.odra.data.utils.FieldSeparator;
import ar.com.odra.data.utils.StreamSource;
import ar.com.odra.data.utils.Utils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public enum DefaultParametersMatcheable implements ParameterMatcheable {

	DOMAIN_BEAN("DOMAIN_BEAN") {
		@Override
		public boolean matchDomainClass() {
			return true;
		}

		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return !ReflectionHelper.isAJavaPrimitiveClass(parameterType) && !Date.class.isAssignableFrom(parameterType)
					&& !Enum.class.isAssignableFrom(parameterType);
		}

		@Override
		public boolean appliesAsParameter() {
			return false;
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			Object bean = ReflectionHelper.createObject(parameterType);
			if (bean == null) {
				throw new NotBeanCreatedException("Bean class [" + parameterType + "] has not a default constructor.");
			}
			return bean;
		}
	},
	ROW_FILE("ROW_FILE") {

		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return RowFileImport.class.isAssignableFrom(parameterType);
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return filaArchivo;
		}
	},

	ROW_IMPORT("ROW_IMPORT") {

		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			boolean hasNotAnnotations = annotations != null && annotations.length == 0;
			boolean hasBindParamAnnotation = Utils.containsAnnotationType(annotations, BindParam.class);
			boolean hasRowFileImportPositions = filaArchivo == null || (filaArchivo != null && filaArchivo.cantidadValores() > parameterPosition);
			return (hasNotAnnotations || hasBindParamAnnotation) && ReflectionHelper.isAJavaPrimitiveClass(parameterType) && hasRowFileImportPositions;
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			Object importParameter = filaArchivo.getValor(parameterPosition);
			try {
				return Converter.convert(importParameter.getClass(), importParameter, parameterType);
			} catch (Exception e) {
				Class<?> importPararameterType = importParameter.getClass();
				String errorMessage = "Error converting value [" + importParameter + "] from type [" + importPararameterType
						+ "] to type [" + parameterType + "].";
				throw new InvalidConvertParameterTypeException(errorMessage, e);
			}
		}
	},
	DATE_TYPE("DATE_TYPE") {

		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return Date.class.isAssignableFrom(parameterType) && Utils.containsAnnotationType(annotations, AsDate.class);
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport rowFileImport, int parameterPosition) {
			AsDate asDate = Utils.getAnnotation(annotations, AsDate.class);
			Object value = rowFileImport.getValor(parameterPosition);
			try {
				DateFormat dateFormat = new SimpleDateFormat(asDate.format());
				return dateFormat.parse(value.toString());
			} catch (ParseException e) {
				throw new InvalidConvertParameterTypeException("Cannot parse [" + value + "] with date format ["
						+ asDate.format() + "].", e);
			}
		}
	},
	STREAM_TYPE("STREAM_TYPE") {
		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return InputStream.class.isAssignableFrom(parameterType)
					&& Utils.containsAnnotationType(annotations, AsInputStream.class);
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport rowFileImport, int parameterPosition) {
			AsInputStream asInputStream = Utils.getAnnotation(annotations, AsInputStream.class);
			String location = rowFileImport.getValorString(parameterPosition);
			StreamSource fileContext = asInputStream.source();
			return fileContext.getInputStream(location);
		}
	},
	ARRAY_TYPE("ARRAY_TYPE") {
		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return ReflectionHelper.isAJavaPrimitiveArrayClass(parameterType)
					&& Utils.containsAnnotationType(annotations, AsArray.class);
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			AsArray asArray = Utils.getAnnotation(annotations, AsArray.class);
			String value = filaArchivo.getValorString(parameterPosition);
			FieldSeparator fieldSeparator = asArray.splitter();
			String[] splitValue = fieldSeparator.split(value);
			return Converter.convert(String[].class, splitValue, parameterType);
		}
	},
	CUSTOM_TYPE("CUSTOM_TYPE") {
		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return CustomParameterMatcheable.class.isAssignableFrom(parameterType)
					&& Utils.containsAnnotationType(annotations, AsCustom.class);
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			AsCustom asCustom = Utils.getAnnotation(annotations, AsCustom.class);
			CustomParameterMatcheable customParameterMatcheable = ReflectionHelper.createObject(asCustom.value());
			if (customParameterMatcheable == null) {
				throw new InvalidConvertParameterTypeException("Cannot create custom parameter matcheable instance ["
						+ asCustom.value() + "].");
			}
			return customParameterMatcheable.matchParameter(parameterType, annotations, filaArchivo, parameterPosition);
		}
	},
	IMPORTER_CONTEXT_TYPE("IMPORTER_CONTEXT_TYPE") {
		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return ImportContext.class.isAssignableFrom(parameterType);
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return importContext;
		}
	},
	BOOLEAN_TYPE("BOOLEAN_TYPE") {
		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return Utils.isBooleanAssignable(parameterType) && Utils.containsAnnotationType(annotations, AsBoolean.class);
		}

		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			String toTest = filaArchivo.getValorString(parameterPosition);
			AsBoolean asBoolean = Utils.getAnnotation(annotations, AsBoolean.class);
			BooleanFilter booleanFilter = asBoolean.filter();
			return booleanFilter.test(asBoolean.value(), toTest);
		}
	},
	ENUM_TYPE("ENUM_TYPE") {
		public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			return Enum.class.isAssignableFrom(parameterType) && Utils.containsAnnotationType(annotations, AsEnum.class);
		}

		@SuppressWarnings("unchecked")
		public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations,
				RowFileImport filaArchivo, int parameterPosition) {
			String toTest = filaArchivo.getValorString(parameterPosition);
			AsEnum asEnum = Utils.getAnnotation(annotations, AsEnum.class);
			Class<Enum<?>> enumClass = (Class<Enum<?>>) asEnum.enumType();
			Enum<?>[] enumTypes = enumClass.getEnumConstants();

			if (enumTypes != null && enumTypes.length > 0) {
				for (Enum<?> enumValue : enumTypes) {
					if (enumValue.name().equalsIgnoreCase(toTest)) {
						return enumValue;
					}
				}
			}
			throw new InvalidConvertParameterTypeException("Cannot get enum instance [" + asEnum.enumType() + "] for value ["
					+ toTest + "].");
		}
	};

	private String name;

	private DefaultParametersMatcheable(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public boolean matchDomainClass() {
		return false;
	}

	public boolean appliesAsParameter() {
		return true;
	}
}
