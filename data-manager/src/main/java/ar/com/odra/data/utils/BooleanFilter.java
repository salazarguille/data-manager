/**
 * 
 */
package ar.com.odra.data.utils;

import ar.com.odra.common.helper.StringHelper;


/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public enum BooleanFilter {

	EQUALS_IGNORECASE("EQUALS_IGNORECASE") {
		@Override
		public boolean test(String value, String toTest) {
			return StringHelper.isNotEmpty(value) && value.equalsIgnoreCase(toTest);
		}
	},
	NOT_EQUALS_IGNORECASE("NOT_EQUALS_IGNORECASE") {
		@Override
		public boolean test(String value, String toTest) {
			return StringHelper.isNotEmpty(value) && !value.equalsIgnoreCase(toTest);
		}
	},
	EQUALS("EQUALS") {
		@Override
		public boolean test(String value, String toTest) {
			return StringHelper.isNotEmpty(value) && value.equals(toTest);
		}
	},
	NOT_EQUALS("NOT_EQUALS") {
		@Override
		public boolean test(String value, String toTest) {
			return StringHelper.isNotEmpty(value) && !value.equals(toTest);
		}
	},
	CONTAINS("CONTAINS") {
		@Override
		public boolean test(String value, String toTest) {
			return StringHelper.isNotEmpty(value) && value.contains(toTest);
		}
	},
	NOT_CONTAINS("NOT_CONTAINS") {
		@Override
		public boolean test(String value, String toTest) {
			return StringHelper.isNotEmpty(value) && !value.contains(toTest);
		}
	};

	private String name;

	private BooleanFilter(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public abstract boolean test(String value, String toTest);
}
