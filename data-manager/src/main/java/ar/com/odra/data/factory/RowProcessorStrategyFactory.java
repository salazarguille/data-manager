/**
 * 
 */
package ar.com.odra.data.factory;

import java.util.Collection;

import ar.com.odra.data.registry.ParameterMatcheableRegistry;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface RowProcessorStrategyFactory {
	
	public boolean canCreateRowProcessorStrategies(Object bean);

	public void createRowProcessorStrategies(Object bean, ParameterMatcheableRegistry matcheableRegistry, Collection<RowProcessorStrategy> strategies);
}
