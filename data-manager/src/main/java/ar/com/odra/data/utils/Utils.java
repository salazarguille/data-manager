/**
 * 
 */
package ar.com.odra.data.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ar.com.odra.common.helper.ReflectionHelper;
import ar.com.odra.data.annotation.generator.OnErrorHandlerGenerator;
import ar.com.odra.data.exception.BeanPropertyNotFoundException;
import ar.com.odra.data.exception.IllegalAccessPropertyException;
import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.exception.InvalidArgumentPropertyException;
import ar.com.odra.data.exception.NotDataGeneratorCreatedException;
import ar.com.odra.data.register.FileTypesSupportableRegister;
import ar.com.odra.data.register.ParameterMatcheablesRegister;
import ar.com.odra.data.register.RowProcessorStrategyFactoryRegister;
import ar.com.odra.data.register.impl.DefaultFileTypesSupportableRegister;
import ar.com.odra.data.register.impl.DefaultParameterMatcheablesRegister;
import ar.com.odra.data.register.impl.DefaultRowProcessorStrategyFactoryRegister;


/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class Utils {

	public final static Class<?>[] NULL_CLASS_ARRAY = null;
	
	public final static Class<?>[] EMPTY_CLASS_ARRAY = {};

	public final static String EMPTY_STRING = "";
	
	public static final Enviroment DUMMY_ENVIROMENT = new Enviroment("enviroment-dummy", "");

	public static final FileTypesSupportableRegister FILE_TYPES_SUPPORTABLE_REGISTER = new DefaultFileTypesSupportableRegister();
	
	public static final ParameterMatcheablesRegister PARAMETER_MATCHEABLES_REGISTER = new DefaultParameterMatcheablesRegister();
	
	public static final RowProcessorStrategyFactoryRegister ROW_PROCESSOR_STRATEGY_FACTORY_REGISTER = new DefaultRowProcessorStrategyFactoryRegister();

	public static final Comparator<Class<?>> CLASS_COMPARATOR = new Comparator<Class<?>>() {
		public int compare(Class<?> o1, Class<?> o2) {
			return o1.equals(o2) ? 0 : -1;
		}
	};
	

	private Utils() {
		super();
	}
	
	public static String getFileExtension(String fileName) {
		String[] fileParts = fileName.split("\\.");
		int lastIndex = fileParts != null ? fileParts.length - 1: -1;
		return lastIndex > -1 ? fileParts[lastIndex]: null;
	}
	
	public static boolean isEmpty(Object value) {
		String valueString = value != null ? value.toString() : null;
		return valueString != null && valueString.length() == 0;
	}
	
	public static boolean isEmptyONull(Object value) {
		String valueString = value != null ? value.toString() : "";
		return valueString.length() == 0;
	}

	public static Method getMethodWithOnErrorHandlerGeneratorAnnotation(Object bean, String strategyName) {
		String errorStrategyName = strategyName + "Error";
		Method[] errorMethods = ReflectionHelper.getAllMethodWith(OnErrorHandlerGenerator.class, bean.getClass());
		Method errorMethod = null;
		for (Method method : errorMethods) {
			OnErrorHandlerGenerator errorHandler = method.getAnnotation(OnErrorHandlerGenerator.class);
			String errorHandlerStrategyName = isEmptyONull(errorHandler.name()) ? method.getName() : errorHandler.name();
			if(errorStrategyName.equalsIgnoreCase(errorHandlerStrategyName)) {
				if(errorMethod != null) {
					throw new ImporterDataException("Exists more than one error handler strategy with name [" + errorStrategyName + "] in bean [" + bean.getClass() + "].");
				}
				errorMethod = method;
			}
		}
		return errorMethod;
	}

	public static <T extends Annotation> boolean containsAnnotationType(Class<?> beanClass, Class<T> annotationType) {
		return containsAnnotationType(beanClass.getAnnotations(), annotationType);
	}
	
	public static <T extends Annotation> boolean containsAnnotationType(Annotation[] annotations, Class<T> annotationType) {
		for (Annotation annotation : annotations) {
			if(annotationType.equals(annotation.annotationType())) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public static <T extends Annotation> T getAnnotation(Annotation[] annotations, Class<T> annotationType) {
		for (Annotation annotation : annotations) {
			if(annotationType.equals(annotation.annotationType())) {
				return (T) annotation;
			}
		}
		return null;
	}
	
	/**
	 * @param bean
	 * @param propertyName
	 * @param value
	 */
	public static void setFieldValue(Object bean, String propertyName, Object value) {
		try {
			Class<?> beanClass = bean.getClass();
			Field field = getFieldRecursive(beanClass, propertyName);
			if(field != null) {
				boolean accessible = field.isAccessible();
				field.setAccessible(true);
				field.set(bean, value);
				field.setAccessible(accessible);
			} else {
				throw new BeanPropertyNotFoundException("Property [" + propertyName + "] not found in bean class [" + bean.getClass() + "].");
			}
		} catch (IllegalArgumentException e) {
			throw new InvalidArgumentPropertyException("Cannot set property: [" + e.getMessage() + "].", e);
		} catch (IllegalAccessException e ) {
			throw new IllegalAccessPropertyException("Cannot set property: [" + e.getMessage() + "].", e);
		} catch (SecurityException e) {
			throw new IllegalAccessPropertyException("Cannot set property: [" + e.getMessage() + "].", e);
		}
	}
	
	public static Field getFieldRecursive(Class<?> beanType, String propertyName) {
		try {
			return beanType.getDeclaredField(propertyName);
		} catch (SecurityException e) {
			throw new IllegalAccessPropertyException("Cannot set property: [" + e.getMessage() + "].", e);
		} catch (NoSuchFieldException e) {
			if(!Object.class.equals(beanType.getSuperclass())) {
				return getFieldRecursive(beanType.getSuperclass(), propertyName);
			} else {
				return null;
			}
		}
	}
	
	public static boolean isBooleanAssignable(Class<?> type) {
		return Boolean.class.isAssignableFrom(type) || boolean.class.isAssignableFrom(type);
	}
	
	public static <T> boolean isEmptyOrNullArray(T...values) {
		return isNullArray(values) || values.length == 0;
	}
	
	public static <T> boolean isNullArray(T...values) {
		return values == null;
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] creates(Class<T>[] values) {
		List<T> instances = new ArrayList<T>();
		for (Class<?> type : values) {
			Object instance = ReflectionHelper.createObject(type);
			if(instance == null) {
				throw new NotDataGeneratorCreatedException("Cannot create data generator [" + type + "] instance.");
			}
			instances.add((T) instance);
		}
		return (T[]) instances.toArray(new Object[instances.size()]);
	}
}
