/**
 * 
 */
package ar.com.odra.data.exception;

/**
 * Excepcion no chequeada que se utiliza en el proceso de importacion.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class DefaultEnviromentRepeteadException extends ImporterDataException {

	private static final long serialVersionUID = -7706358945209586045L;

	/**
	 * 
	 */
	public DefaultEnviromentRepeteadException() {
		super();
	}

	/**
	 * @param message
	 */
	public DefaultEnviromentRepeteadException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DefaultEnviromentRepeteadException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DefaultEnviromentRepeteadException(String message, Throwable cause) {
		super(message, cause);
	}
}
