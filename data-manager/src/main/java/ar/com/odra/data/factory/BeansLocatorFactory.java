/**
 * 
 */
package ar.com.odra.data.factory;

import ar.com.odra.data.locator.BeansLocator;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public interface BeansLocatorFactory {

	public BeansLocator createBeansLocator();
	
}
