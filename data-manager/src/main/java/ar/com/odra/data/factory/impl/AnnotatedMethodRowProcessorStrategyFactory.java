/**
 * 
 */
package ar.com.odra.data.factory.impl;

import java.lang.reflect.Method;
import java.util.Collection;

import ar.com.odra.common.helper.AnnotationHelper;
import ar.com.odra.common.helper.ReflectionHelper;
import ar.com.odra.data.annotation.generator.DataGenerator;
import ar.com.odra.data.annotation.generator.MethodGenerator;
import ar.com.odra.data.factory.RowProcessorStrategyFactory;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;
import ar.com.odra.data.strategy.AnnotatedMethodRowProcessorStrategy;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.Utils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class AnnotatedMethodRowProcessorStrategyFactory implements RowProcessorStrategyFactory {

	public boolean canCreateRowProcessorStrategies(Object bean) {
		boolean hasDataGeneratorAnnotation = AnnotationHelper.hasAnnotation(bean.getClass(), DataGenerator.class);
		Method[] methods = ReflectionHelper.getAllMethodWith(MethodGenerator.class, bean.getClass());
		boolean hasMethodGeneratorAnnotation = methods != null && methods.length > 0;
		return hasDataGeneratorAnnotation && hasMethodGeneratorAnnotation;
	}

	public void createRowProcessorStrategies(Object bean, ParameterMatcheableRegistry matcheableRegistry, Collection<RowProcessorStrategy> strategies) {
		Method[] methods = ReflectionHelper.getAllMethodWith(MethodGenerator.class, bean.getClass());
		for (Method method : methods) {
			MethodGenerator methodGenerator = method.getAnnotation(MethodGenerator.class);
			String strategyName = Utils.isEmptyONull(methodGenerator.name()) ? method.getName(): methodGenerator.name();
			
			Method errorMethod = Utils.getMethodWithOnErrorHandlerGeneratorAnnotation(bean, strategyName);
			
			if(errorMethod != null) {
				strategies.add(new AnnotatedMethodRowProcessorStrategy(bean, matcheableRegistry, method, errorMethod));
			} else {
				strategies.add(new AnnotatedMethodRowProcessorStrategy(bean, matcheableRegistry, method));
			}
		}
	}

}
