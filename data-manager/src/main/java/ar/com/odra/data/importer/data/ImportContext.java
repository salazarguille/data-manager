/**
 * 
 */
package ar.com.odra.data.importer.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections15.CollectionUtils;

import ar.com.odra.data.configuration.Configuration;
import ar.com.odra.data.factory.ConfigurationFactory;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.DefaultFileTypes;
import ar.com.odra.data.utils.FileTypeSupportable;

/**
 * Clase que representa un contexto para la importacion de datos. Una instancia de esta clase es necesaria para la ejecuccion
 * de la importacion de datos. La misma contiene el tipo de archivo a utilizar, el nombre de la estrategia para procesar las
 * filas o registros del archivo, un nombre (o descripcion) de la importacion, y el numero de las paginas del archivo a importar.
 *  Ademas se puede configurar si el archivo contiene el header (nombre de las columnas) o no.
 * </br>
 * Por otro lado se puede configurar para que en caso de error en el proceso de una fila (o registro), evite el corte del proceso de la
 * importacion, y continue con el siguiente registro. Para esto se debe activa {@link #activarSeguridadErrorFila()}. Por default este
 * verificacion se encuentra activada.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see DefaultFileTypes
 * @see RowProcessorStrategy
 * 
 */
public class ImportContext {

	private String nombreEstrategia;
	
	private FileTypeSupportable tipoArchivo;
	
	private String nombre;
	
	private List<Integer> paginas;
	
	private boolean tieneHeader;
	
	private boolean seguridadErrorFila = true;
	
	private long maximoNoProcesado = -1;
	
	private Configuration configuration;
	
	/**
	 * Crea una instancia de importacion contexto sin informacion.
	 */
	public ImportContext(ConfigurationFactory configurationFactory){
		super();
		this.setPaginas(new ArrayList<Integer>());
		this.setConfiguration(configurationFactory.createConfiguration());
	}
	
	/**
	 * @return the configuration
	 */
	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	protected void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Crea una instancia del contexto de importacion con el nombre de la estrategia de procesador de fila (o registro), y el
	 * tipo de archivo a utilizar.</br>
	 * Por defecto el nombre (o descripcion) de este contexto se forma por la concatenacion del nombre del tipo del archivo, " - ", y
	 * el nombre de la estrategia.
	 *  
	 * @param nombreEstrategia nombre de la estrategia a utilizar para procesar cada registro del archivo.
	 * @param tipoArchivo tipo del archivo a procesar en la importacion.
	 * @see DefaultFileTypes
	 * @see RowProcessorStrategy
	 */
	public ImportContext(ConfigurationFactory configurationFactory, String nombreEstrategia, DefaultFileTypes tipoArchivo){
		this(configurationFactory, nombreEstrategia, tipoArchivo, tipoArchivo.getNombre() + " - " + nombreEstrategia);
	}
	
	/**
	 * Crea una instancia del contexto de importacion con el nombre de la estrategia del procesador de fila (o registro), 
	 * el tipo de archivo a utilizar, y el nombre (o descripcion) de la importacion.
	 * Por defecto no se agrega ninguna pagina para procesar.
	 * 
	 * @param nombreEstrategia nombre de la estrategia a utilizar para procesar cada registro del archivo.
	 * @param tipoArchivo tipo del archivo a procesar en la importacion.
	 * @param nombre o descripcion de la importacion.
	 * @See {@link DefaultFileTypes}
	 */
	public ImportContext(ConfigurationFactory configurationFactory, String nombreEstrategia, DefaultFileTypes tipoArchivo, String nombre){
		this(configurationFactory);
		this.setNombreEstrategia(nombreEstrategia);
		this.setTipoArchivo(tipoArchivo);
		this.setNombre(nombre);
	}

	/**
	 * @return the maximoNoProcesado
	 */
	public long getMaximoNoProcesado() {
		return maximoNoProcesado;
	}

	/**
	 * @param maximoNoProcesado the maximoNoProcesado to set
	 */
	public void setMaximoNoProcesado(long maximoNoProcesado) {
		this.maximoNoProcesado = maximoNoProcesado;
	}

	/**
	 * @return the paginas
	 */
	protected List<Integer> getPaginas() {
		return paginas;
	}

	/**
	 * @param paginas the paginas to set
	 */
	protected void setPaginas(List<Integer> paginas) {
		this.paginas = paginas;
	}

	public String getNombreEstrategia() {
		return nombreEstrategia;
	}

	public void setNombreEstrategia(String nombreEstrategia) {
		this.nombreEstrategia = nombreEstrategia;
	}

	public FileTypeSupportable getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(FileTypeSupportable tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Elimina del contexto todas las paginas agregadas para procesar. Una vez ejecutado este mensaje, se debera agregar
	 * una pagina para su proceso.
	 */
	public void eliminarPaginas(){
		this.getPaginas().clear();
	}
	
	/**
	 * Agrega una pagina para procesar la importacion. Comienza en 1.
	 * 
	 * @param numero
	 */
	public void agregarPagina(int numero){
		this.getPaginas().add(numero - 1);
	}
	
	/**
	 * Obtiene los numeros de las paginas a procesar en la importacion de datos en tipo array.
	 * 
	 * @return un array con los numeros de las paginas a procesar.
	 */
	public Integer[] getPaginasAsArray() {
		return this.paginas.toArray(new Integer[this.paginas.size()]);
	}
	
	/**
	 * Testea si el archivo de la importacion contiene encabezado o no.
	 * 
	 * @return true si contiene encabezado, en otro caso retorna false.
	 */
	public boolean contieneHeader() {
		return this.tieneHeader;
	}
	
	/**
	 * En el proceso de importacion va a procesar todos los registros.
	 */
	public void noTieneHeader() {
		this.tieneHeader = false;
	}
	
	/**
	 * En el proceso de importacion no va a en tener en cuenta el encabezado. 
	 */
	public void tieneHeader() {
		this.tieneHeader = true;
	}
	
	/**
	 * Activa la seguridad en el proceso de la filas (o registros).
	 */
	public void activarSeguridadErrorFila() {
		this.seguridadErrorFila = true;
	}
	
	/**
	 * Desactiva la seguridad en el proceso de las filas (o registros).
	 */
	public void desactivarSeguridadErrorFila() {
		this.seguridadErrorFila = false;
	}
	
	/**
	 * Testea si la seguridad en el proceso de las filas esta activado o no.
	 * @return true si esta activado. En otro caso retorna false.
	 */
	public boolean contieneSeguridadErrorFila() {
		return this.seguridadErrorFila;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Nombre Estrategia: " + this.getNombreEstrategia() + "\n.");
		buffer.append("Tipo Archivo: " + this.getTipoArchivo() + "\n.");
		buffer.append("Nombre: " + this.getNombre() + "\n.");
		buffer.append("Paginas: " + Arrays.toString(this.getPaginasAsArray()) + "\n.");
		return buffer.toString();
	}

	public void agregarPaginas(Integer ...pages) {
		CollectionUtils.addAll(this.getPaginas(), pages);
	}
}
