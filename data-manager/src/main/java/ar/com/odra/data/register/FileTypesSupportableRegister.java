/**
 * 
 */
package ar.com.odra.data.register;

import ar.com.odra.data.registry.FileTypeSupportableRegistry;

/**
 * 
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface FileTypesSupportableRegister {

	public void registerFileType(FileTypeSupportableRegistry fileTypeRegistry);
	
}
