/**
 * 
 */
package ar.com.odra.data.strategy;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.matcher.StaticParametersMatcher;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;
import ar.com.odra.data.utils.BindMode;
import ar.com.odra.data.utils.Enviroment;
import ar.com.odra.data.utils.Utils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public class AnnotatedStaticRowProcessorStrategy extends AnnotatedRowProcessorStrategy {

	private static final Logger LOGGER = Logger.getLogger(AnnotatedStaticRowProcessorStrategy.class);
	private StaticGenerator staticGenerator;

	public AnnotatedStaticRowProcessorStrategy(Object dataGeneratorInstance, StaticGenerator staticGenerator,
			ParameterMatcheableRegistry matcheableRegistry, Method errorMethod) {
		super(dataGeneratorInstance, matcheableRegistry, errorMethod);
		this.setStaticGenerator(staticGenerator);
	}

	/**
	 * @return the staticGenerator
	 */
	public StaticGenerator getStaticGenerator() {
		return staticGenerator;
	}

	/**
	 * @param staticGenerator
	 *            the staticGenerator to set
	 */
	protected void setStaticGenerator(StaticGenerator staticGenerator) {
		this.staticGenerator = staticGenerator;
	}

	@Override
	protected boolean canInvokeMethodGenerator() {
		return false;
	}

	@Override
	protected String getAnnotatedSourceName() {
		return this.getStaticGenerator().name();
	}

	@Override
	public String getPropertyNameAt(MatchResult matchResult, BindMode bindMode, Object bean, RowFileImport rowFileImport, int position) {
		LOGGER.debug("Getting property in position [" + position + "]");
		StaticGenerator staticGenerator = this.getStaticGenerator();
		String[] parameterNames = staticGenerator.parameters();
		return parameterNames.length > position ? parameterNames[position] : "";
	}

	@Override
	public BindMode getDataGeneratorBindMode() {
		return BindMode.BY_NAME;
	}

	@Override
	public Integer getOrder() {
		return this.getStaticGenerator().order();
	}
	
	@Override
	public Integer[] getPages() {
		int[] pages = this.getStaticGenerator().pages();
		List<Integer> pagesList = new ArrayList<Integer>();
		for (int page : pages) {
			pagesList.add(page);
		}
		return pagesList.toArray(new Integer[pagesList.size()]);
	}

	@Override
	public Class<?> getDomainClass(ImportContext importContext) {
		return this.getStaticGenerator().beanClass();
	}

	@Override
	protected boolean hasInputStream() {
		return super.hasInputStream() || this.hasStaticGeneratorInputStream();
	}

	protected boolean hasStaticGeneratorInputStream() {
		return !Utils.isEmptyONull(this.getStaticGenerator().resource());
	}

	@Override
	public InputStream getSourceInputStream(Enviroment enviroment) {
		if (this.hasStaticGeneratorInputStream()) {
			StaticGenerator staticGenerator = this.getStaticGenerator();
			return super.getInputStream(enviroment, staticGenerator.source(), staticGenerator.resource());
		} else {
			return super.getSourceInputStream(enviroment);
		}
	}
	
	@Override
	protected ParametersMatcher createParametersMatcher(ImportContext importContext, RowFileImport rowFileImport) {
		StaticParametersMatcher parametersMatcher = new StaticParametersMatcher();
		parametersMatcher.setDataGenerator(this.getDataGeneratorInstance());
		parametersMatcher.setRowFileImport(rowFileImport);
		parametersMatcher.setImportContext(importContext);
		parametersMatcher.setStaticGenerator(this.getStaticGenerator());
		return parametersMatcher;
	}

	@Override
	public String getResource() {
		if (this.hasStaticGeneratorInputStream()) {
			return this.getStaticGenerator().resource();
		}
		return super.getResource();
	}
	
	@Override
	public boolean hasHeader() {
		return this.hasStaticGeneratorInputStream() ? this.getStaticGenerator().hasHeader() : super.hasHeader();
	}
	
	@Override
	public String getDescription() {
		return this.hasStaticGeneratorInputStream() ? this.getStaticGenerator().description() : super.getDescription();
	}	
}
