/**
 * 
 */
package ar.com.odra.data.registry;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.matcher.ParameterMatcheable;


/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class ParameterMatcheableRegistry {

	private List<ParameterMatcheable> parameterMatcheables;
	
	public ParameterMatcheableRegistry(ParameterMatcheable...parameterMatcheables) {
		super();
		if(parameterMatcheables != null && parameterMatcheables.length > 0) {
			this.setParameterMatcheables(Arrays.asList(parameterMatcheables));
		} else {
			this.setParameterMatcheables(new ArrayList<ParameterMatcheable>());
		}
	}

	/**
	 * @return the parameterMatcheables
	 */
	public List<ParameterMatcheable> getParameterMatcheables() {
		return parameterMatcheables;
	}

	/**
	 * @param parameterMatcheables the parameterMatcheables to set
	 */
	protected void setParameterMatcheables(List<ParameterMatcheable> parameterMatcheables) {
		this.parameterMatcheables = parameterMatcheables;
	}
	
	public void register(ParameterMatcheable parameterMatcheable) {
		this.getParameterMatcheables().add(parameterMatcheable);
	}
	
	public void remove(ParameterMatcheable parameterMatcheable) {
		this.getParameterMatcheables().remove(parameterMatcheable);
	}

	public ParameterMatcheable getParameterMatcheable(ImportContext importContext, Class<?> parameterType, Annotation[] parameterAnnotations, RowFileImport filaArchivo, int parameterPosition) {
		for (ParameterMatcheable matcheable: this.getParameterMatcheables()) {
			if(matcheable.appliesMatching(importContext, parameterType, parameterAnnotations, filaArchivo, parameterPosition)) {
				return matcheable;
			}
		}
		return null;
	}
}
