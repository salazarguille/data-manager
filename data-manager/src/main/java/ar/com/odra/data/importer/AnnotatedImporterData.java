/**
 * 
 */
package ar.com.odra.data.importer;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import ar.com.odra.data.annotation.bean.DAOBy;
import ar.com.odra.data.annotation.bean.GeneratedBy;
import ar.com.odra.data.autowire.AutowirerMode;
import ar.com.odra.data.builder.ImportContextBuilder;
import ar.com.odra.data.configuration.Configuration;
import ar.com.odra.data.exception.ImporterDataException;
import ar.com.odra.data.exception.ImporterDataInvalidFileException;
import ar.com.odra.data.exception.NotDependencyFoundException;
import ar.com.odra.data.exception.SelfDependencyException;
import ar.com.odra.data.factory.ConfigurationFactory;
import ar.com.odra.data.factory.StrategiesRowFactory;
import ar.com.odra.data.importer.data.GroupImportResult;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.locator.Locatable;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.FileTypeSupportable;
import ar.com.odra.data.utils.ResourceInfo;
import ar.com.odra.data.utils.Utils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public class AnnotatedImporterData extends ImporterData {
	
	private static final Logger LOGGER = Logger.getLogger(AnnotatedImporterData.class);
	
	private Configuration			configuration;
	private ConfigurationFactory	configurationFactory;
	
	public AnnotatedImporterData(ConfigurationFactory configurationFactory, StrategiesRowFactory strategiesRowFactory) {
		super(strategiesRowFactory);
		this.setConfigurationFactory(configurationFactory);
		this.setConfiguration(configurationFactory.createConfiguration());
	}
	
	public AnnotatedImporterData(ConfigurationFactory configurationFactory) {
		this(configurationFactory, null);
	}
	
	/**
	 * @return the configurationFactory
	 */
	protected ConfigurationFactory getConfigurationFactory() {
		return configurationFactory;
	}

	/**
	 * @param configurationFactory the configurationFactory to set
	 */
	protected void setConfigurationFactory(ConfigurationFactory configurationFactory) {
		this.configurationFactory = configurationFactory;
	}

	/**
	 * @return the configuration
	 */
	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * @param configuration the configuration to set
	 */
	protected void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	public void registrarEstrategias(Object rowStrategy) {
		final Collection<RowProcessorStrategy> strategies = this.getConfiguration().createRowProcessorStrategies(rowStrategy);
		this.autowireProperties(rowStrategy);
		super.registrarEstrategias(new StrategiesRowFactory() {
			public Collection<RowProcessorStrategy> crearEstrategias() {
				return strategies;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Object> void registrarEstrategiasByBeans(Class<?> ...beanClasses) {
		Set<Class<?>> strategiesClasses = new HashSet<Class<?>>();
		for (Class<?> beanClass : beanClasses) {
			LOGGER.info("Registering strategies for bean class [" + beanClass + "].");
			if(Utils.containsAnnotationType(beanClass, GeneratedBy.class)) {
				GeneratedBy generatedBy = Utils.getAnnotation(beanClass.getAnnotations(), GeneratedBy.class);
				LOGGER.debug("Data generator found for bean class [" + beanClass + "].");
				strategiesClasses.addAll(Arrays.asList(generatedBy.values()));
			}
		}
		for ( Class<?> strategyClass : strategiesClasses ) {
			T[] instances = (T[]) Utils.creates(new Class[]{strategyClass});
			this.registrarEstrategias(instances);
		}
	}
	
	public <T> void registrarEstrategias(T ...rowStrategies) {
		for (Object rowStrategy : rowStrategies) {
			this.registrarEstrategias(rowStrategy);
		}
	}
	
	public <T> void registrarEstrategias(Collection<T> rowStrategies) {
		for (Object rowStrategy : rowStrategies) {
			this.registrarEstrategias(rowStrategy);
		}
	}
	
	protected Locatable getLocatable(RowProcessorStrategy rowStrategy, ImportContext importContext) {
		Class<?> domainClass = rowStrategy.getDomainClass(importContext);
		if(Utils.containsAnnotationType(domainClass.getAnnotations(), DAOBy.class)) {
			DAOBy daoBy = Utils.getAnnotation(domainClass.getAnnotations(), DAOBy.class);
			LOGGER.info("Getting locatable for bean [" + domainClass + "]. It is annotated with [" + daoBy +"]");
			if(!daoBy.value().isEmpty() || !Object.class.equals(daoBy.type())) {
				if(!daoBy.value().isEmpty() && this.getConfiguration().hasBean(daoBy.value())) {
					return this.getConfiguration().getBean(daoBy.value());
				} else {
					if(!Object.class.equals(daoBy.type()) && this.getConfiguration().hasBean(daoBy.type())) {
						return this.getConfiguration().getBean(daoBy.type());
					}	
				}
			}
		}
		return null;
	}
	
	/**
	 * Crea una instancia de {@link ImportContextBuilder} a partir de una estrategia de registro.
	 * 
	 * @param rowStrategy estrategia de procesador de registros.
	 * @return una nueva instancia de {@link ImportContextBuilder}.
	 */
	protected ImportContextBuilder createImportContextBuilder(RowProcessorStrategy rowStrategy) {
		ImportContextBuilder importContextBuilder = new ImportContextBuilder();
		importContextBuilder
			.withFileType(rowStrategy.getFileTypeSupportable())
			.withHeader(rowStrategy.hasHeader())
			.withMaximumNoProcess(rowStrategy.getMaximumNoProcess())
			.withName(rowStrategy.getDescription())
			.withSecurityFileError(rowStrategy.isSecurityFileError())
			.withPages(rowStrategy.getPages())
			.withStrategyName(rowStrategy.getNombre())
			.withConfigurationFactory(this.getConfigurationFactory())
			;
		return importContextBuilder;
	}

	public <T, H> GroupImportResult importar() {
		InputStream inputStream = null;
		try {
			GroupImportResult groupResult = new GroupImportResult();
			LOGGER.info("Importing data with configuration factory [" + this.getConfigurationFactory() + "].");
			RowProcessorStrategy[] processorStrategies = this.sortStrategies();
			LOGGER.info("Strategies sorted [" + Arrays.toString(processorStrategies) + "]");
			for (RowProcessorStrategy rowStrategy : processorStrategies) {
				String strategyName = rowStrategy.getNombre();
				LOGGER.info("Row strategy selected to process [" + strategyName + "].");

				ResourceInfo resourceInfo = new ResourceInfo(this.getConfiguration(), rowStrategy);
				inputStream = resourceInfo.createInputStream();
				
				String extension = resourceInfo.getExtension();
				LOGGER.info("Preparing for processing row strategy [" + rowStrategy + "] with source Extension [" + extension + "].");
				
				FileTypeSupportable fileTypeSupportable =  this.getConfiguration().getFileTypeSupportable(extension);
				ImportContextBuilder importContextBuilder = this.createImportContextBuilder(rowStrategy);
				importContextBuilder.withFileType(fileTypeSupportable);
				
				ImportContext importContext = importContextBuilder.buildImportContext();
				
				ImportResult<T, H> importResult = super.importar(inputStream, importContext);
				LOGGER.debug("Row strategy [" + strategyName + "] processed with result [" + importResult + "].");
				
				Locatable locatable = this.getLocatable(rowStrategy, importContext);
				this.executeLocatable(locatable, importResult);
				
				groupResult.addImportResult(strategyName, importResult);
			}
			return groupResult;
		} catch (ImporterDataInvalidFileException e ){
			throw e;
		} catch (Exception e) {
			throw new ImporterDataException("Error on importing resources.", e);
		}finally {
			if(inputStream != null) {
				IOUtils.closeQuietly(inputStream);
			}
		}
	}
	
	protected <T, H> void executeLocatable(Locatable locatable, ImportResult<T, H> importResult) {
		LOGGER.debug("Testing to execute import result with locatable [" + locatable + "].");
		if(locatable != null) {
			LOGGER.debug("Executing import result with locatable [" + locatable + "].");
			locatable.execute(importResult.getFilasProcesadasArray());
		}
	}

	public RowProcessorStrategy[] sortStrategies() {
		this.checkDependencies();
		RowProcessorStrategy[] strategies = super.getRowProcessorStrategiesArray();
		Arrays.sort(strategies);
		return strategies;
	}
	
	protected void checkDependencies() {
		for(RowProcessorStrategy strategy: this.getEstrategias().values()) {
			for(Class<?> dependency : strategy.getDependencies()) {
				if(!this.containsDependency(dependency)) {
					throw new NotDependencyFoundException("Dependency [" + dependency + "] located data generator [" + strategy.getNombre() + "] not found.");
				}
				if(strategy.hasDataGeneratorInstanceClass(dependency)) {
					throw new SelfDependencyException("Data generator [" + dependency + "] has a self dependency.");
				}
			}
		}
	}
	
	protected boolean containsDependency(Class<?> dependency) {
		for(RowProcessorStrategy strategy: this.getEstrategias().values()) {
			if(strategy.hasDataGeneratorInstanceClass(dependency)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	protected void autowireProperties(Object dataGeneratorInstance) {
		AutowirerMode autowirerMode = this.getConfiguration().getAutowirerMode();
		LOGGER.info("Autowiring data generator properties with [" + autowirerMode + "]");
		autowirerMode.autowire(dataGeneratorInstance);
	}
}
