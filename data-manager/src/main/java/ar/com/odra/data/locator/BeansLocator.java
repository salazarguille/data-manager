/**
 * 
 */
package ar.com.odra.data.locator;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface BeansLocator {

	public Locatable locateBean(String name);
	
	public Locatable locateBean(Class<?> type);
	
	public boolean canLocateBean(String name);
	
	public boolean canLocateBean(Class<?> type);
}
