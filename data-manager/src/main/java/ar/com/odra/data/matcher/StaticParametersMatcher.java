/**
 * 
 */
package ar.com.odra.data.matcher;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import ar.com.odra.data.annotation.generator.StaticGenerator;
import ar.com.odra.data.exception.IllegalAccessPropertyException;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 */
public class StaticParametersMatcher extends ParametersMatcher {

	private StaticGenerator staticGenerator;

	/**
	 * @return the staticGenerator
	 */
	public StaticGenerator getStaticGenerator() {
		return staticGenerator;
	}

	/**
	 * @param staticGenerator
	 *            the staticGenerator to set
	 */
	public void setStaticGenerator(StaticGenerator staticGenerator) {
		this.staticGenerator = staticGenerator;
	}

	@Override
	protected List<Class<?>> getParameterTypeList() {
		Class<?> beanClass = this.getStaticGenerator().beanClass();
		List<Class<?>> types = new ArrayList<Class<?>>();
		types.add(this.getStaticGenerator().beanClass());
		for (String parameterName : this.getStaticGenerator().parameters()) {
			try {
				types.add(beanClass.getDeclaredField(parameterName).getType());
			} catch (SecurityException e) {
				throw new IllegalAccessPropertyException("Cannot access property [" + parameterName + "].", e);
			} catch (NoSuchFieldException e) {
				throw new IllegalAccessPropertyException("Cannot access property [" + parameterName + "].", e);
			}
		}
		return types;
	}
	
	@Override
	protected Annotation[] getMethodAnnotationsFor(int indexParameter) {
		return new Annotation[]{};
	}
}
