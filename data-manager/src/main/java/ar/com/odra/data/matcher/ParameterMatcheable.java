/**
 * 
 */
package ar.com.odra.data.matcher;

import java.lang.annotation.Annotation;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface ParameterMatcheable {

	public boolean appliesMatching(ImportContext importContext, Class<?> parameterType, Annotation[] annotations, RowFileImport filaArchivo, int parameterPosition);
	
	public Object matchParameter(ImportContext importContext, Class<?> parameterType, Annotation[] annotations, RowFileImport filaArchivo, int parameterPosition);
	
	public boolean matchDomainClass();
	
	public boolean appliesAsParameter();
}
