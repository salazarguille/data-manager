/**
 * 
 */
package ar.com.odra.data.processor.impl;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.content.impl.TextRowFile;
import ar.com.odra.data.processor.RowProcessor;
import ar.com.odra.data.utils.FieldSeparator;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class TextRowProcessor extends RowProcessor {
	
	private FieldSeparator fieldSeparator;
	
	public TextRowProcessor(FieldSeparator fieldSeparator) {
		super();
		this.fieldSeparator = fieldSeparator;
	}

	public RowFileImport crearFilaArchivo(Object fila, int indice) {
		return new TextRowFile(fila.toString(), this.fieldSeparator, indice);
	}
}
