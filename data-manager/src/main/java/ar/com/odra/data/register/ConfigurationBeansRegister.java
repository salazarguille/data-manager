/**
 * 
 */
package ar.com.odra.data.register;

import ar.com.odra.data.autowire.AutowirerMode;
import ar.com.odra.data.factory.BeansLocatorFactory;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface ConfigurationBeansRegister extends FileTypesSupportableRegister, ParameterMatcheablesRegister,
		RowProcessorStrategyFactoryRegister, BeansLocatorFactory, AutowirerMode {

}
