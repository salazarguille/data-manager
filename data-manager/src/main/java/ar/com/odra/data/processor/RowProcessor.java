/**
 * 
 */
package ar.com.odra.data.processor;

import org.apache.log4j.Logger;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.importer.data.ImportResult;
import ar.com.odra.data.strategy.RowProcessorStrategy;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
public abstract class RowProcessor {

	private static final Logger LOGGER = Logger.getLogger(RowProcessor.class);
	
	public abstract RowFileImport crearFilaArchivo(Object fila, int indice);

	@SuppressWarnings("unchecked")
	public <T, H> void procesarFila(ImportResult<T, H> resultadoImportacion, ImportContext importContext, RowFileImport filaArchivo, RowProcessorStrategy estrategiaFila) {
		try {
			if(importContext.contieneHeader() && filaArchivo.tieneIndice(0)){
				this.procesarHeader(importContext, filaArchivo, estrategiaFila);
			} else {
				T resultado = (T) estrategiaFila.procesarFila(importContext, filaArchivo);
				resultadoImportacion.agregarFilaProcesadaSiNoEsNula(resultado);
			}
		} catch (Exception e){
			LOGGER.error("Error processing row [" + filaArchivo + "]", e);
			H resultadoInvalido = (H) estrategiaFila.procesarFilaErronea(importContext, filaArchivo, e);
			resultadoImportacion.agregarFilaNoProcesadaSiNoEsNula(resultadoInvalido);
			if(!importContext.contieneSeguridadErrorFila()) {
				throw new RuntimeException("Error en la importacion procesando fila con indice [" + filaArchivo.getIndice() + "]: [" + e.getMessage() + "].", e);
			}
		}
	}
	
	protected <T> T procesarHeader(ImportContext contexto, RowFileImport filaArchivo, RowProcessorStrategy estrategiaFila) {
		return null;
	}
}
