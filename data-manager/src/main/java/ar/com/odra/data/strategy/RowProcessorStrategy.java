/**
 * 
 */
package ar.com.odra.data.strategy;

import java.io.InputStream;
import java.util.Arrays;

import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.exception.CannotAccessResourceException;
import ar.com.odra.data.importer.ImporterData;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.processor.FileProcessor;
import ar.com.odra.data.processor.RowProcessor;
import ar.com.odra.data.utils.DefaultFileTypes;
import ar.com.odra.data.utils.Enviroment;
import ar.com.odra.data.utils.FieldSeparator;
import ar.com.odra.data.utils.FileTypeSupportable;
import ar.com.odra.data.utils.StreamSource;
import ar.com.odra.data.utils.Utils;

/**
 * Clase definida para el proceso de importacion de datos. El objetivo de este tipo es el proceso de los registros (o filas)
 * de cada archivo que se desee importar. NO es necesario crear una estrategia para los diferentes tipos de archivos que se desea
 * importar. Por ejemplo, si tengo dos archivos de personas (de tipos diferentes, un excel, y un txt, con igual formato de columnas),
 * no hace falta crear dos  estrategia para procesar cada fila (o registro) para ambos archivos. Simplemente con una implementacion
 * de este tipo, y registracion en el importador de datos es necesario para procesarlo.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see DefaultFileTypes
 * @see RowFileImport
 * @see ImporterData
 *
 */
public abstract class RowProcessorStrategy implements Comparable<AnnotatedRowProcessorStrategy>  {

	public static final Integer DEFAULT_ORDER = 1;

	/**
	 * Obtiene el nombre de esta estrategia de procesador de fila (o registro).
	 * 
	 * @return el nombre de la actual estrategia.
	 */
	public abstract String getNombre();
	
	/**
	 * Procesa una fila del archivo importado. Este mensaje se llamara por cada registro que exista en el archivo
	 * importado. Los datos del registro se podr�n obtener desde la instancia de {@link RowFileImport}.
	 * 
	 * @param <T> tipo que se retorna por el procesa de un registro del archivo.
	 * @param importContext context de esta importacion.
	 * @param filaArchivo instancia que representa al registro (o fila) del archivo.
	 * @return una instancia de tipo <T> que se crea por el proceso del registro (o fila) del archivo.
	 */
	public abstract <T> T procesarFila(ImportContext importContext, RowFileImport rowFileImport);
	
	public abstract <H> H procesarFilaErronea(ImportContext importContext, RowFileImport filaArchivo, Exception error);
	
	public Integer getOrder() {
		return DEFAULT_ORDER;
	}
	
	public Class<?> getDomainClass(ImportContext importContext) {
		return null;
	}
	
	public boolean hasDomainClass(ImportContext importContext) {
		return this.getDomainClass(importContext) != null;
	}
	
	public boolean hasDomainClass() {
		return this.getDomainClass(null) != null;
	}
	
	public Class<?>[] getDependencies() {
		return new Class<?>[]{};
	}
	
	public int compareTo(AnnotatedRowProcessorStrategy o) {
		return this.getNombre().compareTo(o.getNombre());
	}
	
	public boolean hasDependency(Class<?> dependency) {
		return Arrays.binarySearch(this.getDependencies(), dependency, Utils.CLASS_COMPARATOR) > -1;
	}
	
	public boolean hasDependencies() {
		return Utils.isEmptyOrNullArray(this.getDependencies());
	}

	public InputStream getSourceInputStream(Enviroment enviroment) {
		return null;
	}

	public Integer[] getPages() {
		return new Integer[]{1};
	}

	public FileTypeSupportable getFileTypeSupportable() {
		return null;
	}

	
	protected final InputStream getInputStream(Enviroment enviroment, StreamSource streamSource, String location) {
		if(!Utils.isEmpty(location)) {
			String resourcePath = enviroment.getPath(location);
			return streamSource.getInputStream(resourcePath);
		}
		throw new CannotAccessResourceException("Resource location [" + location + "] is not defined. Stream source used [" + streamSource + "]");
	}

	/**
	 * @return
	 */
	public String getResource() {
		return null;
	}

	public boolean hasHeader() {
		return false;
	}
	
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("\nName: [");
		buffer.append(this.getNombre());
		buffer.append("]\t\t");
		buffer.append("Class: [");
		buffer.append(this.getDataGeneratorInstanceClass());
		buffer.append("]\t\t");
		buffer.append("Dependencies: [");
		buffer.append(Arrays.toString(this.getDependencies()) + "].");
		return buffer.toString();
	}

	public Class<?> getDataGeneratorInstanceClass() {
		return null;
	}
	
	public boolean hasDataGeneratorInstanceClass(Class<?> type) {
		return this.getDataGeneratorInstanceClass() != null && this.getDataGeneratorInstanceClass().equals(type);
	}

	public String getDescription() {
		return null;
	}
	
	public boolean isSecurityFileError() {
		return false;
	}

	public int getMaximumNoProcess() {
		return 0;
	}
	
	public RowProcessor getRowProcessor(FileTypeSupportable tipoArchivo) {
		return tipoArchivo.getProcesadorFila(FieldSeparator.COMMA);
	}
	
	public FileProcessor getFileProcessor(FileTypeSupportable tipoArchivo) {
		return tipoArchivo.getProcesadorArchivo();
	}
}
