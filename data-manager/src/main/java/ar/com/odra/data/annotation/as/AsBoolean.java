package ar.com.odra.data.annotation.as;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ar.com.odra.data.utils.BooleanFilter;

/**
 * Anotacion utilizada para convertir un parametro del metodo generador en un boolean. La conversion se realiza a partir
 * de evaluar el valor obtenido de la importacion con el filtro (atributo {@link #filter()}) y el valor (atributo {@link #value()}) definido.
 * </br></br>
 * Por default el atributo {@link #filter()} es {@link BooleanFilter#EQUALS}.
 * </br></br>
 * Por default el atributo {@link #value()} es "true".
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.PARAMETER})
public @interface AsBoolean {

	String value() default "true";
	
	BooleanFilter filter() default BooleanFilter.EQUALS;
	
}
