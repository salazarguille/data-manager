/**
 * 
 */
package ar.com.odra.data.register.impl;

import ar.com.odra.data.register.FileTypesSupportableRegister;
import ar.com.odra.data.registry.FileTypeSupportableRegistry;
import ar.com.odra.data.utils.DefaultFileTypes;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class DefaultFileTypesSupportableRegister implements FileTypesSupportableRegister {
	public void registerFileType(FileTypeSupportableRegistry fileTypeRegistry) {
		fileTypeRegistry.registerFileTypeSupportable(DefaultFileTypes.TIPO_EXCEL_2010);
		fileTypeRegistry.registerFileTypeSupportable(DefaultFileTypes.TIPO_EXCEL_97);
		fileTypeRegistry.registerFileTypeSupportable(DefaultFileTypes.TIPO_TEXTO);
	}
}
