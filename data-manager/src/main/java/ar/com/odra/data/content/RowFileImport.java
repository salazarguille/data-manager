/**
 * 
 */
package ar.com.odra.data.content;

import java.text.DateFormat;
import java.util.Date;

/**
 * 
 * Tipo definido para representar una fila en la importacion de archivos. Por cada registro definido en un archivo, se representara
 * por una instancia de este tipo.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface RowFileImport {

	/**
	 * Obtiene el valor de la posicion definida de la fila actual.
	 *  
	 * @param posicion a buscar su valor.
	 * @return el valor en la posicion definida.
	 */
	public Object getValor(int posicion);
	
	/**
	 * Obtiene el valor como string de la posicion definida.
	 * 
	 * @param posicion a buscar su valor como string.
	 * @return el valor de tipo string de la posicion definida.
	 */
	public String getValorString(int posicion);
	
	/**
	 * Obtiene el valor como long de la posicion definida.
	 * 
	 * @param posicion a buscar su valor como long.
	 * @return el valor de tipo long de la posicion definida.
	 */
	public Long getValorLong(int posicion);
	
	/**
	 * Obtiene el valor como double de la posicion definida.
	 * 
	 * @param posicion a buscar su valor como double.
	 * @return el valor de tipo double de la posicion definida.
	 */
	public Double getValorDouble(int posicion);
	
	/**
	 * Obtiene el valor como integer de la posicion definida.
	 * 
	 * @param posicion a buscar su valor como integer.
	 * @return el valor de tipo integer de la posicion definida.
	 */
	public Integer getValorInteger(int posicion);
	
	/**
	 * Obtiene el valor como float de la posicion definida.
	 * 
	 * @param posicion a buscar su valor como float.
	 * @return el valor de tipo float de la posicion definida.
	 */
	public Float getValorFloat(int posicion);
	
	/**
	 * Obtiene el valor como date de la posicion definida a partir del formato definido.
	 * 
	 * @param posicion a buscar su valor como date.
	 * @param dateFormat que define el formato de la fecha recibida.
	 * @return una instancia de un date formateado segun el formateador.
	 */
	public Date getValorDate(int posicion, DateFormat dateFormat);
	
	/**
	 * Obtiene el valor como date de la posicion definida con un formateador default definido por la implementacion.
	 * 
	 * @param posicion a buscar su valor como date.
	 * @return una instancia de un date formateado segun el formateador default.
	 */
	public Date getValorDate(int posicion);
	
	/**
	 * Obtiene el indice del registro dentro del archivo importado. El primer registro es el numero 1.
	 * 
	 * @return el indice del registro dentro del archivo importado.
	 */
	public int getIndice();
	
	/**
	 * Obtiene todos los valores de la fila actual.
	 * 
	 * @return un array con todos los valores de la fila actual.
	 */
	public Object[] getValores();
	
	/**
	 * Obtiene todos los valor de la fila actual como strings.
	 * 
	 * @return un array con todos los valores de la fila actual como strings.
	 */
	public String[] getValoresString();
	
	/**
	 * Obtiene todos los valor de la fila actual como integers.
	 * 
	 * @return un array con todos los valores de la fila actual como integers.
	 */
	public Integer[] getValoresInteger();
	
	/**
	 * Obtiene todos los valores de la fila actual como floats.
	 * 
	 * @return un array con todos los valores de la fila actual como floats.
	 */
	public Float[] getValoresFloat();
	
	/**
	 * Obtiene todos los valores de la fila actual como longs.
	 * 
	 * @return un array con todos los valores de la fila actual como longs.
	 */
	public Long[] getValoresLong();
	
	/**
	 * Obtiene la cantidad de valores que contiene esta fila.
	 * 
	 * @return la cantidad de valores que contiene esta fila.
	 */
	public int cantidadValores();
	
	/**
	 * Testea si la fila actual contiene valores.
	 * 
	 * @return true si tiene mas de un valor. En otro caso retorna false.
	 */
	public boolean tieneValores();
	
	/**
	 * Testea si la fila tiene un indice determinado.
	 * @param indice a testear.
	 * @return true si tiene asignado el indice definido. En otro caso retorna false.
	 */
	public boolean tieneIndice(int indice);
}
