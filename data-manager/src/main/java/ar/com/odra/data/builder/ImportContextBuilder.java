/**
 * 
 */
package ar.com.odra.data.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ar.com.odra.data.factory.ConfigurationFactory;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.DefaultFileTypes;
import ar.com.odra.data.utils.FileTypeSupportable;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
public class ImportContextBuilder {
	
	/**
	 * Crea un contexto para la importacion de datos. Esta intancia se crea a partir del tipo de archivo a utilizar, el nombre
	 * de la estrategia para procesar los registros ( o filas), una nombre generico para la importacion, y el numero de 
	 * paginas a procesar en la importacion.
	 * 
	 * @param fileTypeSupportable a procesar en la importacion.
	 * @param strategyName estrategia registrada para procesar cada registro de la importacion.
	 * @param name o descripcion del proceso de importacion de datos.
	 * @param pages a procesar del archivo.
	 * @return una instancia del contexto de importacion de datos.
	 * @see ImportContext
	 * @see RowProcessorStrategy
	 * @see DefaultFileTypes
	 */
	public static ImportContextBuilder createImportContextBuilder(ConfigurationFactory configurationFactory, FileTypeSupportable fileTypeSupportable, String strategyName, String name, Integer[] pages) {
		ImportContextBuilder contextBuilder = new ImportContextBuilder();
		contextBuilder
			.withFileType(fileTypeSupportable)
			.withStrategyName(strategyName)
			.withName(name)
			.withPages(pages)
			.withConfigurationFactory(configurationFactory);
		return contextBuilder;
	}
	
	/**
	 * Crea un contexto para la importacion de datos. Esta intancia se crea a partir del tipo de archivo a utilizar, el nombre
	 * de la estrategia para procesar los registros ( o filas), y el numero de 
	 * paginas a procesar en la importacion.</br>
	 * El nombre o descripcion de la importacion se setea a partir del nombre de la estrategia y el nombre del tipo de archivo.
	 * 
	 * @param fileTypeSupportable a procesar en la importacion.
	 * @param strategyName estrategia registrada para procesar cada registro de la importacion.
	 * @param pages a procesar del archivo.
	 * @return una instancia del contexto de importacion de datos.
	 */
	public static ImportContextBuilder createImportContextBuilder(ConfigurationFactory configurationFactory, FileTypeSupportable fileTypeSupportable, String strategyName, Integer[] pages) {
		return createImportContextBuilder(configurationFactory, fileTypeSupportable, strategyName, "", pages);
	}
	
	/**
	 * Crea un contexto para la importacion de datos mediante un archivo de tipo texto ({@link DefaultFileTypes#TIPO_TEXTO}).
	 * Esta instancia se crea a partir del nombre de la estrategia a utilizar, y el nombre generico de la importacion.
	 * Por defecto se utiliza el tipo de archivo de texto {@link DefaultFileTypes#TIPO_TEXTO}, y la pagina numero uno solamente.
	 * 
	 * @param nombreEstrategia estrategia registrada para procesar cada registro de importacion.
	 * @param nombre generico del importador de datos.
	 * @return una intancia del contexto de importacion de datos.
	 */
	public static ImportContextBuilder createImportContextBuilderText(ConfigurationFactory configurationFactory, String nombreEstrategia, String nombre) {
		return createImportContextBuilder(configurationFactory, DefaultFileTypes.TIPO_TEXTO, nombreEstrategia, nombre, new Integer[]{1});
	}
	
	/**
	 * Crea un contexto para la importacion de datos mediante un archivo de tipo excel ({@link DefaultFileTypes#TIPO_EXCEL_97}).
	 * Esta instancia se crea a partir del nombre de la estrategia a utilizar, y el nombre generico de la importacion.
	 * Por defecto se utiliza el tipo de archivo de excel {@link DefaultFileTypes#TIPO_EXCEL_97}, y la pagina numero uno solamente
	 * 
	 * @param nombreEstrategia estrategia registrada para procesar cada registro de la importacion.
	 * @param nombre generico del importador de datos.
	 * @return una instancia del contexto de importacion de datos.
	 */
	public static ImportContextBuilder crearContextoTipoExcel(ConfigurationFactory configurationFactory, String nombreEstrategia, String nombre) {
		return createImportContextBuilder(configurationFactory, DefaultFileTypes.TIPO_EXCEL_97, nombreEstrategia, nombre, new Integer[]{1});
	}
	
	/**
	 * Crea un contexto para la importacion de datos mediante un archivo de tipo excel {@link DefaultFileTypes#TIPO_EXCEL_97}.
	 * Esta instancia se crea a partir del nombre de la estrategia, el nombre (o descripcion) de la importacion, y el numero
	 * de paginas a procesar del excel.
	 * 
	 * @param nombreEstrategia estrategia registrada para procesar cada registro de la importacion.
	 * @param nombre generico (o descripcion) del imortador de datos.
	 * @param numerosPaginas a procesar del archivo.
	 * @return una instancia del contexto de importacion de datos.
	 */
	public static ImportContextBuilder crearContextoTipoExcel(ConfigurationFactory configurationFactory, String nombreEstrategia, String nombre, Integer[] numerosPaginas) {
		return createImportContextBuilder(configurationFactory, DefaultFileTypes.TIPO_EXCEL_97, nombreEstrategia, nombre, numerosPaginas);
	}

	private ConfigurationFactory configurationFactory;

	private String strategyName;
	
	private FileTypeSupportable fileType;
	
	private String name;
	
	private List<Integer> pages;
	
	private boolean hasHeader;
	
	private boolean securityFileError = true;
	
	private long maxNoProcess = -1;
	
	public ImportContextBuilder(){
		super();
		this.pages = new ArrayList<Integer>();
	}
	
	public ImportContextBuilder withStrategyName(String strategyName) {
		this.strategyName = strategyName;
		return this;
	}
	
	public ImportContextBuilder withFileType(FileTypeSupportable fileType) {
		this.fileType = fileType;
		return this;
	}
	
	public ImportContextBuilder withName(String name) {
		this.name = name;
		return this;
	}
	
	public ImportContextBuilder withPages(Integer...pages) {
		Collections.addAll(this.pages, pages);
		return this;
	}
	
	public ImportContextBuilder withHeader() {
		this.hasHeader = true;
		return this;
	}
	
	public ImportContextBuilder withConfigurationFactory(ConfigurationFactory configurationFactory) {
		this.configurationFactory = configurationFactory;
		return this;
	}
	
	public ImportContextBuilder withoutHeader() {
		this.hasHeader = false;
		return this;
	}
	
	public ImportContextBuilder withoutSecurityFileError() {
		this.securityFileError = false;
		return this;
	}
	
	public ImportContextBuilder withSecurityFileError() {
		this.securityFileError = true;
		return this;
	}
	
	public ImportContextBuilder withMaximumNoProcess(int maxNoProcess) {
		this.maxNoProcess = maxNoProcess;
		return this;
	}
	
	protected Integer[] getPagesAsArray() {
		return this.pages.toArray(new Integer[this.pages.size()]);
	}
	
	public ImportContext buildImportContext() {
		ImportContext importContext = new ImportContext(this.configurationFactory);
		importContext.setNombreEstrategia(this.strategyName);
		importContext.setTipoArchivo((DefaultFileTypes) this.fileType);
		importContext.setNombre(this.name);		
		importContext.agregarPaginas(this.getPagesAsArray());
		
		if(this.hasHeader) {
			importContext.tieneHeader();
		} else {
			importContext.noTieneHeader();
		}
		
		if(this.securityFileError) {
			importContext.activarSeguridadErrorFila();
		} else {
			importContext.desactivarSeguridadErrorFila();
		}
		
		importContext.setMaximoNoProcesado(this.maxNoProcess);
		return importContext;
	}

	public ImportContextBuilder withHeader(boolean hasHeader) {
		this.hasHeader = hasHeader;
		return this;
	}

	public ImportContextBuilder withSecurityFileError(boolean securityFileError) {
		this.securityFileError = securityFileError;
		return this;
	}
	
}
