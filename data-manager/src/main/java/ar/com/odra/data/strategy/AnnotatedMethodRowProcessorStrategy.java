/**
 * 
 */
package ar.com.odra.data.strategy;

import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import ar.com.odra.data.annotation.generator.BindParam;
import ar.com.odra.data.annotation.generator.MethodGenerator;
import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.importer.data.ImportContext;
import ar.com.odra.data.matcher.MatchResult;
import ar.com.odra.data.matcher.ParametersMatcher;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;
import ar.com.odra.data.utils.BindMode;
import ar.com.odra.data.utils.Enviroment;
import ar.com.odra.data.utils.Utils;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class AnnotatedMethodRowProcessorStrategy extends AnnotatedRowProcessorStrategy {
	
	private static Logger LOGGER = Logger.getLogger(AnnotatedMethodRowProcessorStrategy.class);

	private MethodGenerator 			methodGenerator;
	private Method 						method;
	
	public AnnotatedMethodRowProcessorStrategy(Object dataGeneratorInstance, ParameterMatcheableRegistry matcheableRegistry, Method method) {
		this(dataGeneratorInstance, matcheableRegistry, method, null);
	}
	
	public AnnotatedMethodRowProcessorStrategy(Object dataGeneratorInstance, ParameterMatcheableRegistry matcheableRegistry, Method method, Method errorMethod) {
		super(dataGeneratorInstance, matcheableRegistry, errorMethod);
		this.setMethod(method);
		this.setMethodGenerator(method.getAnnotation(MethodGenerator.class));
	}
	
	/**
	 * @return the method
	 */
	protected Method getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	protected void setMethod(Method method) {
		this.method = method;
	}

	/**
	 * @return the methodGenerator
	 */
	public MethodGenerator getMethodGenerator() {
		return methodGenerator;
	}

	/**
	 * @param methodGenerator the methodGenerator to set
	 */
	protected void setMethodGenerator(MethodGenerator methodGenerator) {
		this.methodGenerator = methodGenerator;
	}

	protected String getAnnotatedSourceName() {
		boolean isEmptyName = Utils.isEmptyONull(this.getMethodGenerator().name());
		return isEmptyName ? this.getMethod().getName() : this.getMethodGenerator().name();
	}

	protected BindMode getMethodGeneratorBindMode() {
		return this.getMethodGenerator().bindMode();
	}
	
	@Override
	public BindMode getConfiguredBindMode() {
		BindMode dataGeneratorBindMode = super.getDataGeneratorBindMode();
		BindMode methodGeneratorBindMode = this.getMethodGeneratorBindMode();
		if(!BindMode.NONE.equals(methodGeneratorBindMode)) {
			return methodGeneratorBindMode;
		} else {
			if(!BindMode.NONE.equals(dataGeneratorBindMode)) {
				return dataGeneratorBindMode;
			}
		}
		return BindMode.NONE;
	}

	@Override
	protected ParametersMatcher createParametersMatcher(ImportContext importContext, RowFileImport rowFileImport) {
		ParametersMatcher matchingContext = super.createParametersMatcher(importContext, rowFileImport);
		matchingContext.setMethod(this.getMethod());
		return matchingContext;
	}
	
	@Override
	public boolean canUseBindMode(MatchResult matchResult, BindMode bindMode, Object bean, RowFileImport rowFileImport, int currentPosition) {
		int position = currentPosition;
		Method method = this.getMethod();
		MethodGenerator methodGeneratorAnnotation = method.isAnnotationPresent(MethodGenerator.class) ? method.getAnnotation(MethodGenerator.class) : null;
		boolean hasParametersNameInMethodGenerator = methodGeneratorAnnotation != null && methodGeneratorAnnotation.parameterNames().length > position;

		boolean hasDomainClassAsParameter = matchResult.matchDomainClass();
		if(!hasParametersNameInMethodGenerator) {
			if(hasDomainClassAsParameter) {
				position++;
			}
		}
		boolean hasBindParamAnnotation = this.getSourceObject(bean, rowFileImport, position) != null;
		LOGGER.debug("Has parameters [" + position + "] name in method generator [" + hasParametersNameInMethodGenerator + "] - has BindParam annotation [" + hasBindParamAnnotation + "].");
		return hasParametersNameInMethodGenerator || hasBindParamAnnotation;
	}
	
	protected BindParam getSourceObject(Object bean, RowFileImport rowFileImport, int position) {
		Annotation[][] parameterAnnotationsArray = method.getParameterAnnotations();
		if(parameterAnnotationsArray.length > position) {
			Annotation[] annotations = parameterAnnotationsArray[position];
			if(annotations.length > 0) {
				for (Annotation annotation : annotations) {
					if(BindParam.class.isAssignableFrom(annotation.annotationType())) {
						return (BindParam) annotation;
					}
				}
			}
		}
		return null;
	}
	
	public String getPropertyNameAt(MatchResult matchResult, BindMode bindMode, Object bean, RowFileImport rowFileImport, int currentPosition) {
		MethodGenerator methodGeneratorAnnotation = method.isAnnotationPresent(MethodGenerator.class) ? method.getAnnotation(MethodGenerator.class) : null;
		String[] propertyNames = methodGeneratorAnnotation != null ? methodGeneratorAnnotation.parameterNames() : new String[]{};
		boolean hasPropertyNames = propertyNames.length > 0;
		int position = currentPosition;
		if(!hasPropertyNames) {
			boolean hasDomainClassAsParameter = matchResult.matchDomainClass();
			if(hasDomainClassAsParameter) {
				position++;
			}
		}
		String propertyName = 	propertyNames.length > position ? propertyNames[position] : null;
		if(Utils.isEmptyONull(propertyName)) {
			BindParam bindParam = this.getSourceObject(bean, rowFileImport, position);
			if(bindParam != null) {
				propertyName = bindParam.name();
			}
		}
		return propertyName;
	}

	@Override
	public Integer getOrder() {
		return this.getMethodGenerator().order();
	}
	
	protected boolean hasMethodReturnType() {
		return this.getMethod().getReturnType() != null;
	}
	
	@Override
	public Class<?> getDomainClass(ImportContext importContext) {
		ParametersMatcher matchingContext = new ParametersMatcher();
		matchingContext.setDataGenerator(this.getDataGeneratorInstance());
		matchingContext.setMethod(this.getMethod());
		matchingContext.setImportContext(importContext);
		Class<?> domainClass = matchingContext.getDomainClass();
		Class<?> returnType = this.getMethod().getReturnType();
		return domainClass != null ? domainClass : returnType;
	}
	
	@Override
	protected boolean hasInputStream() {
		return super.hasInputStream() || this.hasMethodGeneratorSource();
	}

	protected boolean hasMethodGeneratorResource() {
		return !Utils.isEmptyONull(this.getMethodGenerator().resource());
	}
	
	protected boolean hasMethodGeneratorSource() {
		return !Utils.isEmptyONull(this.getMethodGenerator().source());
	}
	
	@Override
	public String getResource() {
		if(this.hasMethodGeneratorResource()) {
			return this.getMethodGenerator().resource();
		}
		return super.getResource();
	}
	
	@Override
	public InputStream getSourceInputStream(Enviroment enviroment) {
		if(this.hasMethodGeneratorSource()) {
			MethodGenerator methodGenerator = this.getMethodGenerator();
			return super.getInputStream(enviroment, methodGenerator.source(), this.getResource());
		}
		return super.getSourceInputStream(enviroment);
	}
	
	@Override
	public boolean hasHeader() {
		return this.hasMethodGeneratorSource() ? this.getMethodGenerator().hasHeader() : super.hasHeader();
	}
	
	@Override
	public String getDescription() {
		return this.hasMethodGeneratorSource() ? this.getMethodGenerator().description() : super.getDescription();
	}
	
	@Override
	public Integer[] getPages() {
		int[] pages = this.getMethodGenerator().pages();
		return ArrayUtils.toObject(pages);
	}
}
