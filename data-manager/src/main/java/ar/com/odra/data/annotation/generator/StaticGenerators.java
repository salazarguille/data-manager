package ar.com.odra.data.annotation.generator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE})
public @interface StaticGenerators {

	StaticGenerator[] 	values();
}
