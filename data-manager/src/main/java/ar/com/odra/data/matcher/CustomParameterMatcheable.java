/**
 * 
 */
package ar.com.odra.data.matcher;

import java.lang.annotation.Annotation;

import ar.com.odra.data.content.RowFileImport;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public abstract class CustomParameterMatcheable implements ParameterMatcheable {

	public abstract Object matchParameter(Class<?> parameterType, Annotation[] annotations, Object value);
	
	public boolean appliesMatching(Class<?> parameterType, Annotation[] annotations, Object value) {
		return true;
	}

	public boolean appliesMatching(Class<?> parameterType, Annotation[] annotations, RowFileImport rowFileImport, int parameterPosition) {
		Object value = rowFileImport.getValor(parameterPosition);
		return this.appliesMatching(parameterType, annotations, value);
	}

	public Object matchParameter(Class<?> parameterType, Annotation[] annotations, RowFileImport rowFileImport, int parameterPosition) {
		Object value = rowFileImport.getValor(parameterPosition);
		return this.matchParameter(parameterType, annotations, value);
	}

	public boolean matchDomainClass() {
		return false;
	}

	public boolean appliesAsParameter() {
		return true;
	}
}
