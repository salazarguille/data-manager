/**
 * 
 */
package ar.com.odra.data.locator;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface Locatable {

	public <T> T[] execute(T ... beans);
}
