package ar.com.odra.data.annotation.as;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ar.com.odra.data.utils.FieldSeparator;

/**
 * Anotacion utilizada para convertir un parametro del metodo generador en un array del tipo especificado en la firma del metodo.
 * La conversion se basa en el campo {@link #splitter()} que se haya seleccionado.
 * </br></br>
 * Por default el atributo {@link #splitter()} es {@link FieldSeparator#COMMA}
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.PARAMETER})
public @interface AsArray {

	FieldSeparator splitter() default FieldSeparator.COMMA;
	
}
