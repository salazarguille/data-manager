/**
 * 
 */
package ar.com.odra.data.register.impl;

import ar.com.odra.data.matcher.DefaultParametersMatcheable;
import ar.com.odra.data.register.ParameterMatcheablesRegister;
import ar.com.odra.data.registry.ParameterMatcheableRegistry;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class DefaultParameterMatcheablesRegister implements ParameterMatcheablesRegister {
	public void registerParameters(ParameterMatcheableRegistry matcheableRegistry) {
		matcheableRegistry.register(DefaultParametersMatcheable.ARRAY_TYPE);
		matcheableRegistry.register(DefaultParametersMatcheable.BOOLEAN_TYPE);
		matcheableRegistry.register(DefaultParametersMatcheable.CUSTOM_TYPE);
		matcheableRegistry.register(DefaultParametersMatcheable.DATE_TYPE);
		matcheableRegistry.register(DefaultParametersMatcheable.DOMAIN_BEAN);
		matcheableRegistry.register(DefaultParametersMatcheable.ENUM_TYPE);
		matcheableRegistry.register(DefaultParametersMatcheable.IMPORTER_CONTEXT_TYPE);
		matcheableRegistry.register(DefaultParametersMatcheable.ROW_FILE);
		matcheableRegistry.register(DefaultParametersMatcheable.ROW_IMPORT);
		matcheableRegistry.register(DefaultParametersMatcheable.STREAM_TYPE);
	}
}
