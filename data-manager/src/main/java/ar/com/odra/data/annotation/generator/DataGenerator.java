package ar.com.odra.data.annotation.generator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ar.com.odra.data.utils.BindMode;
import ar.com.odra.data.utils.StreamSource;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.TYPE})
public @interface DataGenerator {
	
	String			resource()		default "";
	
	StreamSource 	source()		default StreamSource.CLASSPATH;
	
	boolean			hasHeader()		default false;

	BindMode		bindMode()		default BindMode.NONE;
	
	Class<?>[]		dependencies()	default {};
	
	String			description()	default "";

	boolean			isSecurityFileError()	default false;

	int				maximumNoProcess()		default 0;
	
}
