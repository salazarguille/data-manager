/**
 * 
 */
package ar.com.odra.data.register;

import ar.com.odra.data.registry.RowProcessorStrategyFactoryRegistry;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public interface RowProcessorStrategyFactoryRegister {

	public void registerRowProcessorStrategyFactory(RowProcessorStrategyFactoryRegistry strategyFactoryRegistry);
}
