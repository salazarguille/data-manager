/**
 * 
 */
package ar.com.odra.data.registry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import ar.com.odra.data.factory.RowProcessorStrategyFactory;
import ar.com.odra.data.strategy.RowProcessorStrategy;
import ar.com.odra.data.utils.Utils;



/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class RowProcessorStrategyFactoryRegistry {

	private Set<RowProcessorStrategyFactory>	strategyFactories;
	
	public RowProcessorStrategyFactoryRegistry(RowProcessorStrategyFactory...factories) {
		super();
		if(!Utils.isNullArray(factories)) {
			this.setStrategyFactories(new HashSet<RowProcessorStrategyFactory>(Arrays.asList(factories)));
		} else {
			this.setStrategyFactories(new HashSet<RowProcessorStrategyFactory>());
		}
	}

	/**
	 * @return the strategyFactories
	 */
	public Set<RowProcessorStrategyFactory> getStrategyFactories() {
		return strategyFactories;
	}

	/**
	 * @param strategyFactories the strategyFactories to set
	 */
	protected void setStrategyFactories(Set<RowProcessorStrategyFactory> strategyFactories) {
		this.strategyFactories = strategyFactories;
	}

	public void registerStrategyFactory(RowProcessorStrategyFactory definitionStrategy) {
		this.getStrategyFactories().add(definitionStrategy);
	}
	
	public void removeStrategyFactory(RowProcessorStrategyFactory definitionStrategy) {
		this.getStrategyFactories().remove(definitionStrategy);
	}

	public Collection<RowProcessorStrategy> createRowProcessorStrategies(ParameterMatcheableRegistry matcheableRegistry, Object rowStrategy) {
		Collection<RowProcessorStrategy> strategies = new ArrayList<RowProcessorStrategy>();
		for (RowProcessorStrategyFactory strategyFactory: this.getStrategyFactories()) {
			if(strategyFactory.canCreateRowProcessorStrategies(rowStrategy)) {
				strategyFactory.createRowProcessorStrategies(rowStrategy, matcheableRegistry, strategies);
			}
		}
		return strategies;
	}

}
