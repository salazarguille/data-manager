/**
 * 
 */
package ar.com.odra.data.factory;

import java.util.Collection;

import ar.com.odra.data.importer.ImporterData;
import ar.com.odra.data.strategy.RowProcessorStrategy;


/**
 * El objetivo de esta clase es simplemente centralizar la creacion de estrategias de procesadores de registros (o filas).
 * De esa manera se puede tener en una sola clase todas las estrategias que utiliza un modulo de un sistema. Esto permite 
 * registrar dichas estrategias en el importador de datos, sin necesidad de registrar una por una manualmente.
 * 
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 * 
 * @see RowProcessorStrategy
 * @see ImporterData
 */
public interface StrategiesRowFactory {

	/**
	 * Crea una coleccion de estrategias de procesadores de registros (o filas) a registrarse en el importador de datos.
	 * 
	 * @return una coleccion de estrategias de procesadores de filas.
	 */
	public Collection<RowProcessorStrategy> crearEstrategias();
	
}
