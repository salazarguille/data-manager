/**
 * 
 */
package ar.com.odra.data.matcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import ar.com.odra.common.helper.CollectionHelper;
import ar.com.odra.data.content.RowFileImport;
import ar.com.odra.data.strategy.AnnotatedRowProcessorStrategy;
import ar.com.odra.data.utils.BindMode;



/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class MatchResult {

	private static final Logger LOGGER = Logger.getLogger(MatchResult.class);
	
	private List<MatchParameterResult>	parametersResult;
	private Object						beanInstance;
	private RowFileImport				rowFileImport;
	
	public MatchResult(RowFileImport rowFileImport) {
		super();
		this.setParametersResult(new ArrayList<MatchParameterResult>());
		this.setRowFileImport(rowFileImport);
	}

	/**
	 * @return the rowFileImport
	 */
	public RowFileImport getRowFileImport() {
		return rowFileImport;
	}

	/**
	 * @param rowFileImport the rowFileImport to set
	 */
	protected void setRowFileImport(RowFileImport rowFileImport) {
		this.rowFileImport = rowFileImport;
	}

	/**
	 * @return the beanInstance
	 */
	public Object getBeanInstance() {
		return beanInstance;
	}

	/**
	 * @param beanInstance the beanInstance to set
	 */
	public void setBeanInstance(Object beanInstance) {
		this.beanInstance = beanInstance;
	}

	/**
	 * @return the parametersResult
	 */
	public List<MatchParameterResult> getParametersResult() {
		return parametersResult;
	}


	/**
	 * @param parametersResult the parametersResult to set
	 */
	protected void setParametersResult(List<MatchParameterResult> parametersResult) {
		this.parametersResult = parametersResult;
	}
	
	public void addMatchParameter(Class<?> type, ParameterMatcheable parameterMatcheable, Object value) {
		MatchParameterResult matchParameterResult = new MatchParameterResult(type, parameterMatcheable, value);
		LOGGER.info("Adding to match result a parameter result [" + matchParameterResult + "].");
		this.addMatchParameter(matchParameterResult);
	}
	
	public void addMatchParameter(MatchParameterResult matchParameterResult) {
		this.getParametersResult().add(matchParameterResult);
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Bean: [" + this.getBeanInstance() + "]\n");
		buffer.append("Parameter Result: [" + Arrays.toString(this.getParametersResult().toArray()) + "]\n");
		return buffer.toString();
	}
	
	public Object[] getParameterValues() {
		List<Object> values = new ArrayList<Object>();
		for(MatchParameterResult result: this.getParametersResult()) {
			values.add(result.getValue());
		}
		return values.toArray();
	}
	
	public Object bindBean(AnnotatedRowProcessorStrategy annotatedRowProcessorStrategy) {
		LOGGER.info("Processing bind mode for match result [" + this + "].");
		if(this.hasBeanInstance()) {
			Object bean = this.getBeanInstance();
			BindMode bindMode = annotatedRowProcessorStrategy.getConfiguredBindMode();
			LOGGER.debug("Bind mode configured [" + bindMode + "] for bean [" + bean + "].");
			bindMode.bind(this, annotatedRowProcessorStrategy, bean);
			return bean;
		}
		return null;
	}

	public boolean hasBeanInstance() {
		return this.getBeanInstance() != null;
	}

	public int getParametersLength() {
		int length = 0;
		for (MatchParameterResult result: this.getParametersResult()) {
			if(result.appliesAsParameter()) {
				length++;
			}
		}
		return length;
	}

	public MatchParameterResult getParameterResultAt(int position) {
		return this.getParametersResult().size() > position ? this.getParametersResult().get(position) : null;
	}
	
	public boolean hasParameterTypes(Class<?>...types) {
		int currentSize = CollectionHelper.getSizeFromArray(this.getParametersResult().toArray());
		int otherSize = CollectionHelper.getSizeFromArray(types);
		boolean hasParameterValues = currentSize == otherSize;
		if(currentSize == otherSize) {
			for (MatchParameterResult matchParameterResult : this.getParametersResult()) {
				int resultIndex = this.getParametersResult().indexOf(matchParameterResult);
				Class<?> value = types[resultIndex];
				boolean hasValueType = matchParameterResult.hasValueType(value);
				LOGGER.debug("Parameter result [" + matchParameterResult + "] has value type [" + value + "] ? [" + hasValueType + "].");
				if(!hasValueType) {
					return false;
				}
			}
		}
		return hasParameterValues;
	}

	public boolean matchDomainClass() {
		for (MatchParameterResult matchParameterResult : this.getParametersResult()) {
			boolean matchDomainClass = matchParameterResult.matchDomainClass();
			if(matchDomainClass) {
				return true;
			}
		}
		return false;
	}
}
