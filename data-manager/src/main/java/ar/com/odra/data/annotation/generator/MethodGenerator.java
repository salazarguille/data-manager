package ar.com.odra.data.annotation.generator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import ar.com.odra.data.utils.BindMode;
import ar.com.odra.data.utils.StreamSource;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target( { ElementType.METHOD})
public @interface MethodGenerator {

	String			resource()	default "";
	
	StreamSource 	source()	default StreamSource.CLASSPATH;
	
	String			name()		default "";
	
	BindMode		bindMode()	default BindMode.NONE;
	
	int 			order()		default 0;
	
	int[] 			pages()		default {1};

	String[] 		parameterNames()	default {};

	boolean			hasHeader()			default true;

	String			description()		default "";
}
