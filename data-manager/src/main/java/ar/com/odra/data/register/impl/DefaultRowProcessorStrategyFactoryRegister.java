/**
 * 
 */
package ar.com.odra.data.register.impl;

import ar.com.odra.data.factory.impl.AnnotatedMethodRowProcessorStrategyFactory;
import ar.com.odra.data.factory.impl.AnnotatedStaticRowProcessorStrategyFactory;
import ar.com.odra.data.register.RowProcessorStrategyFactoryRegister;
import ar.com.odra.data.registry.RowProcessorStrategyFactoryRegistry;

/**
 * TODO Create comments.
 * 
 * @author <a href="mailto:guillermo@odra.com.ar">Guille Salazar</a>
 * @since 05/06/2013
 *
 */
public class DefaultRowProcessorStrategyFactoryRegister implements RowProcessorStrategyFactoryRegister {

	public void registerRowProcessorStrategyFactory(RowProcessorStrategyFactoryRegistry strategyFactoryRegistry) {
		strategyFactoryRegistry.registerStrategyFactory(new AnnotatedMethodRowProcessorStrategyFactory());
		strategyFactoryRegistry.registerStrategyFactory(new AnnotatedStaticRowProcessorStrategyFactory());
	}
	
}
